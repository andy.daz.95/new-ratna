<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
    <div class="row">
        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Invoice</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="input-field col l3">
                                    <input id="filterInvoice" type="text" class="f-input">
                                    <label>Filter Purchase Invoice No.</label>
                                </div>
                                <div class="input-field col l3">
                                    <input id="filterPo" type="text" class="f-input datepicker">
                                    <label>Filter PO</label>
                                </div>
                                <div class="input-field col l3">
                                    <input id="filterSupplier" type="text" class="f-input">
                                    <label>Filter Supplier</label>
                                </div>
                                <div class="col l12 m12 s12">
                                    <table id="purchaseInvoiceTable" class="highlight table display nowrap dataTable dtr-  inline">
                                        <thead>
                                        <tr>
                                            <th>No. Invoice</th>
                                            <th>PO. Number</th>
                                            <th>Tanggal PO</th>
                                            <th>Supplier</th>
                                            <th>Invoice Amount</th>
                                            <th>Jatuh Tempo</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data['invoice'] as $key => $value)
                                            {{--<tr value="{{$value->invoice_purchase_id}}">--}}
                                            {{--<td>{{$value->invoice_purchase_number}}</td>--}}
                                            {{--<td>{{$value->purchase_order_number}}</td>--}}
                                            {{--<td>{{$value->date_purchase_order.' / '.$value->po_note}}</td>--}}
                                            {{--<td>{{$value->company_name}}</td>--}}
                                            {{--<td>{{number_format($value->total_amount)}} </td>--}}
                                            {{--<td>{{$value->jatuh_tempo}}</td>--}}
                                            {{--</tr>--}}
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Details PO</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <br>
                                <form id="formPo">
                                    <div class="col l12 m12 s12">
                                        <div class="table-responsive" style="overflow: visible;">
                                            <br>
                                            <table class="stoko-table no-border">
                                                <thead>
                                                <tr>
                                                    <th class="theader">Kategori Barang</th>
                                                    <th class="theader">Brand</th>
                                                    <th class="theader">Tipe Barang</th>
                                                    <th class="theader">Harga Beli</th>
                                                    <th class="theader">Diskon</th>
                                                    <th class="theader">Harga Jual</th>
                                                    <th class="theader">Rate Keuntungan</th>
                                                </tr>
                                                </thead>
                                                <tbody id="barang-data">
                                                <tr id="no-item-po"><td colspan="7"><span> No Item Selected</span></td></tr>
                                                <tr class="barang-row" hidden>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="id-1" type="text" class="f-input id-detail" name="idbarang[]" hidden>
                                                            <input id="kategori-1" type="text" class="f-input" name="kategori[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="brand-1" type="text" class="f-input brand" name="brand[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="type-1" type="text" class="f-input type" name="type[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="harga-beli-1" type="text" class="f-input hargabeli" name="hargabeli[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="diskon-1" type="text" class="f-input diskon" name="diskon[]" value="0">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="harga-jual-1" type="text" class="f-input hargajual" name="hargajual[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="rate-1" type="text" class="f-input rate" name="rate[]" disabled>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <br>
                                            <div id="discount-transaction-div" class="input-field" hidden>
                                                <label>Diskon Transaksi</label>
                                                <input id="diskon-transaksi" type="text" class="f-input diskon-transaksi" name="diskontransaksi" value="0">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<div id="modal-delete-invoice" class="modal">
    <div class="modal-content">
        <h5 id="delete-message"></h5>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
        <a id="confirm-delete-invoice" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
    </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/bootstrap-select.min.js',
            dataType: "script",
        });

        firstload();

        $('#purchaseInvoiceTable').on('click','tr, .edit', function(event){
            event.stopImmediatePropagation();
            var id = $(this).attr('value');
            var mode = $(event.currentTarget).attr('mode');
            $.ajax({
                type:"Get",
                url:"getpurchaseinvoice",
                data:{id:id},
                success:function(response){
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    $.each(response.podetail, function(k,v){
                        var newid = "id-"+(k+1);
                        var newKategori = "kategori-"+(k+1);
                        var newBrand = "brand-"+(k+1);
                        var newType = "type-"+(k+1);
                        var newHargaBeli = "harga-beli-"+(k+1);
                        var newHargaJual = "harga-jual-"+(k+1);
                        var newDiskon = "diskon-"+(k+1);
                        var newRate = "rate-"+(k+1);
                        $temp = $original.clone();
                        $temp.removeAttr('hidden');
                        $temp.find('#id-1').attr('id',newid);
                        $temp.find('#kategori-1').attr('id',newKategori);
                        $temp.find('#brand-1').attr('id',newBrand);
                        $temp.find('#type-1').attr('id',newType);
                        $temp.find('#harga-jual-1').attr('id',newHargaJual);
                        $temp.find('#harga-beli-1').attr('id',newHargaBeli);
                        $temp.find('#diskon-1').attr('id',newDiskon);
                        $temp.find('#rate-1').attr('id',newRate);
                        $temp.appendTo('#barang-data');
                        $('#id-'+(k+1)).val(v.purchase_order_details_id).attr('disabled',true);
                        $('#kategori-'+(k+1)).val(v.c_name).attr('disabled',true);
                        $('#brand-'+(k+1)).val(v.b_name).attr('disabled',true);
                        $('#type-'+(k+1)).val(v.t_name).attr('disabled',true);
                        $('#harga-beli-'+(k+1)).val(v.price_buy).attr('disabled',true);
                        $('#harga-jual-'+(k+1)).val(v.price_sale).attr('disabled',true);
                        $('#rate-'+(k+1)).val(v.profit_rate).attr('disabled',true);
                        $('#diskon-'+(k+1)).val(v.discount_nominal).attr('disabled',true);
                    });
                    $('#diskon-transaksi').val(response.discount_transaction).attr('disabled',true);
                    $('#discount-transaction-div').removeAttr('hidden');
                    $('#no-item-po').attr('hidden',true);
                },complete:function(){
                }
            });
        });

        //function
        function firstload()
        {
            invoiceTable = $('#purchaseInvoiceTable').DataTable({ // This is for home page
                searching: true,
                processing: true,
                serverSide: true,
                "aaSorting": [],
                'sDom': 'tip',
                "pageLength": 5,
                ajax: {
                    url : 'getpurchaseinvoicetable',
                    data : function (d){
                        d.number = $('#filterPenerimaan').val();
                        d.po = $('#filterPo').val();
                        d.supplier = $('#filterSupplier').val();
                    }
                },
                rowId : 'invoice_purchase_id',
                columns: [
                    { data: 'invoice_purchase_number', name: 'invoice_purchase_number', class:'noinvoice'},
                    { data: 'purchase_order_number', name: 'purchase_order_number'},
                    { data: 'date_purchase_order', name: 'date_purchase_order'},
                    { data: 'company_name', name: 'company_name'},
                    { data: 'total_amount', name: 'total_amount'},
                    { data: 'jatuh_tempo', name: 'jatuh_tempo'},
                ],
            });

            $('#filterInvoice').on('keyup', function () { // This is for news page
                invoiceTable.draw();
            });
            $('#filterPo').on('keyup', function () { // This is for news page
                invoiceTable.draw();
            });
            $('#filterSupplier').on('keyup', function () { // This is for news page
                invoiceTable.draw();
            });

        }
    });
</script>
