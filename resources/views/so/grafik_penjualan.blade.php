<script type="text/javascript">

$(document).ready(function(){
	var data= [];
	$.ajax({
		method:"GET",
		url:"grafik_penjualan/2017-10-01/2017-11-31",
		success:function(response){
			console.log(response);
			var data = [];
			$.each(response, function(key,value){
				var object = { "label":value.product_name, "y":parseInt(value.product_sales)};
				data.push(object);
			});
			var chart = new CanvasJS.Chart("chartContainer", {
				animationEnabled: true,
				theme: "dark1",
				title:{
					text: "Grafik Penjualan"              
				},
				axisY: {
					title: "Total Penjualan in Rupiah" 
				},
				data: [              
				{
					// Change type to "doughnut", "line", "splineArea", etc.
					type: "column",
					dataPoints: data,
				}
				]
			});
			chart.render();
		}
	})
})

</script>
<div id="chartContainer" style="height: 300px; width: 100%;"></div>
