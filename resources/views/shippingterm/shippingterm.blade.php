<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/toastr.css">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l4 m4 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">create</i>Form Shipping Term</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <form id="formExpenseCategory">
                <div class="row margin-top">
                  <div class="col l12">
                    <div class="row">
                      <div class="input-field col l12 m12 s12">
                        <input id="id" type="text" class="f-input" name="id" disabled>
                        <label>ID</label>
                      </div>
                      <div class="input-field col l12 m12 s12">
                        <input id="name" name="name" type="text" class="f-input">
                        <label>Nama</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col l12 m12 s12">
                    <button id="btn-submit" type="submit" class="btn btn-raised btn-sm light-blue darken-2" mode="save">simpan</button>
                    <a href="#" class="btn btn-sm btn-raised grey lighten-4 grey-text text-darken-4 reset">cancel</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </li>
      </ul>
    </div>
    <div class="col l8 m8 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Shipping Term</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row margin-top">
                <div class="input-field col l6">
                  <input id="filterCategory" type="text" class="f-input">
                  <label>Search</label>
                </div>
                <div class="col l12 m12 s12">
                  <table id="shippingTermTable" class="table table-bordered display nowrap dataTable dtr-inline">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Shipping Term</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($shippingterm as $key => $value)
                      <tr value="{{$value->shipping_term_id}}">
                          <td>{{$value->shipping_term_id}}</td>
                          <td>{{$value->shipping_term_name}}</td>
                        <td>
                         <a class="btn btn-sm btn-raised light-blue darken-2 edit"><i class="material-icons">edit</i></a>
                         <a href="#delete_data" class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>
                       </td>
                     </tr>
                      @endforeach
               </tbody>
             </table>
           </div>
         </div>
       </div>
     </div>
   </li>
 </ul>
</div>
</div>
</div>
<!-- Modal Delete Shipping Term -->
<div id="delete_data" class="modal">
  <div class="modal-content">
    <h5>Yakin ingin menghapus data berikut?</h5>
    <div class="row">
      <div class="col l12">
        <table id="modal-delete-data">
          
        </table>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <form>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat red white-text delete">Hapus</a>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat">cancel</a>
    </form>
  </div>
</div>
<!-- Modal Tambah Shipping Term -->
<div id="tambah_satuan" class="modal modal-satuan">
  <form>
    <div class="modal-content">
      <h5>Tambah Shipping Term</h5>
      <div class="row">
        <div class="input-field col l12 m12 s12">
          <input type="text" class="validate" placeholder="Cm / M / Inch / Lembar">
          <label>Shipping Term Barang</label>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action waves-effect btn-flat light-blue darken-2 white-text">tambah</a>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat">cancel</a>
    </div>
  </form>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#btn-submit').attr('mode', 'save');
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: 'js/custom.js',
      dataType: "script",
    });
    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });

    shippingTermTable = $('#shippingTermTable').DataTable({ // This is for home page
      searching: true,
      responsive: true,
      'sDom': 'ti',
      'pagingType': 'full_numbers_no_ellipses',
      "language": {
        "infoEmpty": "No records to display",
        "zeroRecords": "No records to display",
        "emptyTable": "No data available in table",
      },
    });

    $('#filterCategory').on('keyup', function () { // This is for news page
      shippingTermTable.search($(this).val()).draw();
    });

    $('#formExpenseCategory').submit(function(event){
      event.preventDefault();
      var mode = $(this).find('#btn-submit').attr('mode');
      var empty = required(['description']);
      if(empty != '0')
      {
        toastr.warning(empty+' tidak boleh kosong!');
      }else{
        if (mode == 'save') 
        {
          var id = $(this).find('#id').val();  
          $.ajax({
            type:"POST",
            url:"createshippingterm",
            data: $("#formExpenseCategory").serialize(),
            success:function(response){
              toastr.success('Shipping Term Telah Berhasil Dibuat!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
            }
          });  
        }else if(mode == 'edit'){
          var id = $(this).find('#id').val();  
          $.ajax({
            type:"POST",
            url:"updateshippingterm",
            data: $("#formExpenseCategory").serialize()+"&id="+id,
            success:function(response){
              toastr.success('Shipping Term Telah Berhasil Diubah!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
            }
          });
        }
      }
    });

    $('body').on('click','#shippingTermTable .reset', function(event){
      event.stopImmediatePropagation();
      $(this).closest('#shippingTermTable').find("input[type=text], textarea").val("");
      $('#btn-submit').attr('mode', 'save');
    });

    $('body').on('click','#shippingTermTable .edit', function(event){
      event.stopImmediatePropagation();
      var id = $(this).closest('tr').attr('value');
      $.ajax({
        type:"GET",
        url:"getshippingtermdata",
        data: {id:id},
        success:function(response){
          $('#id').val(response.shipping_term_id);
          $('#name').val(response.shipping_term_name);
        }
      });
      $('#btn-submit').attr('mode', 'edit');
    });

    $(".delete-modal").on('click',function(event){
      event.preventDefault();
      var id = $(this).closest('tr').attr('value');

      $.ajax({
        type:"GET",
        url:"modalshippingtermdata",
        async:false,
        data:{
          id:id,
        },
        success:function(response){
          $('#modal-delete-data').html(response);
        }
      });
      $('#delete_data').modal('open');
    });

    $(".delete").on('click',function(){
      var id = $(this).closest('.modal').find('#id-delete').attr('value');
      console.log(id);
      $.ajax({
        type:"Get",
        url:"deleteshippingterm",
        data:{id:id},
        success:function(response){
          toastr.success('Berhasil Menghapus Shipping Term!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
        }
      });
    });
  })
</script>
