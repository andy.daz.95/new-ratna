<?
$date = date("d F Y, H:i");
?>
<script type="text/javascript">
  function updateclock() {
    $('#clock').html(moment().format('D. MMMM YYYY H:mm:ss'));
  }
  setInterval(updateclock, 1000);
</script>
<nav class="red darken-1">
  <div class="container-fluid">
    <div class="nav-wrapper">
      <a href="#" data-activates="slide-out" class="button-collapse left"><i class="material-icons">menu</i></a>
      <a href="#" class="brand-logo center">Ratna Boutique</a>
        {{--<div class=".center">--}}
            {{--<img src="{{asset('images/ratna.png')}}" alt="no-images">--}}
        {{--</div>--}}
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li id="clock">08 / 02 /2017 ( 11:53:46 AM )</li>
      </ul>
    </div>
  </div>
</nav>