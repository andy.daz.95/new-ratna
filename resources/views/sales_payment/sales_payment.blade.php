<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/toastr.css">
<link rel="stylesheet" href="css/bootstrap-select.min.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text"><i class="material-icons">search</i>Cari Pembayaran Cicilan</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row margin-top">
                <div class="input-field col l3">
                  <input id="filterPaymentNumber" type="text" class="f-input">
                  <label>Filter Purchase Payment No.</label>
                </div>
                <div class="input-field col l3">
                  <input type="text" id="filterPelanggan" class="f-input">
                  <label>Filter Pelanggan</label>
                </div>
                <div class="input-field col l3">
                  <input type="text" id="filterTanggal" class="f-input">
                  <label>Filter Tanggal Pembayaran</label>
                </div>
                <div class="col l12 m12 s12 margin-top">
                  <div class="table-responsive">
                    <table id="salesPaymentTable" class="highlight table table-bordered display nowrap dataTable dtr-inline"
                           style="width: 100%">
                      <thead>
                      <tr>
                        <th>No Pembayaran</th>
                        <th>Tanggal Pembayaran</th>
                        <th>Pelanggan</th>
                        <th>Total</th>
                        <th>Action</th>
                      </tr>
                      </thead>
                      <tbody>
                      {{--@foreach($data['payment'] as $key => $value)--}}
                      {{--<tr value="{{$value->payment_sales_id}}">--}}
                      {{--<td class="nopayment">{{$value->payment_sales_number}}</td>--}}
                      {{--<td>{{$value->date_sales_payment}}</td>--}}
                      {{--<td>--}}
                      {{--{{$value->so->customer->first_name.' '.$value->so->customer->last_name}}--}}
                      {{--</td>--}}
                      {{--<td>{{$value->total_paid}}</td>--}}
                      {{--<td>--}}
                      {{--kalau payment pertama tidak dapat di delete dari page ini, harus dari so --}}
                      {{--@if($value->is_first == 0)--}}
                      {{--<a class="btn btn-sm btn-raised red delete-modal tooltipped" data-position="bottom" data-tooltip="edit"><i--}}
                      {{--class="material-icons">delete</i></a>--}}
                      {{--@endif--}}
                      {{--</td>--}}
                      {{--</tr>--}}
                      {{--@endforeach--}}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Formulir Pembayaran Cicilan</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <form id="formPayment">
                  <br/>
                  <div class="input-field col l3">
                    <label>No Pembayaran</label>
                    <input id="nopayment" type="text" name="nopayment" class="f-input" placeholder="No Pembayaran">
                  </div>
                  <div class="input-field col l3">
                    <label>Pelanggan</label>
                    <select id="customer" class="browser-default selectpicker"  data-live-search="true" name="customer" >
                      <option value="0">Select Customer</option>
                      @foreach($data['customer'] as $key => $value)
                        <option value="{{$value->customer_id}}">{{$value->first_name.' '.$value->last_name.' / '.$value->book_number}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="input-field col l3">
                    <label>Sales Order</label>
                    <select id="so" class="browser-default selectpicker"  data-live-search="true" name="so" disabled>
                      <option value="0">Select Sales Order</option>

                    </select>
                  </div>
                  <div class="input-field col l3">
                    <label>Jenis Pembayaran</label>
                    <input id="paymenttype" class="f-input"  name="paymenttype" disabled/>
                  </div>
                  <div class="input-field col l3">
                    <label>Saldo</label>
                    <input id="balance" class="f-input"  name="balance" disabled/>
                  </div>
                  <div class="input-field col l3">
                    <label>Tgl Pembayaran</label>
                    <input id="tglpayment" type="text" class="f-input datepicker" name="tglpayment" placeholder="Tanggal SO">
                  </div>
                  <div class="input-field col l3">
                    <label>Note</label>
                    <input id="note" type="text" class="f-input" name="note" placeholder="Note">
                  </div>
                  <div class="col l12 m10 s12 margin-top">
                    <div id="append-payment_detail" class="table-responsive">
                      {{-- append table credit details --}}
                      @include('sales_payment.sales_payment_detail')
                    </div>
                  </div>
                  <div class="col l6 m6 s12 margin-top">
                    <div class="input-field" class="">
                      <input id="total" style="display:inline-block; width: 90%;" type="text" class="f-input" disabled>
                      <a style="display:inline-block; width: 9%;"
                         class="btn btn-sm btn-raised green darken-2 copy-value right tooltipped" data-position="bottom" data-tooltip="copy"><i
                                class="material-icons">content_copy</i></a>
                      <input id="total-hidden" type="text" class="f-input" name="total"
                             hidden>
                      <label>Total</label>
                    </div>
                    {{--<div class="input-field" class="">--}}
                      {{--<input id="total" type="text" class="f-input" disabled>--}}
                      {{--<input id="total-hidden" type="text" class="f-input" name="total" hidden>--}}
                      {{--<label>Total</label>--}}
                    {{--</div>--}}
                    <div id="div-receiveable" class="input-field" class="">
                      <input id="payable" type="text" class="f-input" disabled>
                      <label>Sisa Hutang</label>
                    </div>
                  </div>
                  <div class="col l6 margin-top">
                    <div id="append-metode-pembayaran">
                      <div class="metode-pembayaran-div col l12">
                        <div class="input-field col l4">
                          <label>Metode Pembayaran</label>
                          <select id="metode-1" class="selectpicker browser-default" data-live-search="true" name="metode[]">
                            @foreach($data['payment_method'] as $key => $value)
                              <option value="{{$value->payment_method_id}}">{{$value->bank->bank_name.' '.$value->payment_method_type->description}}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="input-field col l8">
                          <label>Jumlah Bayar</label>
                          <input style="display:inline-block; width: 80%" id="bayar-1" type="text" class="f-input bayar" placeholder="" name="bayar[]">
                          <a id="tambah-metode-pembayaran" style="display:inline-block; width: 15%;" class="btn btn-sm btn-raised green darken-2 add right"><i class="material-icons">add</i></a>
                          <a style="display:none; width: 15%;" class="btn btn-sm btn-raised red darken-2 remove right"><i class="material-icons">remove</i></a>
                        </div>
                      </div>
                    </div>
                    <div class="col l12">
                      <div class="input-field col l6">
                        <label>Additional Charge</label>
                        <input id="additionalCharge" name="addcharge" type="text" class="f-input">
                      </div>
                      <div class="input-field col l6">
                        <label>Additional Charge Note</label>
                        <input id="additionalNote" name="addnote" type="text" class="f-input">
                      </div>
                    </div>
                    <div class="col l12">
                      <div class="input-field col l12">
                        <label>Total Bayar</label>
                        <input type="text" name="totalbayar" id="totalbayar" class="f-input" disabled>
                      </div>
                      <div id="div-remaining" class="input-field col l12">
                        <label>Sisa Bayar</label>
                        <input type="text" name="remaining" id="remaining"
                               class="f-input" disabled>
                      </div>
                    </div>
                  </div>
                  <div class="col l12 s12 m12 main-button-group margin-top">
                    <a href="#" class="btn-stoko teal white-text submit" mode="save">simpan</a>
                    <a id="clear" href="#" class="btn-stoko orange">batal</a>
                    <a id="print" href="#" target="_blank" class="btn-stoko">print</a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<div id="modal-delete-payment" class="modal">
  <div class="modal-content">
    <h5 id="delete-message"></h5>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a id="confirm-delete-payment" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
  </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'js/bootstrap-select.min.js',
            dataType: "script",
        });

        firstload();

        $('#customer').on('change',function(event){
            event.stopImmediatePropagation();
            var customer = $(this).val();
            $.ajax({
                type:"GET",
                url:"getsofromcustomer",
                data:{customer:customer},
                success:function(response){
                    $('#so').html('');
                    $('#balance').val(response['customer'].total_balance);
                    $('#so').html('<option value="0">Select Sales Order</option>');
                    $.each(response['so'], function(i,v){
                        $('#so').append('<option value="'+v.sales_order_id+'">'+v.sales_order_number+" - "+v.note+'</option>');
                    });
                    $('#so').removeAttr('disabled');
                    $('.selectpicker').selectpicker('refresh');
                }
            });
        });

        $('#so').on('change',function(event){
            event.stopImmediatePropagation();
            var so = $(this).val();
            $.ajax({
                type:"GET",
                url:"getcreditdetails",
                data:{so:so},
                success:function(response){
                    console.log(response);
                    $('#append-payment_detail').html('');
                    $('#append-payment_detail').html(response['details']);
                    $('#paymenttype').val(response['so'].payment_type.payment_description)
                }
            });
        });

        $('#formPayment').on('keyup', '.bayar', function (ecebt) {
            event.stopImmediatePropagation();
            calculateTotalBayar();
        });

        $('#additionalCharge').on('keyup', function(event){
            $(this).val(accounting.unformat($(this).val()));
            $(this).val(accounting.formatMoney($(this).val(),'',0,',','.'));
            calculateTotalBayar();
        })

        $('#salesPaymentTable').on('click', 'tr', function(){
            var id = $(this).attr('value');
            $.ajax({
                type:"GET",
                url:"getsalespayment",
                data: {id:id},
                success:function(response){
                    $('#nopayment').val(response.payment.payment_sales_number).attr('disabled',true);
                    $('#customer').append('<option class="remove-when-clear" value="'+response.payment.so.customer_id+'" selected="selected">'+response.payment.so.customer_name+'</option>').attr('disabled',true);
                    $('#so').append('<option class="remove-when-clear" value="'+response.payment.so.sales_order_id+'" selected="selected">'+response.payment.so.sales_order_number+'</option>').attr('disabled',true);
                    $('#paymenttype').val(response.payment.so.payment_type.payment_description).attr('disabled',true);
                    $('#tglpayment').val(response.payment.date_sales_payment).attr('disabled',true);
                    $('#note').val(response.payment.note).attr('disabled',true);
                    $('#total').val(accounting.formatMoney(response.payment.total_paid,'Rp ',0,',','.')).attr('disabled',true);
                    $('#no-item').attr('hidden',true);
                    $('#print').removeAttr('hidden').attr('href','downloadsalespayment/'+response.payment.payment_sales_id);

                    var $paymentdiv = $('.metode-pembayaran-div:first');
                    $('.metode-pembayaran-div').remove();
                    for(var i = 0; i< response.payment.details.length; i++){
                        var $cloneddiv = $paymentdiv.clone();
                        var $newBayar = 'bayar-'+(i+1);
                        var $newMetode = 'metode-'+(i+1);

                        $cloneddiv.find('#bayar-1').attr('id',$newBayar).attr('disabled',true);
                        $cloneddiv.find('#metode-1').attr('id',$newMetode).attr('disabled',true);
                        $('#append-metode-pembayaran').append($cloneddiv);
                        $('#metode-'+(i+1)).val(response.payment.details[i].payment_method_id);
                        $('#bayar-'+(i+1)).val(response.payment.details[i].paid);
                        $cloneddiv.find('.bootstrap-select').replaceWith(function() { return $('select', this); });
                        $cloneddiv.find('select').selectpicker();
                    }
                    $('#additionalCharge').val(accounting.formatMoney(response.payment.additional_charge,'',0,',','.')).attr('disabled',true);
                    $('#additionalNote').val(response.payment.additional_note).attr('disabled',true);
                    $('#totalbayar').val(accounting.formatMoney(parseFloat(response.payment.total_paid )+ parseFloat(response.payment.additional_charge),'Rp ',0,',','.'));
                    hideDivRemaining();
                    hideDivReceiveable();
                    $('#append-payment_detail').html('');
                    $('#append-payment_detail').html(response['details']);
                    $('.selectpicker').selectpicker('refresh');
                    $('.submit').attr('hidden',true);
                }
            });
        });

        $('.submit').click(function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            mode = $(event.currentTarget).attr('mode');
            var kode = $('#nopayment').val();

            if($(event.currentTarget).attr('mode') == 'save')
            {
                var url = "createpayment";
                var successmessage = 'Pembayaran '+kode+' telah berhasil dibuat!';
            }else{
                var url = "updatepayment";
                var successmessage = 'Pembayaran '+kode+' telah berhasil diubah!';
            }


            //validasi apakah menggunakan saldo, kalau ada di cek apakah dia melebihi saldo pelanggan. kalau lebih valid saldo diubah false
            validsaldo = true;
            $('.metode-pembayaran-div').each(function () {
                if ($(this).find('#metode').val() == 2 && parseInt($(this).find('#bayar').val()) > parseInt($('#balance').val())) {
                    validsaldo = false;
                }
            });

            var totalbayar = 0;
            $('.bayar').each(function(){
                if(!isNaN(parseInt($(this).val())))
                {
                    totalbayar += parseInt($(this).val());
                }else{
                    totalbayar += 0;
                }
            });
            var total = $('#total-hidden').val();

            if($('#so').val() == 0){
                toastr.warning('Anda Belum Memilih SO!');
            }else if($('#total-hidden').val() == 0 || $('#total-hidden').val() == ""){
                toastr.warning('Anda Belum Memilih Invoice yang ingin dibayar!');
            }else if(totalbayar < total){
                toastr.warning('Jumlah Bayar harus sama / lebih besar dari Total!');
            }else if (validsaldo == false) {
                toastr.warning('Metode bayar saldo melebihi jumlah saldo pelanggan');
            }else{
                hidemainbuttongroup();
                $('#total-hidden').removeAttr('disabled');
                $('#additionalCharge').val(accounting.unformat($('#additionalCharge').val()));
                $.ajax({
                    type:"POST",
                    url:url,
                    data:$(this).closest('#formPayment').serialize(),
                    success:function(response){
                        window.open('downloadsalespayment/'+response, '_blank');
                        toastr.success(successmessage,{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                    }
                })
            }
        });

        $('#clear').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('.side-nav .nav-item.active a').click()
        });

        $('#pembayaran').on('focusout', function(){
            $('#pembayaran-hidden').val($(this).val());
            $(this).val(accounting.formatMoney($(this).val(),'Rp ',2,',','.'))
        });

        $('#pembayaran').on('focusin', function(){
            $(this).val("");
        });

        $('body').on('click','.checkbox', function(event){
            event.stopImmediatePropagation();
            var totalbayar = 0;
            var totalhutang = 0;
            $('.checkbox').each(function(i,v){
                if($(this).is(':checked')){
                    amount = $(this).closest('tr').find('.amount').val();
                    totalbayar += parseInt(amount);
                }else{
                    amount = $(this).closest('tr').find('.amount').val();
                    totalhutang += parseInt(amount);
                }
            });
            $('#total').val(accounting.formatMoney(totalbayar,'Rp ',2,'.',','));
            $('#total-hidden').val(totalbayar);
            $('#payable').val(accounting.formatMoney(totalhutang,'Rp ',2,'.',','));
        });

        $('#tambah-metode-pembayaran').on('click', function(event){
            event.stopImmediatePropagation();
            var $orginal = $('.metode-pembayaran-div:first');
            var $cloned = $orginal.clone().addClass('cloned');
            $cloned.find('.bayar').val("");
            $cloned.find('.add').remove();
            $cloned.find('.remove').css("display","inline-block");
            $cloned.find('.bootstrap-select').replaceWith(function() { return $('select', this); });
            $cloned.find('select').selectpicker();
            $cloned.appendTo('#append-metode-pembayaran');
        });

        $('#formPayment').on('click','.remove',function(event){
            event.stopImmediatePropagation();
            $(this).closest('.metode-pembayaran-div').remove();
            calculateTotalBayar();
        });

        $('#salesPaymentTable').on('click', '.delete-modal', function(event){
            event.stopImmediatePropagation();
            var nopayment = $(this).closest('tr').find('.nopayment').html();
            var idpayment = $(this).closest('tr').attr('value');
            $('#confirm-delete-payment').attr('value',idpayment).attr('nomor',nopayment);
            $("#delete-message").html("Yakin ingin menghapus data "+nopayment+" ?")
            $('#modal-delete-payment').modal('open');
        });

        $('#confirm-delete-payment').on('click', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-payment').modal('close');
            var id = $(this).attr('value');
            var nopayment = $(this).attr('nomor');
            $.ajax({
                type:"POST",
                url:"deletesalespayment",
                data:{id:id},
                success:function(response){
                    toastr.success('Sales Payment dengan '+nopayment+' telah berhasil Dihapus!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                }
            })
        });

        $('.copy-value').on('click',function(event){
            var nominal = accounting.unformat($(this).closest('.input-field').find('input[type="text"]:first').val(),',');
            $('.bayar:first').val(nominal).trigger('keyup');
        });

        //function
        function firstload(){

            $('#tglpayment').dcalendarpicker({
                format: 'dd-mm-yyyy'
            });
            $('#tglpayment').val(moment().format('DD-MM-YYYY'));

            $('.selectpicker').selectpicker('render');

            $.ajax({
                type:"GET",
                url:"lastsalespaymentnumber",
                success:function(response){
                    $('#nopayment').val(response);
                }
            });

            // salesPaymentTable = $('#salesPaymentTable').DataTable({ // This is for home page
            //     searching: true,
            //     responsive: true,
            //     'aaSorting':[],
            //     'sDom':'tip',
            //     "bPaginate":true,
            //     "bFilter": false,
            //     "sPaginationType": "full_numbers",
            //     "iDisplayLength": 10,
            //     "language": {
            //         "infoEmpty": "No records to display",
            //         "zeroRecords": "No records to display",
            //         "emptyTable": "No data available in table",
            //     },
            // });

            salesPaymentTable = $('#salesPaymentTable').DataTable({ // This is for home page
                searching: true,
                processing: true,
                serverSide: true,
                "aaSorting": [],
                'sDom': 'tip',
                "pageLength": 5,
                ajax: {
                    url : 'getsalespaymenttable',
                    data : function (d){
                        d.number = $('#filterPaymentNumber').val();
                        d.date = $('#filterTanggal').val();
                        d.pelanggan = $('#filterPelanggan').val();
                    }
                },
                rowId : 'payment_sales_id',
                columns: [
                    { data: 'payment_sales_number', name: 'payment_sales_number', class:'nopayment'},
                    { data: 'date_sales_payment', name: 'date_sales_payment'},
                    { data: 'so.customer_name', name: 'customer_name'},
                    { data: 'total_paid', name: 'total_paid'},
                    { data: 'action', name: 'action'},
                ],
            });

            $('#filterPaymentNumber').on('keyup', function () { // This is for news page
                salesPaymentTable.draw();
            });
            $('#filterPelanggan').on('keyup', function () { // This is for news page
                salesPaymentTable.draw();
            });
            $('#filterTanggal').on('keyup', function () { // This is for news page
                salesPaymentTable.draw();
            });
        }

        function calculateTotalBayar() {
            var totalpayment = $('#total-hidden').val()
            var total = 0;
            $('.bayar').each(function () {
                total += parseInt($(this).val());
            });
            var addcharge = parseFloat(accounting.unformat($('#additionalCharge').val()));
            $('#totalbayar').val(accounting.formatMoney(total + addcharge, 'Rp ', 2, '.', ','));
            $('#remaining').val(accounting.formatMoney(totalpayment - total, 'Rp ', 2, '.', ','));
        }

        function hidemainbuttongroup(){
            $('.main-button-group a').attr('hidden',true);
        }
        function hideDivRemaining(){
            $('#div-remaining').attr('hidden',true);
        }
        function showDivRemaining(){
            $('#div-remaining').removeAttr('hidden');
        }
        function hideDivReceiveable(){
            $('#div-receiveable').attr('hidden',true);
        }
        function showDivReceiveable(){
            $('#div-receiveable').removeAttr('hidden');
        }
    });
</script>