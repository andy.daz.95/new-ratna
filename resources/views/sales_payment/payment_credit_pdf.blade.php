<style type="text/css">
    table.table {
        border-collapse: collapse;
    }
    /*table.table th{*/
        /*border-top: 1px solid black;*/
        /*border-bottom: 1px solid black;*/
    /*}*/
    p , span{
        font-size: 12px;
        font-weight: 700;
    }
    body{
        margin:0px;
    }
    html { margin: 2cm 1cm 0cm 1cm}

</style>
<body>
<div style="">
    <table style ="width:100%">
        <tr>
            <td style="width: 2.5cm"><span></span></td>
            <td style="width: 11cm"><span>{{$payment->payment_sales_number}}</span></td>
            <td style="width: 2.2cm"><span></span></td>
            <td><span>{{$payment->date_sales_payment}}</span></td>
        </tr>
        <tr>
            <td ><span></span></td>
            <td ><span>{{$payment->invoice->so->customer_name}}</span></td>
            <td ><span></span></td>
            <td ><span>IDR (Rupiah)</span></td>
        </tr>
        <tr>
            <td ><span></span></td>
            <td ><span>{{$payment->invoice->so->customer->phone}}</span></td>
            <td ><span></span></td>
            <td ><span>{{$payment->details->count() == 1 ? $payment->details[0]->method->bank->bank_name.' '.$payment->details[0]->method->payment_method_type->description : 'Mixed Payment'}}</span></td>
        </tr>
        <tr>
            <td style="width: 2.5cm"><span></span></td>
            <td colspan="3"><span>{{$payment->invoice->so->customer->address}}</span></td>
        </tr>
    </table>
    <div>
        <p>Untuk Pembayaran : Cicilan {{$payment->invoice->product->category->description.' '.$payment->invoice->product->brand->name}} Ke {{$payment->invoice->term_number}}</p>
    </div>
    <table style="width:100%; clear: both; margin-top:10px" class="table table-bordered">
        <thead>
        <tr>
            <th class="color" style="text-align: center;"><span>No</span></th>
            <th class="color" style="text-align: center;"><span>Faktur No</span></th>
            <th class="color" style="text-align: center;"><span>Tanggal</span></th>
             <th class="color" style="text-align: center;"><span>Saldo Hutang</span></th>
            <th class="color" style="text-align: center;"><span>Dibayar</span></th>
            <th class="color" style="text-align: center;"><span>Sub Total</span></th>
        </tr>
        </thead>
        <tbody >
            <tr style="text-align: center">
                <td>
                    <span>{{1}}</span>
                </td>
                <td>
                    <span>{{$payment->invoice->invoice_sales_number}}</span>
                </td>
                <td>
                    <span>{{$payment->invoice->so->date_sales_order}}</span>
                </td>
                <td>
                    <span>{{number_format($listinvoice->where('payment_sales_id',null)->sum('total_amount') + $payment->invoice->total_amount)}}</span>
                </td>
                <td>
                    <span>{{number_format($payment->invoice->total_amount)}}</span>
                </td>
                <td>
                    <span>{{number_format($listinvoice->where('payment_sales_id',null)->sum('total_amount'))}}</span>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div style="position: fixed; top:62mm; left:120mm" >
  <p style="margin: 1.5mm; padding: 0">Additional Charge {{ $payment->additional_note ? "( ".$payment->additional_note." )" :''}} : {{number_format($payment->additional_charge)}}</p>
</div>
<div style="position: fixed; top:68mm; left:15mm">
  <p style="margin: 1.5mm; padding: 0">Tebilang : {{$terbilang}} Rupiah</p>
</div>
<div style="position: fixed; top:68mm; left:155mm">
    <p style="margin: 1.5mm; padding: 0">{{number_format($listinvoice->where('payment_sales_id',null)->sum('total_amount') + $payment->invoice->total_amount)}}</p>
    <p style="margin: 1.5mm; padding: 0">{{number_format($payment->invoice->total_amount)}}</p>
    <p style="margin: 1.5mm; padding: 0">{{number_format($listinvoice->where('payment_sales_id',null)->sum('total_amount'))}}</p>
    <p style="margin: 2.5mm 1.5mm 1.5mm 1.5mm; padding: 0">{{number_format($payment->overpaid)}}</p>
</div>
</body>
