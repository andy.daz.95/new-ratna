<table id="reportPiutangTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
    <th>No Invoice</th>
    <th>Customer</th>
    <th>Cicilan Ke</th>
    <th>Kode Barcode</th>
    <th>Pembayaran Masuk</th>
  </thead>
  <tbody>
    @foreach($report as $key => $value)
        <tr>
            <td>{{$value->invoice_sales_number}}</td>
            <td>{{$value->so->customer_name}}</td>
            <td>{{$value->term_number}} </td>
            <td>{{$value->product->bar_code}}</td>
            <td>{{number_format($value->total_amount)}}</td>
        </tr> 
    @endforeach
  </tbody>
</table>
<script type="text/javascript">
  reportPiutangTable = $('#reportPiutangTable').DataTable({ // This is for home page
    responsive: true,
    'sDom':'ti',
    "language": {
      "infoEmpty": "No records to display",
      "zeroRecords": "No records to display",
      "emptyTable": "No data available in table",
    },
  });
</script>