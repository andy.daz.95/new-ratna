<table id="reportTotalPoTable" class="highlight table table-bordered display nowrap dtr-inline margin-top">
    <tbody>
    @if(sizeOf($po) > 0)
        <tr>
            <td colspan="6">Total Nilai PO</td>
            <td id="total-po">{{number_format($po->sum('grand_total_idr'))}}</td>
        </tr>
        <tr>
            <td colspan="6">Total Pembayaran</td>
            <td id="total-payment">{{number_format($po->sum('totalpaid')+$po->sum('deposit_reduction'))}}</td>
        </tr>
        <tr>
            <td colspan="6"> Total Utang Tersisa</td>
            <td id="total-payable">{{number_format($po->sum('grand_total_idr') - ($po->sum('totalpaid')+$po->sum('deposit_reduction')))}}</td>
        </tr>
    @endif
    </tbody>
</table>
<table id="reportPoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
    <thead>
    <th>No PO</th>
    <th>Deskripsi</th>
    <th>Tanggal</th>
    <th>Supplier</th>
    <th>Status</th>
    <th>Total</th>
    <th>Payment</th>
    </thead>
    <tbody>
    @foreach($po as $key => $value)
        <tr id="{{$value->purchase_order_id}}" class="po-row">
            <td class="nopo">{{$value->purchase_order_number}}</td>
            <td>{{$value->notes}}</td>
            <td>{{$value->date_purchase_order}}</td>
            <td>{{$value->supplier->company_name}}</td>
            <td>{{$value->po_status}}</td>
            <td class="total">{{number_format($value->grand_total_idr)}}</td>
            <td class="payment">{{$value->totalpaid ? number_format($value->totalpaid + $value->deposit_reduction): '-' }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<script type="text/javascript">
      reportPoTable = $('#reportPoTable').DataTable({ // This is for home page
        responsive: true,
        'sDom':'ti',
        "language": {
          "infoEmpty": "No records to display",
          "zeroRecords": "No records to display",
          "emptyTable": "No data available in table",
        },
      });

      // reportTotalPoTable = $('#reportTotalPoTable').DataTable({ // This is for home page
      //   responsive: true,
      //   'sDom':'ti',
      //   "language": {
      //     "infoEmpty": "No records to display",
      //     "zeroRecords": "No records to display",
      //     "emptyTable": "No data available in table",
      //   },
      // });
</script>