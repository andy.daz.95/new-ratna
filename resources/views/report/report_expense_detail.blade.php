<table id="reportExpenseTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
    <th>Tanggal</th>
    <th>Kategori Biaya</th>
    <th>Deskripsi</th>
    <th>Jumlah</th>
    <th>Pengguna</th>
  </thead>
  <tbody>
    @foreach($report as $key => $value)
        <tr>
            <td>{{$value->date_expense}}</td>
            <td>{{$value->expense_category->expense_description}}</td>
            <td>{{$value->description}} </td>
            <td>{{number_format($value->nominal)}}</td>
            <td>{{$value->pengguna}}</td>
        </tr> 
    @endforeach
  </tbody>
</table>
<script type="text/javascript">
  reportPiutangTable = $('#reportExpenseTable').DataTable({ // This is for home page
    responsive: true,
    'sDom':'ti',
    "language": {
      "infoEmpty": "No records to display",
      "zeroRecords": "No records to display",
      "emptyTable": "No data available in table",
    },
  });
</script>