<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
    <div class="row">
        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Filter Report Sales Order</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <br>
                                <div class="input-field col l3">
                                    <label>Tahun</label>
                                    <select id="year" class="browser-default selectpicker" >

                                    </select>
                                </div>
                                <div class="input-field col l3">
                                    <label>bulan</label>
                                    <select id="month" class="browser-default selectpicker">
                                    </select>
                                </div>
                                <div class="input-field col l3">
                                    <label></label>
                                    <a id="preview" href="#" class="btn btn-raised blue white-text">preview</a>
                                    <a id="export" target="_blank" href="downloadprofitloss" class="btn btn-raised blue white-text" disabled>Export</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Formulir PO</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <br>
                                <div class="col l12 m12 s12">
                                </div>
                                <br>
                                <div class="col l12 m12 s12">
                                    <h4 id="report-title"></h4>
                                    <div id="report-table" class="table-responsive">
                                        @include('report.report_profit_loss_detail')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#btn-submit').attr('mode', 'save');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
            dataType: "script",
        });

        $('#preview').on('click', function(event){
            event.stopImmediatePropagation();
            var year = $('#year').val();
            var month = $('#month').val();
            $.ajax({
                method:"get",
                url:"reportprofitloss",
                data:{year:year, month:month},
                success:function(response){
                    $('#report-table').html(response);
                },
                complete:function(){
                    $('#export').attr('href','downloadreportprofitloss/'+year+'/'+month).removeAttr('disabled');
                }
            })
        });

        $('#year').on('change',function(event){
           event.stopImmediatePropagation();
            var date = moment();
            var year = date.year();
            var month = date.month();
            $('#month').html('');

            if($(this).val() == year){
                for (var i = 0; i <= month; i++)
                {
                    $('#month').append("<option value='"+(i+1)+"'>"+ moment().month(i).format('MMMM') +"</option");
                }
            }else{
                for (var i = 0; i <= 11; i++)
                {
                    $('#month').append("<option value='"+(i+1)+"'>"+ moment().month(i).format('MMMM') +"</option");
                }
            }

            $("#month").val(moment().month()+1);
            $('.selectpicker').selectpicker('refresh');
        });


        firstload();

        function firstload(){
            var date = moment();
            var year = date.year();
            var month = date.month();
            var minyear = date.subtract(5,'years').year();

            for (var i = year; i >= minyear; i--)
            {
                $('#year').append("<option value='"+i+"'>"+i+"</option");
            }

            $('.selectpicker').selectpicker('render');

            $('#year').trigger('change');
            calculatetotal();
            changetitle();
        }
        function changetitle(){
            $('#report-title').html("Report Profit Loss Periode "+moment($('#month').val(), 'M').format('MMMM')+' '+$('#year').val());
        }

        function calculatetotal(){
            var total = 0;
            var payment = 0;

            if($('.so-row').length > 0)
            {
                $('.total').each(function(){
                    if($(this).html() =='-') {
                        total += 0;
                    }else{
                        total += parseInt($(this).html());
                    }
                });

                $('.payment').each(function(){
                    if($(this).html() =='-') {
                        payment += 0;
                    }else{
                        payment += parseInt($(this).html());
                    }
                });

                $('#total-so').html(total);
                $('#total-payment').html(payment);
            }
        }
    });

</script>