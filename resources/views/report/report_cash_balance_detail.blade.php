<table id="reportExpenseTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
    <th>Account</th>
    <th>Debit</th>
    <th>Credit</th>
    <th>Saldo</th>
  </thead>
  <tbody>
    @foreach($metode as $key => $value)
        <tr>
            @php
                $debit = 0;
                foreach($paymentsales as $key2 => $value2)
                {
                    if($value2->details->where('payment_method_id',$value->payment_method_id)->count() > 0)
                    {
                        $debit += $value2->details->where('payment_method_id',$value->payment_method_id)->sum('paid');
                    }
                }
                $debit += $depositoclearence->where('payment_method_id',$value->payment_method_id)->sum('payment_amount');
                $credit = $paymentpurchase->where('payment_method_id',$value->payment_method_id)->sum('total_paid');
                + $expense->where('payment_method_id',$value->payment_method_id)->sum('nominal');
            @endphp

            <td>{{$value->bank->bank_name.' '.$value->payment_method_type->description}}</td>
            <td>{{number_format($debit)}}</td>
            <td>{{number_format($credit)}}</td>
            <td>{{number_format($debit - $credit)}}</td>
        </tr> 
    @endforeach
  </tbody>
</table>
<script type="text/javascript">
  reportPiutangTable = $('#reportExpenseTable').DataTable({ // This is for home page
    responsive: true,
    'sDom':'ti',
    "language": {
      "infoEmpty": "No records to display",
      "zeroRecords": "No records to display",
      "emptyTable": "No data available in table",
    },
  });
</script>