<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
    <div class="row">
        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Filter Report Sales Order</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <br>
                                <div class="input-field col l3">
                                    <label>Tanggal Awal</label>
                                    <input id="tglmulai" type="text" class="f-input" placeholder="Tanggal Awal">
                                </div>
                                <div class="input-field col l3">
                                    <label>Tanggal Akhir</label>
                                    <input id="tglakhir" type="text" class="f-input" placeholder="Tanggal Akhir">
                                </div>
                                <div class="input-field col l3">
                                    <label></label>
                                    <a id="preview" href="#" class="btn btn-raised blue white-text">preview</a>
                                    <a id="export" target="_blank" href="" class="btn btn-raised blue white-text" disabled>Export</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Formulir PO</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <br>
                                <div class="col l12 m12 s12">
                                </div>
                                <br>
                                <div class="col l12 m12 s12">
                                    <h4 id="report-title"></h4>
                                    <div id="report-table" class="table-responsive">
                                        @include('report.report_expense_detail')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#btn-submit').attr('mode', 'save');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
            dataType: "script",
        });

        $('#preview').on('click', function(event){
            event.stopImmediatePropagation();
            var start = $('#tglmulai').val();
            var end = $('#tglakhir').val();
            $.ajax({
                method:"get",
                url:"reportexpense",
                data:{start:start, end:end},
                success:function(response){
                    $('#report-table').html(response);
                },
                complete:function(){
                    $('#export').attr('href','downloadreportexpense/'+start+'/'+end).removeAttr('disabled');
                    changetitle();
                }
            })
        });


        firstload();

        function firstload(){
            $('#tglmulai, #tglakhir').dcalendarpicker({
                format: 'dd-mm-yyyy'
            });

            var date = moment();
            var month = date.month()+1;
            var year = date.year();
            var endofmonth = moment().endOf('month').format('DD');

            $('#tglmulai').val('01-'+month+'-'+year);
            $('#tglakhir').val(endofmonth+'-'+month+'-'+year);

            $('.selectpicker').selectpicker('render');

            changetitle();
            calculatetotal();
        }
        function changetitle(){
            $('#report-title').html("Report Expense Periode "+$('#tglmulai').val()+" - "+$('#tglakhir').val());
        }

        function calculatetotal(){
            var total = 0;
            var payment = 0;

            if($('.so-row').length > 0)
            {
                $('.total').each(function(){
                    if($(this).html() =='-') {
                        total += 0;
                    }else{
                        total += parseInt($(this).html());
                    }
                });

                $('.payment').each(function(){
                    if($(this).html() =='-') {
                        payment += 0;
                    }else{
                        payment += parseInt($(this).html());
                    }
                });

                $('#total-so').html(total);
                $('#total-payment').html(payment);
            }
        }
    });

</script>