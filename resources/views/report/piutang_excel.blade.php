<!DOCTYPE html>
<html>
<head>
    <title>Test</title>
</head>
<body>
<table id="reportSoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
    <tr>
        <td>Date</td>
        <td>{{Carbon\Carbon::now()->format('d-m-Y')}}</td>
    </tr>
    <tr></tr>
    <tr>
        <td>Kode Customer</td>
        <td>Name</td>
        <td>Kode Barcode</td>
        <td>Klasifikasi Barang</td>
        <td>Deskripsi Barang</td>
        <td>Harga Jual</td>
        <td>Jumlah Cicilan</td>
        <td>Cicilan Per Bulan</td>
        <td>Jumlah Cicilan Terbayar</td>
        <td>Total Bayar</td>
        <td>Saldo Terhutang</td>
        <td>Hutang Jatuh Tempo</td>
        <td>Purchase Date</td>
        <td>Last Jatuh Tempo</td>
        <td>Last Payment</td>
        <td>Aging</td>
        <td>Month Aging</td>
        <td>Status</td>
    </tr>
    <tbody>
    @foreach($report as $key => $value)
        <tr>
            <td>{{$value->so->customer_id}}</td>
            <td>{{$value->so->customer_name}}</td>
            <td>{{$value->product->bar_code}}</td>
            <td>{{$value->product->category->description}}</td>
            <td>{{$value->product->brand->name.' '.$value->product->type->name}}</td>
            <td>{{$value->product->price_sale}}</td>
            <td>{{$value->so->term_payment}}</td>
            <td>{{$value->total_amount}}</td>
            <td>{{$value->termpaid}}</td>
            <td>{{$value->totalpaid}}</td>
            <td>{{$value->totalreceiveable}}</td>
            <td>{{$value->hutangjatuhtempo}}</td>
            <td>{{$value->so->date_sales_order}}</td>
            <td>{{$value->lastduedate}}</td>
            <td>{{$value->lastpaymentdate}}</td>
            <td>{{Carbon\Carbon::createFromFormat('Y-m-d', $value->lastduedate)->diffInDays(\Carbon\Carbon::now(), false)}}</td>
            <td>{{Carbon\Carbon::createFromFormat('Y-m-d', $value->lastduedate)->diffInMonths(\Carbon\Carbon::now(), false)}}</td>
            <td>
                @if(Carbon\Carbon::createFromFormat('Y-m-d', $value->lastduedate)->diffInDays(\Carbon\Carbon::now(), false) < 0)
                    {{'0 Bulan'}}
                @elseif(Carbon\Carbon::createFromFormat('Y-m-d', $value->lastduedate)->diffInDays(\Carbon\Carbon::now(), false) < 30)
                    {{'1 Bulan'}}
                @elseif(Carbon\Carbon::createFromFormat('Y-m-d', $value->lastduedate)->diffInDays(\Carbon\Carbon::now(), false) < 60)
                    {{'1-2 Bulan'}}
                @elseif(Carbon\Carbon::createFromFormat('Y-m-d', $value->lastduedate)->diffInDays(\Carbon\Carbon::now(), false) < 90)
                    {{'2-3 Bulan'}}
                @elseif(Carbon\Carbon::createFromFormat('Y-m-d', $value->lastduedate)->diffInDays(\Carbon\Carbon::now(), false) < 180)
                    {{'3-6 Bulan'}}
                @elseif(Carbon\Carbon::createFromFormat('Y-m-d', $value->lastduedate)->diffInDays(\Carbon\Carbon::now(), false) < 360)
                    {{'6-12 Bulan'}}
                @else
                    {{'>1 Tahun'}}
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>