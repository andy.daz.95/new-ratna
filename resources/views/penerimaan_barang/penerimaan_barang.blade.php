<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
    <div class="row">
        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text"><i class="material-icons">search</i>Cari Penerimaan Barang</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row margin-top">
                                <div class="input-field col l3">
                                    <input id="filterPenerimaan" type="text" class="f-input">
                                    <label>Filter Penerimaan No</label>
                                </div>
                                <div class="input-field col l3">
                                    <input id="filterPo" type="text" class="f-input datepicker">
                                    <label>Filter PO</label>
                                </div>
                                <div class="input-field col l3">
                                    <input id="filterSupplier" type="text" class="f-input">
                                    <label>Filter Supplier</label>
                                </div>
                                <div class="col l12 m12 s12 margin-top">
                                    <div class="table-responsive">
                                        <table id="penerimaanTable" class="highlight table table-bordered display nowrap dataTable dtr-inline" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>No BPB</th>
                                                <th>No Purchase Order</th>
                                                <th>Tanggal</th>
                                                <th>Supplier</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {{--@foreach($data['bpb'] as $key => $value)--}}
                                                {{--<tr mode="view" value="{{$value->good_receive_id}}">--}}
                                                    {{--<td class="nobpb">{{$value->good_receive_code}}</td>--}}
                                                    {{--<td>{{$value->purchase_order_number.' / '.$value->po_note}}</td>--}}
                                                    {{--<td>{{$value->good_receive_date}}</td>--}}
                                                    {{--<td>{{$value->company_name}}</td>--}}
                                                    {{--<td>--}}
                                                         {{--<a class="btn btn-sm btn-raised light-blue darken-2 edit tooltipped" mode="edit" data-position="bottom" data-tooltip="delete" value="{{$value->good_receive_id}}"><i class="material-icons">edit</i></a>--}}
                                                         {{--<a class="btn btn-sm btn-raised red delete-modal tooltipped" data-position="bottom" data-tooltip="delete"><i class="material-icons">delete</i></a>--}}
                                                    {{--</td>--}}
                                                {{--</tr>--}}
                                            {{--@endforeach--}}
                                            </tbody>
                                        </table>
                                        </br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Formulir Penerimaan Barang</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <br>
                                <form id="formBpb">
                                    <div class="input-field col l3">
                                        <input id="bpbnumber" type="text" class="f-input" name="bpbnumber" disabled>
                                        <input id="idbpb" name="id" type="text" class="f-input" hidden>
                                        <label>No. Penerimaan Barang</label>
                                    </div>
                                    <div class="input-field col l3">
                                        <select id="nopo" name="po" class="selectpicker browser-default" data-live-search="true">
                                            <option value="0">Select PO</option>
                                            @foreach($data['po'] as $key => $value)
                                                <option value="{{$value->purchase_order_id}}">{{$value->purchase_order_number.' - '.$value->notes}}</option>
                                            @endforeach
                                        </select>
                                        <label>No Purchase Order</label>
                                    </div>
                                    <div class="input-field col l3">
                                        <input id="supplier" name="supplier" type="text" class="f-input" disabled>
                                        <label>Nama Supplier</label>
                                    </div>
                                    <div class="input-field col l3">
                                        <input id="tglbpb" type="text" name="tglbpb" class="f-input datepicker" data-maxdate="today">
                                        <label>Tanggal</label>
                                    </div>
                                    <div class="input-field col l3">
                                        <input id="notes" name="notes" type="text" class="f-input">
                                        <label>Notes</label>
                                    </div>
                                    <div class="col l12 m12 s12 margin-top">
                                        <div class="table-responsive">
                                            <table class="stoko-table no-border">
                                                <thead>
                                                <tr>
                                                    <th class="theader">Kategori Barang</th>
                                                    <th class="theader">Brand</th>
                                                    <th class="theader">Tipe Barang</th>
                                                    <th class="theader">Gudang</th>
                                                    <th class="theader">Receive</th>
                                                    <th class="theader">Cancel</th>
                                                </tr>
                                                </thead>
                                                <tbody id="barang-data">
                                                <tr id="no-item"><td colspan="6"><span> No Item Selected</span></td></tr>
                                                <tr class="barang-row" hidden>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="id-1" type="text" class="f-input id-detail" name="idbarang[]" hidden>
                                                            <input id="kategori-1" type="text" class="f-input" name="kategori[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="brand-1" type="text" class="f-input brand" name="brand[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="type-1" type="text" class="f-input type" name="type[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <select id="gudang-1" class="f-select gudang" name="gudang[]">
                                                                @foreach($data['warehouse'] as $key => $value)
                                                                    <option value="{{$value->warehouse_id}}">{{$value->warehouse_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" id="checkbox-received-1" class="filled-in received" value="" />
                                                        <label id="label-received-1" for="checkbox-received-1"></label>
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" id="checkbox-cancel-1" class="filled-in cancel" value="" />
                                                        <label id="label-cancel-1" for="checkbox-cancel-1"></label>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <br>
                                        <div class="input-field main-button-group right">
                                            <a id="clear" href="" class="btn-stoko btn-stoko-primary orange">Clear</a>
                                            <a id="edit-bpb" href="" mode="edit" class="btn-stoko btn-stoko-primary" hidden>Edit BPB</a>
                                            <a id="submit-bpb" href="" mode="save" class="btn-stoko btn-stoko-primary">Submit BPB</a>
                                            <a id="print-barcode" href="#" target="_blank" class="btn-stoko btn-stoko-primary" hidden>Print Barcode</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<!-- Modal Delete Delivery Order -->
<div id="modal-delete-bpb" class="modal">
    <div class="modal-content">
        <h5 id="delete-message"></h5>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
        <a id="confirm-delete-bpb" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
    </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn-submit').attr('mode', 'save');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/bootstrap-select.min.js',
            dataType: "script",
        });

        firstload();

        $('#nopo').on('change',function(){
            var id = $(this).val();
            $.ajax({
                type:"Get",
                url:"getpoforpenerimaan",
                data:{id:id},
                success:function(response){
                    var $original = $('.barang-row:first');
                    if (response.length == 0) {
                        $original.clone();
                        $('.barang-row').remove();
                        $original.attr('hidden',true);
                        $original.appendTo('#barang-data');
                        $('#formBpb').find("input[type=text], textarea").val("");
                        $('#no-item').removeAttr('hidden');
                        reset();
                    }else{
                        $('#supplier').val(response['podata'].s_name);
                        var $cloned = $original.clone();
                        $('.barang-row').remove();
                        for (var i=0; i<response['podetail'].length; i++)
                        {
                            var newid = "id-"+(i+1);
                            var newKategori = "kategori-"+(i+1);
                            var newBrand = "brand-"+(i+1);
                            var newType = "type-"+(i+1);
                            var newReceived = "checkbox-received-"+(i+1);
                            var newCancel = "checkbox-cancel-"+(i+1);
                            var newLabelReceived = "label-received-"+(i+1);
                            var newLabelCancel = "label-cancel-"+(i+1);
                            var newGudang = "gudang-"+(i+1);
                            $temp = $original.clone();
                            $temp.removeAttr('hidden');
                            $temp.find('#id-1').attr('id',newid);
                            $temp.find('#kategori-1').attr('id',newKategori);
                            $temp.find('#brand-1').attr('id',newBrand);
                            $temp.find('#type-1').attr('id',newType);
                            $temp.find('#checkbox-received-1').attr('id',newReceived);
                            $temp.find('#checkbox-cancel-1').attr('id',newCancel);
                            $temp.find('#label-received-1').attr('id',newLabelReceived).attr('for',"checkbox-received-"+(i+1));
                            $temp.find('#label-cancel-1').attr('id',newLabelCancel).attr('for',"checkbox-cancel-"+(i+1));
                            $temp.find('#gudang-1').attr('id',newGudang);
                            $temp.appendTo('#barang-data');
                            $('#id-'+(i+1)).val(response['podetail'][i].purchase_order_details_id).attr('disabled',true);
                            $('#kategori-'+(i+1)).val(response['podetail'][i].c_name).attr('disabled',true);
                            $('#brand-'+(i+1)).val(response['podetail'][i].b_name).attr('disabled',true);
                            $('#type-'+(i+1)).val(response['podetail'][i].t_name).attr('disabled',true);

                            if(response['podetail'][i].is_retur == 1)
                            {
                                $('#checkbox-cancel-'+(i+1)).attr('disabled',true);
                            }
                        }
                        $('#no-item').attr('hidden',true);

                    }
                }
            });
        });

        $('#penerimaanTable').on('click', 'tr, .edit', function(event){
            event.stopImmediatePropagation();
            id = $(this).attr('value');
            var mode = $(event.currentTarget).attr('mode');
            $.ajax({
                type:"GET",
                url:"getpenerimaan",
                data:{id:id},
                success:function(response){
                    $('#supplier').val(response.company_name);
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    $('#bpbnumber').val(response[0].good_receive_code).attr('disabled',true);
                    $('#idbpb').val(response[0].good_receive_id).attr('disabled',true);
                    $('#nopo').append('<option class="remove-when-clear" value="'+response[0].purchase_order_id+'" selected="selected">'+response[0].purchase_order_number+'</option>').attr('disabled',true);
                    $('#tglbpb').val(response[0].good_receive_date).attr('disabled',true);
                    $('#supplier').val(response[0].company_name).attr('disabled',true);
                    $('#notes').val(response[0].note).attr('disabled',true);
                    $('#no-item').attr('hidden',true);
                    $('#barang-row').removeAttr('hidden');
                    for (var i=0; i<response.length; i++)
                    {
                        var newid = "id-"+(i+1);
                        var newKategori = "kategori-"+(i+1);
                        var newBrand = "brand-"+(i+1);
                        var newType = "type-"+(i+1);
                        var newReceived = "checkbox-received-"+(i+1);
                        var newCancel = "checkbox-cancel-"+(i+1);
                        var newLabelReceived = "label-received-"+(i+1);
                        var newLabelCancel = "label-cancel-"+(i+1);
                        var newGudang = "gudang-"+(i+1);
                        $temp = $original.clone();
                        $temp.removeAttr('hidden');
                        $temp.find('#id-1').attr('id',newid);
                        $temp.find('#kategori-1').attr('id',newKategori);
                        $temp.find('#brand-1').attr('id',newBrand);
                        $temp.find('#type-1').attr('id',newType);
                        $temp.find('#checkbox-received-1').attr('id',newReceived);
                        $temp.find('#checkbox-cancel-1').attr('id',newCancel);
                        $temp.find('#label-received-1').attr('id',newLabelReceived).attr('for',"checkbox-received-"+(i+1));
                        $temp.find('#label-cancel-1').attr('id',newLabelCancel).attr('for',"checkbox-cancel-"+(i+1));
                        $temp.find('#gudang-1').attr('id',newGudang);
                        $temp.appendTo('#barang-data');
                        $('#id-'+(i+1)).val(response[i].purchase_order_details_id).attr('disabled',true);
                        $('#kategori-'+(i+1)).val(response[i].c_name).attr('disabled',true);
                        $('#brand-'+(i+1)).val(response[i].b_name).attr('disabled',true);
                        $('#type-'+(i+1)).val(response[i].t_name).attr('disabled',true);
                        $('#gudang-'+(i+1)).val(response[i].warehouse_id).attr('disabled',true);
                    }
                    $('.selectpicker').selectpicker('refresh');
                    $('.cancel, .received').attr('disabled',true);
                },complete:function(){
                    if(mode == "edit")
                    {
                        $('#edit-bpb').removeAttr('hidden');

                        $('#submit-bpb').attr('hidden',true);
                        $('#tglbpb, #notes').removeAttr('disabled');
                    }
                }
            })
        });

        $('#submit-bpb, #edit-bpb').click(function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('#bpbnumber,#idbpb, #nopo, .earnedqty').removeAttr('disabled');
            var kode = $('#bpbnumber').val();
            mode = $(event.currentTarget).attr('mode');

            //validasi checkbox receive dan cancel berbarenagan
            validcheckbox = [];
            $('.barang-row').each(function(k,v){
                if($(this).find('.received').is(':checked') && $(this).find('.cancel').is(':checked')) {
                    validcheckbox.push('false');
                }else{
                    validcheckbox.push('true');
                }
            });

            if(validcheckbox.indexOf('false') > -1){
                toastr.warning('Received dan Cancel tidak boleh di cek bersamaan pada 1 item')
            }else{
                hidemainbuttongroup()

                if($(event.currentTarget).attr('mode') == 'save')
                {
                    var url = "createpenerimaanbarang";
                    var successmessage = 'Penerimaan Barang '+kode+' telah berhasil dibuat!';
                }else{
                    var url = "updatepenerimaanbarang";
                    var successmessage = 'Penerimaan Barang '+kode+' telah berhasil diubah!';
                }

                var id = [];
                var cancel = [];
                var gudang = [];
                var po = $('#nopo').val();
                var nobpb = $('#bpbnumber').val();
                var idbpb = $('#idbpb').val();
                var tglbpb = $('#tglbpb').val();
                var notes = $('#notes').val();

                //get id po details dan gudang dari row yang di select
                $('.received').each(function(i,v){
                    if($(this).is(':checked'))
                    {
                        id.push($(this).closest('tr').find('.id-detail').val());
                        gudang.push($(this).closest('tr').find('.gudang').val());
                    }
                })

                //ajax request untuk create barang

                //check apakah ada barang yang di cancel, kalau ada ajax request untuk ganti status is_received nya
                $('.cancel').each(function(i,v){
                    if($(this).is(':checked'))
                    {
                        cancel.push($(this).closest('tr').find('.id-detail').val());
                    }
                });

                if(cancel.length > 0)
                {
                    $.ajax({
                        type:"POST",
                        async:"false",
                        url:'cancelbarang',
                        data:{cancel:cancel},
                        success:function(response){
                            toastr.success("cancel berhasil" , {"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                        }
                    })
                }

                if(id.length > 0 || mode == 'edit')
                {
                    $.ajax({
                        type:"POST",
                        url:url,
                        data:{id:id, gudang:gudang, po:po, idbpb:idbpb, nobpb:nobpb, tglbpb:tglbpb, notes:notes},
                        success:function(response){
                            if(mode == 'save')
                            {
                                $('#print-barcode').attr('href', 'excelgrbarcode/'+response).removeAttr('hidden');
                                window.open($('#print-barcode').attr('href'), '_blank');
                            }
                            toastr.success(successmessage, {"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                        }
                    })
                }
            }
        })

        $('#clear').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('.side-nav .nav-item.active a').click()
        });

        $('body').on('click','.delete-detail',function(event){
            event.stopImmediatePropagation();
            $(this).closest('tr').remove();
        });

        $('#penerimaanTable').on('click','.delete-modal', function(event){
            event.stopImmediatePropagation();
            var nobpb = $(this).closest('tr').find('.nobpb').html();
            var idbpb = $(this).closest('tr').attr('value');
            $('#confirm-delete-bpb').attr('value',idbpb).attr('nomor',nobpb);
            $("#delete-message").html("Yakin ingin menghapus data "+nobpb+" ?")
            $('#modal-delete-bpb').modal('open');
        });

        $('#confirm-delete-bpb').on('click', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-bpb').modal('close');
            var id = $(this).attr('value');
            var nobpb = $(this).attr('nomor');
            $.ajax({
                type:"POST",
                url:"deletepenerimaanbarang",
                data:{id:id},
                success:function(response){
                    toastr.success('Penerimaan Barang '+nobpb+' telah berhasil Dihapus!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                }
            })
        });

        function firstload(){
            $('#tglbpb').dcalendarpicker({
                format: 'dd-mm-yyyy'
            });

            $('#tglbpb').val(moment().format('DD-MM-YYYY'));

            $('.selectpicker').selectpicker('render');

            $.ajax({
                type:"GET",
                url:"lastbpbnumber",
                success:function(response){
                    $('#bpbnumber').val(response);
                }
            })

            $('.number').each(function(){
                $(this).html(accounting.formatMoney($(this).html(),'Rp ',2,',','.'));
            });

            // penerimaanTable = $('#penerimaanTable').DataTable({ // This is for home page
            //     searching: true,
            //     responsive: true,
            //     'aaSorting':[],
            //     'sDom':'tip',
            //     "bPaginate":true,
            //     "bFilter": false,
            //     "sPaginationType": "full_numbers",
            //     "iDisplayLength": 10,
            //     "language": {
            //         "infoEmpty": "No records to display",
            //         "zeroRecords": "No records to display",
            //         "emptyTable": "No data available in table",
            //     },
            // });


            penerimaanTable = $('#penerimaanTable').DataTable({ // This is for home page
                searching: true,
                processing: true,
                serverSide: true,
                "aaSorting": [],
                'sDom': 'tip',
                "pageLength": 5,
                ajax: {
                    url : 'getpenerimaantable',
                    data : function (d){
                        d.number = $('#filterPenerimaan').val();
                        d.po = $('#filterPo').val();
                        d.supplier = $('#filterSupplier').val();
                    }
                },
                rowId : 'good_receive_id',
                columns: [
                    { data: 'good_receive_code', name: 'good_receive_code', class:'nobpb'},
                    { data: 'purchase_order_number', name: 'purchase_order_number'},
                    { data: 'good_receive_date', name: 'good_receive_date'},
                    { data: 'company_name', name: 'company_name'},
                    { data: 'action', name: 'action'},
                ],
            });

            $('#filterPenerimaan').on('keyup', function () { // This is for news page
                penerimaanTable.draw();
            });
            $('#filterPo').on('keyup', function () { // This is for news page
                penerimaanTable.draw();
            });
            $('#filterSupplier').on('keyup', function () { // This is for news page
                penerimaanTable.draw();
            });
        }

        function reset()
        {
            $('#nopo').val(0);
            $('#supplier').val("");
            $('#notes').val("");
            $('#tglbpb').val(moment().format('DD-MM-YYYY'));
        }

        function hidemainbuttongroup(){
            $('.main-button-group a').attr('hidden',true);
        }
    });
</script>
