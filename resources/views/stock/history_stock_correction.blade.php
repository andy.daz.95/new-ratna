<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container-fluid padding-top">
    <ul class="collapsible" data-collapsible="accordion">
        <li>
            <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>
                Stock Barang Tesedia
            </div>
            <div class="collapsible-body">
                <br>
                <div class="container-fluid">
                    <div class="row">
                        <div class="row margin-zero">
                            <div class="input-field col l3">
                                <label style="margin-top: -10px">Filter Kategori Barang</label>
                                <input id="filterKategoriBarang" type="text" name="" class="f-input margin-zero" >
                            </div>
                            <div class="input-field col l3">
                                <label style="margin-top: -10px">Filter Brand Barang</label>
                                <input id="filterBrandBarang" type="text" name="" class="f-input margin-zero" >
                            </div>
                            <div class="input-field col l3">
                                <label style="margin-top: -10px">Filter Bar Code</label>
                                <input id="filterBarCode" type="text" name="" class="f-input margin-zero" >
                            </div>
                        </div>
                        <div class="container-fluid" style="padding: 10px 20px 20px 20px;">
                            <table id="HistoryStockCorectionTable" style="width: 100%" class="table highlight table-bordered display nowrap dataTable">
                                <thead>
                                <tr>
                                    <th>Kategori Produk</th>
                                    <th>Brand Produk</th>
                                    <th>Tipe Produk</th>
                                    <th>Barcode Barang</th>
                                    <th>Alasan</th>
                                    <th>Tanggal</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data['history'] as $key => $value)
                                    <tr value="{{$value->product_id}}">
                                        <td>{{$value->category->description}}</td>
                                        <td>{{$value->brand->name}}</td>
                                        <td>{{$value->type->name}}</td>
                                        <td>{{$value->bar_code}}</td>
                                        <td>{{$value->delete_notes}}</td>
                                        <td>{{$value->delete_date}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });

    $.ajax({
      url: 'js/custom.js',
      dataType: "script",
    });


    var hscTable = $('#HistoryStockCorectionTable').DataTable({ // This is for home page
        searching: true,
        responsive: true,
        'sDom': 'tip',
        "bPaginate": true,
        "bFilter": false,
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aaSorting": [],
        "language": {
            "infoEmpty": "No records to display",
            "zeroRecords": "No records to display",
            "emptyTable": "No data available in table",
        },
        "sPaginationType": "full_numbers",
        // "bInfo": false//Showing 1 to 1 of 1 entries (filtered from 7 total entries)
    });

    $('#filterKategoriBarang').on('keyup', function () { // This is for news page
      hscTable.column(0).search(this.value).draw();
    });
    $('#filterBrandBarang').on('keyup', function () { // This is for news page
        hscTable.column(1).search(this.value).draw();
    });
    $('#filterBarCode').on('keyup', function () { // This is for news page
        hscTable.column(3).search(this.value).draw();
    });

  });
</script>