<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/toastr.css">
<!-- Latest compiled and minified JavaScript -->

<!-- <link rel="stylesheet" href="bootstrap/css/bootstrap.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Stock Transfer</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <div class="input-field col l3">
                  <label>Filter Periode (Awal Periode)</label>
                  <input type="text" class="f-input datepicker" placeholder="Awal Periode" />
                </div>
                <div class="input-field col l3">
                  <label>Filter Periode (Akhir Periode)</label>
                  <input type="text" class="f-input datepicker" placeholder="Akhir Periode" />
                </div>
                <div class="input-field col l6">
                  <a href="#" class="btn btn-raised blue white-text">preview</a>
                </div>
              </div>
              <div class="row">
                <div class="input-field col l3">
                  <label>Filter PO</label>
                  <input id="findKode" type="text" class="f-input" placeholder="Filter PO" />
                </div>
                <div class="input-field col l3">
                  <label>Filter Tgl</label>
                  <input id="findPelanggan" type="text" class="f-input datepicker" placeholder="Filter Tgl" />
                </div>
                <div class="input-field col l3">
                  <label>Filter Pelanggan</label>
                  <input id="findSupplier" type="text" class="f-input" placeholder="Filter Pelanggan" />
                </div>
                <div class="col l12 m12 s12 margin-top">
                  <div class="table-responsive">
                    <table id="stockTransferTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                        <tr>
                          <th class="theader">No Stock Transfer</th>
                          <th class="theader">Tanggal</th>
                          <th class="theader">Gudang Dari</th>
                          <th class="theader">Gudang Ke</th>
                          <th class="theader">Modified by</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($data['stockmovement'] as $key => $value)
                          <tr value="{{$value->stock_movement_id}}">
                            <td>{{$value->stock_movement_number}}</td>
                            <td>{{$value->created_at}}</td>
                            <td>{{$value->warehouse_from}}</td>
                            <td>{{$value->warehouse_to}}</td>
                            <td>-</td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Formulir Stock Transfer</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <form id="formStocktransfer">
                <div class='row'>
                  <div class="input-field col l3 m12 s12">
                    <label>No Stock Transfer</label>
                    <input id="nostocktransfer" type="text" class="f-input" name="nostocktransfer" disabled>
                  </div>
                  <div class="input-field col l3 l3 m12 s12">
                    <label>Barang</label>
                    <select id="barang" class="browser-default selectpicker" data-live-search="true" name="barang">
                    <option value="0">Select Barang</option>
                      @foreach($data['barang'] as $key => $value)
                      <option value="{{$value->product_id}}">{{$value->product_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="input-field col l3 l3 m12 s12">
                    <label>Gudang Dari</label>
                    <select id="gudangfrom" class="browser-default selectpicker" data-live-search="true" name="gudangfrom">
                      @foreach($data['gudang'] as $key => $value)
                      <option value="{{$value->warehouse_id}}">{{$value->warehouse_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="input-field col l3 l3 m12 s12">
                    <label>Gudang Ke</label>
                    <select id="gudangto" class="selectpicker browser-default trigger-submit" data-live-search="true" name="gudangto">
                      @foreach($data['gudang'] as $key => $value)
                      <option value="{{$value->warehouse_id}}">{{$value->warehouse_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  </div>
                  <div class="input-field col l3 l3 m12 s12">
                    <label>Tgl Stock Transfer</label>
                    <input id="tglsm" type="text" class="f-input" name="tglsm" disabled>
                  </div>
                  <div class="col l12 m12 s12 margin-top">
                    <div class="table-responsive">
                      <table class="stoko-table no-border">
                        <thead>
                          <tr>
                            <th class="theader" style="width: 300px">Nama</th>
                            <th class="theader">Stock Gudang Dari</th>
                            <th class="theader">Stock Gudang To</th>
                            <th class="theader">Transfer Quantity</th>
                          </tr>
                        </thead>
                        <tbody id="barang-data">
                          <tr id="no-item"><td colspan="6"><span> No Item Selected</span></td></tr>
                          <tr class="barang-row" hidden>
                            <td>
                              <div class="input-field">
                                <input id="id" type="text" class="f-input "name="idbarang" hidden>
                                <input id="nama" type="text" class="f-input">
                              </div>
                            </td>
                            <td>
                              <div class="input-field">
                                <input id="from" type="text" class="f-input qty" name="from">
                              </div>
                            </td>
                            <td>
                              <div class="input-field">
                                <input id="to" type="text" class="f-input weight" name="to">
                              </div>
                            </td>
                            <td>
                              <div class="input-field">
                                <input id="qty" type="text" class="f-input harga" name="qty">
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="input-field right">
                      <a id="submit-ts" href="" class="btn-stoko btn-stoko-primary">Submit So</a>
                      <a id="cetak-so" href="" target="_blank" class="btn-stoko btn-stoko-primary orange darken-4" hidden>Cetak So</a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });

    $.ajax({
      url: 'js/custom.js',
      dataType: "script",
    });

    $.ajax({
      url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
      dataType: "script",
    });

    // $(".bootstrap-select").click(function (event) {
    //     event.preventDefault();
    //     event.stopImmediatePropagation();
    //     alert('test');
    //     $(this).addClass("open");
    //     $('.selectpicker').selectpicker('toogle');
    //     $(this).find('.dropdown-toggle').attr('aria-expanded',true);
    // });

    

    // $('#gudangfrom').change(function(event){
    //   event.stopImmediatePropagation();
    //   var idgudang = $(this).val();
    //   var temp = $('#gudangfrom').html();
    //   $('#gudangto').html(temp);
    //   $('#gudangto option').each(function (){
    //     if($(this).val() == idgudang)
    //     {
    //       $(this).remove();
    //     }
    //   })
    //   $("#gudangto option:selected").prop("selected", false);
    //   $("#gudangto option:first").prop("selected", "selected");
    //   getBarang();
    // })

    $('#gudangfrom').on('change', function(){
      var idgudang = $(this).val();
      var temp = $('#gudangfrom').html();
      $('#gudangto').html(temp);
      $('#gudangto option').each(function (){
        if($(this).val() == idgudang)
        {
          $(this).remove();
        }
      })
      $("#gudangto option:selected").prop("selected", false);
      $("#gudangto option:first").prop("selected", "selected");
      $('.selectpicker').selectpicker('refresh');
      getBarang();
    });

    firstload();
    // $('.selectpicker').selectpicker('render');

    $('.bootstrap-select>.dropdown-toggle').on('click',function(event){
      event.preventDefault();
      event.stopImmediatePropagation();
      $(this).closest('.bootstrap-select').addClass("open");
      $(this).attr('aria-expanded',true);
    })

    $('#barang').change(function(event){
      event.stopImmediatePropagation();
      getBarang();
    });

    $('#submit-ts').click(function(event){
      event.preventDefault();
      event.stopImmediatePropagation();
      $('#nostocktransfer, #tglsm').removeAttr('disabled');
      var kode = $('#nostocktransfer').val();
      $.ajax({
        type:"POST",
        url:"createstocktransfer",
        data:$(this).closest('#formStocktransfer').serialize(),
        success:function(response){
          toastr.success('Purchase Order '+kode+' has been Created!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
        }
      })
    })

    $('.highlight tr').on('click',function(event){
      event.stopImmediatePropagation();
      var id = $(this).attr('value');
      $.ajax({
        type:"GET",
        url:"getstocktransfer",
        data:{id:id},
        success:function(response){
          console.log(response);
          $('#barang').val(response.product_id).attr('disabled',true);
          $('#gudangfrom').val(response.warehouse_from).attr('disabled',true);
          $('#gudangfrom').trigger('change');
          $('#gudangto').val(response.warehouse_to).attr('disabled',true);
          $('#no-item').attr('hidden',true);
          $('#id').val(response.product_id);
          $('#nama').val(response.product_name).attr('disabled',true);
          $('#qty').val(response.quantity).attr('disabled',true);
          $('.barang-row').removeAttr('hidden');
          $('.selectpicker').selectpicker('refresh');
        }
      });
    })

    $('.trigger-submit').change(function(event){
      event.stopImmediatePropagation();
      getBarang();
    });

    // $('body').on('click','.bootstrap-select>.dropdown-toggle',function(event){
    //   event.stopPropagation();
    //   event.stopImmediatePropagation();
    //   alert('test');
    //   // $('#barang').selectpicker('toggle');
    // })

    //function
    function firstload()
    {
      $.ajax({
        type:"GET",
        url:"laststocktransfernumber",
        success:function(response){
          $('#nostocktransfer').val(response);
        }
      })
      $('#tglsm').dcalendarpicker({
        format: 'dd-mm-yyyy'
      });
      $('#tglsm').val(moment().format('DD-MM-YYYY'));

      $('#gudangfrom').trigger('change');
      $('.selectpicker').selectpicker('render');

      stockTransferTable = $('#stockTransferTable').DataTable({ // This is for home page
      searching: true, 
      responsive: true,
      'sDom':'tip',
      "bPaginate":true,
      "bFilter": false,
      "sPaginationType": "full_numbers",
      "iDisplayLength": 10,
      "language": {
        "infoEmpty": "No records to display",
        "zeroRecords": "No records to display",
        "emptyTable": "No data available in table",
      },
    });
    }

    function sortgudang(id){
      var temp = $('#gudangfrom').html(q4);
      $('#gudangto').html(temp);
      $('#gudangto option').each(function (){
        if($(this).val() == id)
        {
          $(this).remove();
        }
      })
      $("#gudangto option:selected").prop("selected", false);
      $("#gudangto option:first").prop("selected", "selected");
      $('.selectpicker').selectpicker('refresh');
    }

    function getBarang()
    {
      var id = $('#barang').val();
      if (id !== '0') 
      {
        $.ajax({
          type:"GET",
          url:"getBarangTransfer",
          data: $('#formStocktransfer').serialize()+"&id="+id,
          success:function(response){
            $('#no-item').attr('hidden',true);
            $('#id').val(response.product.product_id);
            $('#nama').val(response.product.product_name).attr('disabled',true);
            $('#from').val(response.qtyfrom).attr('disabled',true);
            $('#to').val(response.qtyto).attr('disabled',true);
            $('.barang-row').removeAttr('hidden');
          }
        });
      }
    }
  });
</script>