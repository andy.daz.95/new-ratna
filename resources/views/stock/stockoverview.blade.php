<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet"
      href="css/bootstrap-select.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
    <ul class="collapsible" data-collapsible="accordion">
        <li>
            <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>
                Stock Barang Tesedia
            </div>
            <div class="collapsible-body">
                <br>
                <div class="container-fluid">
                    <div class="row">
                        <br>
                        <div class="row">
                            <div class="input-field col l3">
                                <label style="margin-top: -10px">Filter Jenis barang</label>
                                <select id="filterStatus" name="status" class="browser-default selectpicker margin-zero status">
                                    <option value="1">Available</option>
                                    <option value="2">Selled</option>
                                </select>
                            </div>
                        </div>
                        <div class="input-field col l3">
                            <label style="margin-top: -10px">Filter Kategori Barang</label>
                            <input id="filterKategoriBarang" type="text" name="" class="f-input margin-zero">
                        </div>
                        <div class="input-field col l3">
                            <label style="margin-top: -10px">Filter Brand Barang</label>
                            <input id="filterBrandBarang" type="text" name="" class="f-input margin-zero">
                        </div>
                        <div class="input-field col l3">
                            <label style="margin-top: -10px">Filter Bar Code</label>
                            <input id="filterBarCode" type="text" name="" class="f-input margin-zero">
                        </div>
                        <div class="col l12 m12 s12">
                            <div class="table-responsive">
                                <table id="StockOverviewTable" class="table highlight table-bordered display nowrap dataTable">
                                    <thead>
                                    <tr>
                                        <th>Kategori </br> Produk</th>
                                        <th>Brand </br> Produk</th>
                                        <th>Tipe Produk</th>
                                        <th>No PO</th>
                                        <th>Barcode <br> Barang</th>
                                        @if(Session('roles')->name == 'master')
                                        <th>Harga Beli</th>
                                        @endif
                                        <th>Harga Jual</th>
                                        @if(Session('roles')->name == 'master')
                                        <th>Rate <br> Profit</th>
                                        @endif
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {{--@foreach($data['product'] as $key => $value)--}}
                                        {{--<tr value="{{$value->product_id}}">--}}
                                            {{--<td>{{$value->c_name}}</td>--}}
                                            {{--<td>{{$value->b_name}}</td>--}}
                                            {{--<td>{{$value->t_name}}</td>--}}
                                            {{--<td>{{$value->bar_code}}</td>--}}
                                            {{--<td>{{$value->price_buy}}</td>--}}
                                            {{--<td>{{$value->price_sale}}</td>--}}
                                            {{--<td>{{$value->profit_rate}}</td>--}}
                                            {{--<td>--}}
                                                {{--<a href="" class="btn btn-primary edit"><i class="material-icons">edit</i></a>--}}
                                                {{--<a href="printproduct/{{$value->product_id}}" target="_blank" class="btn btn-primary orange print-product"><i class="material-icons">print</i></a>--}}
                                                {{--<a href="#" class="btn btn-danger block-product"><i class="material-icons">block</i></a>--}}
                                            {{--</td>--}}
                                        {{--</tr>--}}
                                    {{--@endforeach--}}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
    <ul class="collapsible" data-collapsible="accordion">
        <li>
            <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>
                Stock Barang DiBlokir
            </div>
            <div class="collapsible-body">
                <br>
                <div class="container-fluid">
                    <div class="row">
                        <div class="container-fluid" style="padding: 10px 20px 20px 20px;">
                            <table id="BlockedProductTable" style="width: 100%" class="table highlight table-bordered display nowrap dataTable">
                                <thead>
                                <tr>
                                    <th>Kategori Produk</th>
                                    <th>Brand Produk</th>
                                    <th>Tipe Produk</th>
                                    <th>Barcode Barang</th>
                                    <th>Status</th>
                                    <th>Alasan</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data['blocked_product'] as $key => $value)
                                    <tr value="{{$value->product_id}}">
                                        <td>{{$value->category->description}}</td>
                                        <td>{{$value->brand->name}}</td>
                                        <td>{{$value->type->name}}</td>
                                        <td>{{$value->bar_code}}</td>
                                        <td>{{$value->status == 3 ? 'Blokir' : 'Hapus' }}</td>
                                        <td>
                                            @if($value->status == 3) {{--Block--}}
                                            {{$value->block_reason}}
                                            @elseif($value->status == 4)
                                            {{$value->delete_notes}}
                                            @endif
                                        </td>
                                        <td><a href="#" class="btn btn-primary light-blue unblock-product"><i class="material-icons">rotate_left</i></a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>
<div id="modal-block-product" class="modal">
    <div class="modal-content row">
        <div class="col l12">
            <h3><label for="">Konfirmasi</label></h3>
            <form id="formBlockProduct">
                <label id="block-product-message">Apakah Yakin akan menghapus <span id="block-product-detail"></span></label>
                <div class="input-field">
                    <label for="">Alasan</label>
                    <input type="text" name="reason" id="block-po-reason" class="f-input">
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect btn-stoko">Batal</a>
            <a id="confirm-block-product" class="modal-action modal-close waves-effect btn-stoko red white-text">Block</a>
        </div>
    </div>
</div>
<div id="modal-edit-product" class="modal">
    <div class="modal-content row">
        <div class="col l12">
            <h3><label for="">Edit Product</label></h3>
            <form id="formEditProduct">
                <div class="input-field">
                    <label for="producttype">Tipe Produk</label>
                    <select id="producttype" class="browser-default selectpicker" name="producttype">
                    </select>
                </div>
                <div class="input-field">
                    <label for="hargabeli">harga Jual</label>
                    <input type="text" id="hargabeli" class="f-input" name="hargabeli" disabled>
                </div>
                <div class="input-field">
                    <label for="hargajual">harga Jual</label>
                    <input type="text" id="hargajual" class="f-input" name="hargajual">
                </div>
                <div class="input-field">
                    <label for="rate">Rate</label>
                    <input type="text" id="rate" class="f-input" name="rate">
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect btn-stoko">Batal</a>
            <a id="update-product" class="modal-action modal-close waves-effect btn-stoko light-blue white-text">Update</a>
        </div>
    </div>
</div>
<div id="modal-unblock-product" class="modal">
    <div class="modal-content row">
        <div class="col l12">
            <h3><label for="">Konfirmasi</label></h3>
            <label id="unblock-product-message">Apakah Yakin akan menngembalikan <span id="unblock-product-detail"></span></label>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect btn-stoko">Batal</a>
            <a id="confirm-unblock-product" class="modal-action modal-close waves-effect btn-stoko light-blue white-text">Unblock</a>
        </div>
    </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });

        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $('.selectpicker').selectpicker('render');

        $('#StockOverviewTable').on('click', '.block-product', function (event) {
            event.stopImmediatePropagation();
            event.preventDefault();
            var id = $(this).closest('tr').attr('value');
            $.ajax({
                method: "get",
                url: "getbaranginfo",
                data: {id: id},
                success: function (response) {
                    $("#block-product-detail").html(response.product.category.description + " " + response.product.brand.name + " " + response.product.type.name + " dengan Barcode " + response.bar_code + " ?");
                    $("#confirm-block-product").attr('value', id);
                    $('#modal-block-product').modal('open');
                }
            })
        });

        $('#confirm-block-product').on('click', function (event) {
            event.stopImmediatePropagation();
            event.preventDefault();
            var id = $(this).attr('value');
            var reason = $('#formBlockProduct').find('#block-po-reason').val();
            $.ajax({
                method: "post",
                url: "blockproduct",
                data: {id: id, reason: reason},
                success: function (response) {
                    $('#modal-block-product').modal('close');
                    toastr.success('Berhasil Block Barang!',
                        {
                            "onShow": setTimeout(function () {
                                $('.side-nav .nav-item.active a').click();
                            }, 2600)
                        });
                }
            })
        });

        $('#StockOverviewTable').on('click','.edit', function (event) {
            event.stopImmediatePropagation();
            event.preventDefault();
            var id = $(this).closest('tr').attr('value');
            $.ajax({
                method: "get",
                url: "getbaranginfo",
                data: {id: id},
                success: function (response) {
                    $('#producttype').html('');
                    $.each(response['type'], function(i,v){
                        $('#producttype').append("<option value='"+v.product_type_id+"'>"+v.name+"</option>");
                    });
                    $('#hargabeli').val(response['product'].price_buy);
                    $('#hargajual').val(response['product'].price_sale);
                    $('#rate').val(response['product'].profit_rate);
                    $("#update-product").attr('value', id);
                    $('#modal-edit-product').modal('open');
                    $('.selectpicker').selectpicker('refresh');
                }
            })
        });

        $('#formEditProduct').on('keyup','#hargajual', function(event){
            calculateratebynominal();
        });
        $('#formEditProduct').on('keyup','#rate', function(event){
            calculatenominalbyrate();
        });

        $('#update-product').on('click', function(event) {
            var id = $(this).attr('value');
            $('#rate').removeAttr('disabled');
            $.ajax({
               method: "post",
               url: "updatebarangfromoverview",
               data: $('#formEditProduct').serialize()+'&id='+id,
                success: function (response) {
                    toastr.success('Berhasil Update Barang!',
                        {
                            "onShow": setTimeout(function () {
                                $('.side-nav .nav-item.active a').click();
                            }, 2600)
                        });
               }
           })
        });

        $('#BlockedProductTable').on('click', '.unblock-product' ,function (event) {
            event.stopImmediatePropagation();
            event.preventDefault();
            var id = $(this).closest('tr').attr('value');
            $.ajax({
                method: "get",
                url: "getbaranginfo",
                data: {id: id},
                success: function (response) {
                    $("#unblock-product-detail").html(response.product.category.description + " " + response.product.brand.name + " " + response.product.type.name + " dengan Barcode " + response.product.bar_code + " ?");
                    $("#confirm-unblock-product").attr('value', id);
                    $('#modal-unblock-product').modal('open');
                }
            })
        });

        $('#confirm-unblock-product').on('click', function (event) {
            event.stopImmediatePropagation();
            event.preventDefault();
            var id = $(this).attr('value');
            $.ajax({
                method: "post",
                url: "unblockproduct",
                data: {id: id},
                success: function (response) {
                    $('#modal-unblock-product').modal('close');
                    toastr.success('Berhasil Unblock Barang!',
                        {
                            "onShow": setTimeout(function () {
                                $('.side-nav .nav-item.active a').click();
                            }, 2600)
                        });
                }
            })
        });

        var role = '{{ Session('roles')->name }}';
        // define column untuk manager dan staff (staff gak bisa lihat harga beli dan profit rate)
        var colmaster = [
            { data: 'c_description', name: 'c_description'},
            { data: 'b_name', name: 'b_name'},
            { data: 't_name', name: 't_name'},
            { data: 'purchase_order_number', name: 'purchase_order_number'},
            { data: 'bar_code', name: 'bar_code'},
            { data: 'price_buy', name: 'price_buy'},
            { data: 'price_sale', name: 'price_sale'},
            { data: 'profit_rate', name: 'profit_rate'},
            { data: 'action', name: 'action'},
        ];

        var colstaff = [
            { data: 'c_description', name: 'c_description'},
            { data: 'b_name', name: 'b_name'},
            { data: 't_name', name: 't_name'},
            { data: 'purchase_order_number', name: 'purchase_order_number'},
            { data: 'bar_code', name: 'bar_code'},
            { data: 'price_sale', name: 'price_sale'},
            { data: 'action', name: 'action'},
        ];

        var stTable = $('#StockOverviewTable').DataTable({ // This is for home page
            searching: true,
            processing: true,
            serverSide: true,
            "aaSorting": [],
            'sDom': 'tip',
            "pageLength": 5,
            ajax: {
                url : 'getstockoverviewtable',
                data : function (d){
                    d.status = $('#filterStatus').val();
                    d.category = $('#filterKategoriBarang').val();
                    d.brand = $('#filterBrandBarang').val();
                    d.barcode = $('#filterBarCode').val();
                }
            },
            rowId : 'product_id',
            columns: role=='master'? colmaster : colstaff,
        });

        $('#filterKategoriBarang').on('keyup', function () { // This is for news page
            stTable.draw();
        });
        $('#filterBrandBarang').on('keyup', function () { // This is for news page
            stTable.draw();
        });
        $('#filterBarCode').on('keyup', function () { // This is for news page
            stTable.draw();
        });
        $('#filterStatus').on('change', function () { // This is for news page
            stTable.draw();
        });

        var bpTable = $('#BlockedProductTable').DataTable({ // This is for home page
            searching: true,
            responsive: true,
            'sDom': 'tip',
            "bPaginate": true,
            "bFilter": false,
            "sPaginationType": "full_numbers",
            "iDisplayLength": 10,
            "aaSorting": [],
            "language": {
                "infoEmpty": "No records to display",
                "zeroRecords": "No records to display",
                "emptyTable": "No data available in table",
            },
            "sPaginationType": "full_numbers",
            // "bInfo": false//Showing 1 to 1 of 1 entries (filtered from 7 total entries)
        });

        // function calculaterate() {
        //     var hargabeli = $('#hargabeli').val();
        //     var hargajual = $('#hargajual').val();
        //     if (hargabeli != "" && hargajual != "") {
        //         var rate = (hargajual - hargabeli) * 100 / hargabeli;
        //         $('#rate').val(parseFloat(rate).toFixed(2));
        //     }
        // }
        function calculateratebynominal() {
            var hargabeli = $('#hargabeli').val();
            var hargajual = $('#hargajual').val();
            var rate = (hargajual - hargabeli) * 100 / hargabeli;
            $('#rate').val(parseFloat(rate).toFixed(2));
        }

        function calculatenominalbyrate(){
            var hargabeli = $('#hargabeli').val();
            var rate = $('#rate').val();
            var hargajual = parseInt(hargabeli )+(hargabeli * rate / 100);
            $('#hargajual').val(hargajual);
        }

    });
</script>