@if(count($overall)>0)
@foreach($overall as $key => $value)
<tr class="ks-row">
	<td>{{$value->tcode}}</td>
	<td>{{$value->date}}</td>
	@if($value->stat == 'inout')
		<td class="in">{{$value->qty}}</td>
		<td class="out">{{$value->qty}}</td>
	@else
		@if($value->stat == 'in')
		<td class="in">{{$value->qty}}</td>
		@else
		<td class="in">0</td>
		@endif
		@if($value->stat == 'out')
		<td class="out">{{$value->qty}}</td>
		@else
		<td class="out">0</td>
		@endif
	@endif
	<td class="balance"></td>
</tr>
@endforeach
@else
<tr>
	<td colspan="0">No Data</td>
</tr>
@endif