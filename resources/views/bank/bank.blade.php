<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/toastr.css">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l4 m4 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">create</i>Form Bank</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <form id="formBank">
                <div class="row margin-top">
                  <div class="col l12">
                    <div class="row">
                      <div class="input-field col l12 m12 s12">
                        <input id="id" type="text" class="f-input" name="id" hidden>
                        <input id="bank" type="text" class="f-input" name="bank">
                        <label>Bank</label>
                      </div>
                    </div>
                    <div>Payment Method Section</div>
                    <div class="row">
                      @foreach($paymentmethodtype as $key => $value)
                        <div class="input-field col l12 m12 s12">
                          <input type="checkbox" class="checkbox" id="checkbox-{{$value->payment_method_type_id}}" value='{{$value->payment_method_type_id}}' name="type[]"/>
                          <label for="checkbox-{{$value->payment_method_type_id}}">{{$value->description}}</label>
                        </div>
                      @endforeach
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col l12 m12 s12">
                    <button id="btn-submit" type="submit" class="btn btn-raised btn-sm light-blue darken-2" mode="save">simpan</button>
                    <a href="#" class="btn btn-sm btn-raised grey lighten-4 grey-text text-darken-4 reset">cancel</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </li>
      </ul>
    </div>
    <div class="col l8 m8 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Bank</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <div class="input-field col l3">
                  <input id="findBank" type="text" class="f-input" placeholder="Search Bank">
                </div>
                <div class="col l12 m12 s12">
                  <div class="table-responsive">
                    <table id="bankTable" class="table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                        <tr>
                          <th class="theader">ID</th>
                          <th class="theader">Bank</th>
                          <th class="theader">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($bank as $key => $value)
                          <tr value="{{$value->bank_id}}">
                            <td>{{$value->bank_id}}</td>
                            <td>{{$value->bank_name}}</td>
                            <td>
                              <a class="btn btn-sm btn-raised light-blue darken-2 edit"><i class="material-icons">edit</i></a>
                              <a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<!-- Modal Tambah Bank -->
<div id="tambah_bank" class="modal">
  <div class="modal-content">
    <h4>Tambah Bank</h4>
    <div class="input-field">
      <input type="text" class="f-input" placeholder="Bank">
      <label>Bank</label>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a href="#!" onclick="Materialize.toast('Berhasil menambahkan bank', 4000)" class="modal-action modal-close waves-effect btn-stoko teal white-text">tambah</a>
  </div>
</div>

<!-- Modal Rubah Bank -->
<div id="rubah_bank" class="modal">
  <div class="modal-content">
    <h4>Rubah Bank</h4>
    <div class="input-field">
      <input type="text" class="f-input" placeholder="Bank" value="Mandiri">
      <label>Bank</label>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a href="#!" onclick="Materialize.toast('Berhasil merubah bank', 4000)" class="modal-action modal-close waves-effect btn-stoko teal white-text">simpan</a>
  </div>
</div>

<!-- Modal Delete Bank -->
<!-- Modal Delete Satuan -->
<div id="delete_data" class="modal">
  <div class="modal-content">
    <h5>Yakin ingin menghapus data berikut?</h5>
    <div class="row">
      <div class="col l12">
        <table id="modal-delete-data">
          
        </table>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <form>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat red white-text delete">Hapus</a>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat">cancel</a>
    </form>
  </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#btn-submit').attr('mode', 'save');
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: 'js/custom.js',
      dataType: "script",
    });
    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });

    bankTable = $('#bankTable').DataTable({ // This is for home page
      searching: true,
      responsive: true,
      'sDom': 'ti',
      'pagingType': 'full_numbers_no_ellipses',
      "language": {
        "infoEmpty": "No records to display",
        "zeroRecords": "No records to display",
        "emptyTable": "No data available in table",
      },
    });

    $('#findBank').on('keyup', function () { // This is for news page
      bankTable.search($(this).val()).draw();
    });

    $('#formBank').submit(function(event){
      event.preventDefault();
      var mode = $(this).find('#btn-submit').attr('mode');
      var empty = required(['bank']);
      if(empty != '0')
      {
        toastr.warning(empty+' tidak boleh kosong!');
      }else{
        if (mode == 'save') 
        {
          var id = $(this).find('#id').val();  
          $.ajax({
            type:"POST",
            url:"createBank",
            data: $("#formBank").serialize(),
            success:function(response){
              toastr.success('Bank telah berhasil dibuat!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
            }
          });  
        }else if(mode == 'edit'){
          var id = $(this).find('#id').val();  
          $.ajax({
            type:"POST",
            url:"updateBank",
            data: $("#formBank").serialize()+"&id="+id,
            success:function(response){
              toastr.success('Bank telah berhasil diubah!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
            }
          });
        }
      }
    });

    $('body').on('click','#formSatuan .reset', function(event){
      event.stopImmediatePropagation();
      $(this).closest('#formBank').find("input[type=text], textarea").val("");
      $('#btn-submit').attr('mode', 'save');
    });

    $('body').on('click','#bankTable .edit', function(event){
      event.stopImmediatePropagation();
      var id = $(this).closest('tr').attr('value');
      $.ajax({
        type:"GET",
        url:"getBankData",
        data: {id:id},
        success:function(response){
          $('#id').val(response.bank.bank_id);
          $('#bank').val(response.bank.bank_name);
          $.each(response.paymentmethod, function(i,v){
             $('#checkbox-'+v.payment_method_type.payment_method_type_id).attr('checked',true)
          });
        }
      });
      $('#btn-submit').attr('mode', 'edit');
    });

    $(".delete-modal").on('click',function(event){
      event.preventDefault();
      var id = $(this).closest('tr').attr('value');

      $.ajax({
        type:"GET",
        url:"modalBankData",
        async:false,
        data:{
          id:id,
        },
        success:function(response){
          $('#modal-delete-data').html(response);
        }
      });
      $('#delete_data').modal('open');
    });

    $(".delete").on('click',function(){
      var id = $(this).closest('.modal').find('#id-delete').attr('value');
      console.log(id);
      $.ajax({
        type:"Get",
        url:"deleteBank",
        data:{id:id},
        success:function(response){
          toastr.success('Berhasil Menghapus Bank!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
        }
      });
    });

  });
</script>