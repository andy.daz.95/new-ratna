<!DOCTYPE html>

<html>

<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta charset="utf-8">

    <meta name="csrf-token" content="{{csrf_token()}}">

    <title>Sistem Toko</title>

    <link rel="icon" href="icon.PNG">

    <link rel="stylesheet" href="css/materialize.css">

    {{-- <link rel="stylesheet" href="css/bootstrap.css"> --}}

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

    <link rel="stylesheet" href="js/dcalendar/dcalendar.picker.min.css">

    <link rel="stylesheet" href="css/master.css">

    <link rel="stylesheet" href="http://fonts.googleapis.com/icon?family=Material+Icons">

    <link rel="stylesheet" href="css/main.css">

    <link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">

    <link rel="stylesheet" href="css/prism.css">

    <link rel="stylesheet" href="css/font-awesome.min.css">

    <link rel="stylesheet" href="css/loaders.css">

</head>

<body>

<script src="{{ asset('js/jquery.min.js') }}"></script>

<script src="js/dcalendar/dcalendar.picker.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script type="text/javascript" src="js/jquery.newsTicker.min.js"></script>

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script src="js/bootstrap-select.min.js"></script>

<script src="js/materialize.min.js"></script>

<script src="js/moment.js"></script>

<script src="js/accounting.min.js"></script>

<script type="text/javascript" src="js/toastr.js"></script>

<script type="text/javascript" src="js/jquery.canvasjs.min.js"></script>

<script src="js/custom.js"></script>

<script src="js/loaders.css.js"></script>

@include('menu')

<div class="wrapper">
    @include('header')
    <div id="content" class="container-fluid padding-top">
        @yield('content')
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function()
    {
        $('.nav-item a').on('click', function(event){
            event.stopPropagation();
            $url = $(this).attr('url');

            $.ajax({
                type:'GET',
                url: $url,
                data :{},
                datatype:'html',
                success:
                    function(response){
                        $('#content').html(response);
                    }
            });
            $(this).closest('.side-nav').find('.nav-item.active').removeClass('active');
            $(this).closest('.nav-item').addClass('active');
        });
    });
</script>
</body>

</html>