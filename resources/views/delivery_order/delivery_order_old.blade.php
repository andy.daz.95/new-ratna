<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Delivery Order</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row margin-top">
                <div class="input-field col l3">
                  <input id="filterDoNumber" type="text" class="f-input">
                  <label>Filter DO</label>
                </div>
                <div class="input-field col l3">
                  <input id="filterPelanggan" type="text" class="f-input datepicker">
                  <label>Filter Pelanggan</label>
                </div>
                <div class="col l12 m12 s12 margin-top">
                  <div class="table-responsive">
                    <table id="deliveryTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                        <tr>
                          <th>No Delivery</th>
                          <th>No Sales Order</th>
                          <th>Tanggal</th>
                          <th>Pelanggan</th>
                          <th>Total</th>
                          <th>Sumber</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($do as $key => $value)
                          <tr value="{{$value->delivery_order_id}}">
                            <td class="nodo">{{$value->delivery_order_number}}</td>
                            <td>{{$value->sales_order_number}}</td>
                            <td>{{$value->date_delivery}}</td>
                            <td>{{$value->company_name}}</td>
                            <td class="number">{{$value->grand_total_idr}}</td>
                            @if($value->delivery_type_id == 1)
                            <td>SO</td>
                            @else
                            <td>Retur</td>
                            @endif
                            <td>
                              <a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="{{$value->delivery_order_id}}"><i class="material-icons">edit</i></a>
                              <a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Formulir Delivery Order</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <form id="formDo">
                  <div class="input-field col l3">
                    <input id="donumber" type="text" class="f-input" name="donumber" disabled>
                    <input id="iddo" type="text" class="f-input" name="iddo" disabled hidden> 
                    <label>No. Do</label>
                  </div>
                  <div class="input-field col l3">
                    <select id="so" name="so" class="selectpicker browser-default" data-live-search="true">
                      <option value="0">Select SO</option>
                      @foreach($so as $key =>$value)
                        <option value="{{$value->sales_order_id}}">{{$value->sales_order_number}}</option>
                      @endforeach
                    </select>
                    <label>Sales Order ID</label>
                  </div>
                  <div class="input-field col l3">
                    <select id="shippingterm" name="shippingterm" class="selectpicker browser-default" data-live-search="true">
                      <option value="0">Select Shipping Term</option>
                      @foreach($shippingterm as $key => $value)
                        <option value="{{$value->shipping_term_id}}">{{$value->shipping_term_name}}</option>
                      @endforeach
                    </select>
                    <label>Shipping Term</label>
                  </div>
                  <div class="input-field col l3">
                    <input id="status" type="text" name="status" class="f-input">
                    <label>Status Pembayaran</label>
                  </div>
                  <div class="input-field col l3">
                    <input id="pelanggan" name="pelanggan" type="text" class="f-input" disabled>
                    <label>Nama Pelanggan</label>
                  </div>
                  <div class="input-field col l3">
                    <input id="tgldo" type="text" name="tgldo" class="f-input datepicker">
                    <label>Tanggal</label>
                  </div>
                  <div class="input-field col l3">
                    <select id="gudang" type="text" name="gudang" class="selectpicker browser-default" data-live-search="true" disabled>
                      <option value="0">Pilih Gudang</option>
                      @foreach($warehouse as $key => $value)
                        <option value="{{$value->warehouse_id}}">{{$value->warehouse_name}}</option>
                      @endforeach
                    </select>
                    <label>Gudang</label>
                  </div>
                  <div class="input-field col l3">
                    <input id="notes" name="notes" type="text" class="f-input">
                    <label>Notes</label>
                  </div>
                  <div class="col l12 m12 s12 margin-top">
                    <div class="table-responsive">
                      <table class="stoko-table no-border">
                        <thead>
                          <tr>
                            <th class="theader">Nama Barang</th>
                            <th class="theader">Order Quantity</th>
                            <th class="theader" style="width:75px">Delivered Quantity</th>
                            <th class="theader">Stock In Warehouse</th>
                            <th class="theader" style="width:75px">Sent Quantity</th>
                            <th class="theader" colspan="3">Action</th>
                          </tr>
                        </thead>
                        <tbody id="barang-data">
                          <tr id="no-item"><td colspan="6"><span> No Item Selected</span></td></tr>
                          <tr class="barang-row" hidden>
                            <td>
                              <div class="input-field">
                                <input id="id-1" type="text" class="f-input idbarang" name="idbarang[]" hidden>
                                <input id="nama-1" type="text" class="f-input" name="namabarang[]">
                              </div>
                            </td>
                            <td>
                              <div class="input-field">
                                <input id="order-qty-1" type="text" class="f-input orderqty" name="orderqty[]">
                              </div>
                            </td>
                            <td>
                              <div class="input-field">
                                <input id="delivered-qty-1" type="text" class="f-input deliveredqty" name="deliveredqty[]">
                              </div>
                            </td>
                            <td>
                              <div class="input-field">
                                <input id="stock-1" type="text" class="f-input stock" name="stock[]">
                              </div>
                            </td>
                            <td>
                              <div class="input-field">
                                <input id="send-qty-1" type="text" class="f-input sendqty" name="sendqty[]">
                              </div>
                            </td>
                            <td>
                              <p>
                                <input type="checkbox" class="filled-in" id="filled-in-box"/>
                                <label for="filled-in-box"></label>
                              </p>
                            </td>
                            <td>
                              <a href="#" class="btn btn-sm white grey-text"><i class="material-icons">create</i></a>
                            </td>
                            <td>
                              <a href="#" class="btn btn-sm white grey-text"><i class="material-icons">delete</i></a>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="input-field right">
                      <a id="clear" href="" class="btn-stoko orange">Clear</a>
                      <a id="cetak-do" href="" target="_blank" class="btn-stoko btn-stoko-primary orange darken-4" hidden>Cetak DO</a>
                      <a id="submit-do" href="" class="btn-stoko btn-stoko-primary" mode="save">Submit DO</a>
                      <a id="edit-do" href="" class="btn-stoko btn-stoko-primary" mode="edit" hidden>Edit DO</a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<div id="modal-tambah-gudang" class="modal">
  <form id="form-list-gudang">
    <div class="modal-content">
      <h4>Pilih Gudang</h4>
      <div class="row">
        <table>
          <thead>
            <th>Nama</th>
            <th>Qty</th>
            <th>Gudang</th>
            <th>check</th>
          </thead>
          <tbody id="modal-barang-data">
          </tbody>
        </table>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect btn-flat red white-text no-border-radius submit-barang">OK</a>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat orange white-text no-border-radius">batal</a>
    </div>
  </form>
</div>

<!-- Modal Tambah Delivery Order -->
<div id="tambah_do" class="modal">
  <div class="modal-content">
    <h5>Tambah Delivery Order</h5>
    <form>
      <div class="row margin-top">
        <div class="col l6">
          <div class="row">
            <div class="input-field col l12 m12 s12">
              <select class="f-select browser-default">
                <option value="">Select Order ID</option>
                <option value="">Order 001</option>
                <option value="">Order 002</option>
                <option value="">Order 003</option>
              </select>
              <label>Order ID</label>
            </div>
            <div class="input-field col l12 m12 s12">
              <input type="text" class="f-input">
              <label>Status Pembayaran</label>
            </div>
          </div>
        </div>
        <div class="col l6">
          <div class="row">
            <div class="input-field col l6 m12 s12">
              <input type="text" class="f-input">
              <label>Nama Barang</label>
            </div>
            <div class="input-field col l6">
              <input type="text" class="f-input datepicker">
              <label>Tanggal</label>
            </div>
            <div class="input-field col l12 m12 s12">
              <input type="text" class="f-input">
              <label>Notes</label>
            </div>
          </div>
        </div>
        <div class="col l12 m12 s12">
          <div class="table-responsive">
            <table class="stoko-table">
              <tr>
                <th class="white">Nama Barang</th>
                <th class="white">Qty</th>
                <th class="white">Berat</th>
                <th class="white">Notes</th>
              </tr>
              <tr>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
      <a href="#!" onclick="Materialize.toast('Berhasil menambahkan delivery order', 4000)" class="modal-action modal-close waves-effect btn-stoko teal white-text">tambah</a>
    </div>
  </form>
</div>

<!-- Modal Rubah Delivery Order -->
<div id="rubah_do" class="modal">
  <div class="modal-content">
    <h5>Rubah Delivery Order</h5>
    <form>
      <div class="row margin-top">
        <div class="col l6">
          <div class="row">
            <div class="input-field col l12 m12 s12">
              <select class="f-select browser-default">
                <option value="">Select Order ID</option>
                <option value="">Order 001</option>
                <option value="">Order 002</option>
                <option value="">Order 003</option>
              </select>
              <label>Order ID</label>
            </div>
            <div class="input-field col l12 m12 s12">
              <input type="text" class="f-input">
              <label>Status Pembayaran</label>
            </div>
          </div>
        </div>
        <div class="col l6">
          <div class="row">
            <div class="input-field col l6 m12 s12">
              <input type="text" class="f-input">
              <label>Nama Pelanggan</label>
            </div>
            <div class="input-field col l6">
              <input type="text" class="f-input datepicker">
              <label>Tanggal</label>
            </div>
            <div class="input-field col l12 m12 s12">
              <input type="text" class="f-input">
              <label>Notes</label>
            </div>
          </div>
        </div>
        <div class="col l12 m12 s12">
          <div class="table-responsive">
            <table class="stoko-table">
              <tr>
                <th class="white">Nama Barang</th>
                <th class="white">Qty</th>
                <th class="white">Berat</th>
                <th class="white">Notes</th>
              </tr>
              <tr>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
      <a href="#!" class="modal-action waves-effect btn-stoko btn-stoko-primary">cetak</a>
      <a href="#!" onclick="Materialize.toast('Berhasil merubah barang', 4000)" class="modal-action modal-close waves-effect btn-stoko teal white-text">simpan</a>
    </div>
  </form>
</div>

<!-- Modal Delete Delivery Order -->
<div id="modal-delete-do" class="modal">
  <div class="modal-content">
    <h5 id="delete-message"></h5>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a id="confirm-delete-do" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
  </div>
</div>

<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#btn-submit').attr('mode', 'save');
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });
    $.ajax({
      url: 'js/custom.js',
      dataType: "script",
    });

    $.ajax({
      url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
      dataType: "script",
    });

    firstload();
    
    $('#so').on('change',function(){
      var id = $(this).val();
      $.ajax({
        type:"Get",
        url:"getsofordelivery",
        data:{id:id},
        success:function(response){
          var $original = $('.barang-row:first');
          var $cloned = $original.clone();
          $('.barang-row').remove();
          if(response.length == 0)
          {
            $original.clone();
            $original.attr('hidden',true)
            $original.appendTo('#barang-data');
            $('#formDo').find("input[type=text], textarea").val("");
            $('#no-item').removeAttr('hidden');
          }
          else{
            $('#pelanggan').val(response['sodata'].c_name);
            for (var i=0; i<response['sodetail'].length; i++)
            {
              var newid = "id-"+(i+1);
              var newnama = "nama-"+(i+1);
              var neworderqty = "order-qty-"+(i+1);
              var newdeliveredqty = "delivered-qty-"+(i+1);
              var newsendqty = "send-qty-"+(i+1);
              var newstock = "stock-"+(i+1);
              var newnote = "note-"+(i+1);
              var newharga = "harga-"+(i+1);
              $temp = $original.clone();
              $temp.removeAttr('hidden');
              $temp.find('#id-1').attr('id',newid);
              $temp.find('#nama-1').attr('id',newnama);
              $temp.find('#send-qty-1').attr('id',newsendqty);
              $temp.find('#delivered-qty-1').attr('id',newdeliveredqty);
              $temp.find('#order-qty-1').attr('id',neworderqty);
              $temp.find('#stock-1').attr('id',newstock);
              $temp.find('#note-1').attr('id',newnote);
              $temp.find('#harga-1').attr('id',newharga);
              $temp.appendTo('#barang-data');
              $('#id-'+(i+1)).val(response['sodetail'][i].product_id);
              $('#nama-'+(i+1)).val(response['sodetail'][i].p_name).attr('disabled',true);
              $('#order-qty-'+(i+1)).val(response['sodetail'][i].quantity).attr('disabled',true);
              $('#stock-'+(i+1)).val(0).attr('disabled',true);
              if(response['dodetail'].length == 0)
              {
                $('#delivered-qty-'+(i+1)).val(0).attr('disabled',true);
              }
              else{
                $('#delivered-qty-'+(i+1)).val(response['dodetail'][i].sum).attr('disabled',true);
              }
              $('#send-qty-'+(i+1)).val($('#order-qty-'+(i+1)).val() - $('#delivered-qty-'+(i+1)).val());
              if($('#delivered-qty-'+(i+1)).val() == $('#order-qty-'+(i+1)).val() ) {
                $('#send-qty-'+(i+1)).val(0).attr('disabled',true);
                $('#stock-'+(i+1)).attr('disabled',true);
                $('#note-'+(i+1)).attr('disabled',true);
              }
              /*$('#harga-'+(i+1)).val(response.harga[i]).attr('disabled',true);*/
            }
            $('#gudang').removeAttr('disabled');
            $('#no-item').attr('hidden',true);
            $('.barang-row').removeAttr('hidden');
            $('.selectpicker').selectpicker('refresh');
          }
        }
      });
    });

    $('#submit-do, #edit-do').click(function(event){
      event.preventDefault();
      event.stopImmediatePropagation();
      var kode = $('#donumber').val();

      if($(event.currentTarget).attr('mode') == 'save')
      {
        var url = "createdeliveryorder";
        var successmessage = 'Delivery Order '+kode+' telah berhasil dibuat!';
      }else{
        var url = "updatedeliveryorder";
        var successmessage = 'Delivery Order '+kode+' telah berhasil diubah!';
      }

      var validqty = [];
      var lebihqty = [];
      var stockkurang = [];
      $('.barang-row').each(function(){
        var deliveredqty = parseInt($(this).find('.deliveredqty').val());
        var sendqty = parseInt($(this).find('.sendqty').val());
        var orderqty = parseInt($(this).find('.orderqty').val());
        var stock = parseInt($(this).find('.stock').val());
        validqty.push(ceknumber($(this).find('.sendqty').val()));
        if(deliveredqty + sendqty > orderqty){
          lebihqty.push(false);
        }else{
          lebihqty.push(true);
        }

        if(sendqty > stock){
          stockkurang.push(false)
        }else{
          stockkurang.push(true)
        }
      });

      if($('#so').val() == 0){
        toastr.warning('Anda Belum Memilih SO!');
      }else if($('#gudang').val() == 0){
        toastr.warning('Anda Belum Memilih Gudang!');
      }else if($('#shippingterm').val() == 0){
        toastr.warning('Anda Belum Memilih Shipping Term!');
      }else if(validqty.indexOf(false) > -1){
        toastr.warning('Quantity harus berupa angka!');
      }else if(lebihqty.indexOf(false) > -1 && $(event.currentTarget).attr('mode') == "save"){
        toastr.warning('Quantity melebihi jumalh order!');
      }else if(stockkurang.indexOf(false) > -1){
        toastr.warning('Stock Di Gudang Tidak Mencukupi!');
      }else{
        $('#donumber, #tgldo , #iddo , .sendqty, .weight, #gudang').removeAttr('disabled');
        $.ajax({
          type:"POST",
          url:url,
          data:$(this).closest('#formDo').serialize(),
          success:function(response){
            console.log(response);
            toastr.success(successmessage,{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
          }
        })
      }
    })

    $('#gudang').on('change', function(){
      var idgudang = $(this).val();
      var idproduct = [];
      $('.idbarang').each(function(i){
        idproduct.push($(this).val());
      });
      $.ajax({
        type:"GET",
          url:"getStockInWarehouse",
          data: {idproduct:idproduct, idgudang:idgudang},
          success:function(response){
            for (var i=0; i<response.length; i++)
            {
              $('#stock-'+(i+1)).val(response[i].quantity).attr('disabled',true);
            }
          }
      })
    });

    $('#deliveryTable tr, .edit').on('click', function(event){
      event.stopImmediatePropagation();
      var mode = $(event.currentTarget).attr('mode');
      id = $(this).attr('value');
      $.ajax({
        type:"GET",
        url:"getdeliveryorder",
        data:{id:id},
        success:function(response){
          $('#pelanggan').val(response.company_name);
          var $original = $('.barang-row:first');
          var $cloned = $original.clone();
          $('.barang-row').remove();
          /*name="donumber"name="so"name="shippingterm"name="status"name="pelanggan"name="tgldo"name="notes"*/ 
          $('#donumber').val(response[0].delivery_order_number).attr('disabled',true);
          $('#iddo').val(response[0].delivery_order_id).attr('disabled',true);
          $('#so').append('<option class="remove-when-clear" value="'+response[0].sales_order_id+'" selected="selected">'+response[0].sales_order_number+'</option>').attr('disabled',true);
          $('#gudang').val(response[0].warehouse_id).attr('disabled',true);
          $('#tgldo').val(response[0].date_delivery).attr('disabled',true);
          $('#pelanggan').val(response[0].company_name).attr('disabled',true);
          $('#shippingterm').val(response[0].shipping_term_id).attr('disabled',true);
          for (var i=0; i<response.length; i++)
          {
            var newid = "id-"+(i+1);
            var newnama = "nama-"+(i+1);
            var neworderqty = "order-qty-"+(i+1);
            var newdeliveredqty = "delivered-qty-"+(i+1);
            var newsendqty = "send-qty-"+(i+1);
            var newstock = "stock-"+(i+1);
            var newweight = "weight-"+(i+1);
            var newnote = "note-"+(i+1);
            var newharga = "harga-"+(i+1);
            $temp = $original.clone();
            $temp.removeAttr('hidden');
            $temp.find('#id-1').attr('id',newid);
            $temp.find('#nama-1').attr('id',newnama);
            $temp.find('#send-qty-1').attr('id',newsendqty);
            $temp.find('#stock-1').attr('id',newstock);
            $temp.find('#delivered-qty-1').attr('id',newdeliveredqty);
            $temp.find('#order-qty-1').attr('id',neworderqty);
            $temp.find('#weight-1').attr('id',newweight);
            $temp.find('#note-1').attr('id',newnote);
            $temp.find('#harga-1').attr('id',newharga);
            $temp.appendTo('#barang-data');
            $('#id-'+(i+1)).val(response[i].product_id);
            $('#nama-'+(i+1)).val(response[i].product_name).attr('disabled',true);
            $('#order-qty-'+(i+1)).val(response[i].quantity).attr('disabled',true);
            $('#stock-'+(i+1)).val(0).attr('disabled',true);
            $('#delivered-qty-'+(i+1)).val(response[i].total_sent).attr('disabled',true);
            $('#weight-'+(i+1)).val(response[i].weight).attr('disabled',true);
            $('#send-qty-'+(i+1)).val(response[i].quantity_sent).attr('disabled',true);
          }
          $('.selectpicker').selectpicker('refresh');
          $('#no-item').attr('hidden',true);
          $('#barang-row').removeAttr('hidden');
          $('#gudang').trigger('change');
        },complete:function(){
          $('#submit-do').attr('hidden',true);
          $('#cetak-do').removeAttr('hidden').attr('href',"downloaddeliveryorder/"+id);
          if(mode == "edit")
          {
            $('#edit-do').removeAttr('hidden');
            $('#tgldo , .sendqty').removeAttr('disabled');
          }
        }
      })
    });

    $('#clear').on('click', function(event){
      event.preventDefault();
      event.stopImmediatePropagation();
      $('.side-nav .nav-item.active a').click()
    });

    $('.delete-modal').on('click', function(event){
      event.stopImmediatePropagation();
      var nodo = $(this).closest('tr').find('.nodo').html();
      var iddo = $(this).closest('tr').attr('value');
      $('#confirm-delete-do').attr('value',iddo).attr('nomor',nodo);
      $("#delete-message").html("Yakin ingin menghapus data "+nodo+" ?")
      $('#modal-delete-do').modal('open');
    });

    $('#confirm-delete-do').on('click', function(event){
      event.stopImmediatePropagation();
      event.preventDefault();
      $('#modal-delete-do').modal('close');
      var id = $(this).attr('value');
      var nodo = $(this).attr('nomor');
      $.ajax({
        type:"POST",
        url:"deletedeliveryorder",
        data:{id:id},
        success:function(response){
          toastr.success('Delivery Order '+nodo+' telah berhasil Dihapus!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
        }
      })
    });

    //function
    function firstload(){
      $('#tgldo').dcalendarpicker({
        format: 'dd-mm-yyyy'
      });
      $('#tgldo').val(moment().format('DD-MM-YYYY'));
      $.ajax({
        type:"GET",
        url:"lastdeliveryordernumber",
        success:function(response){
          $('#donumber').val(response);
        }
      })

      $('.selectpicker').selectpicker('render');

      $('.number').each(function(){
          $(this).html(accounting.formatMoney($(this).html(),'Rp ',2,',','.'));
      });

      deliveryTable = $('#deliveryTable').DataTable({ // This is for home page
      searching: true, 
      responsive: true,
      'sDom':'tip',
      "bPaginate":true,
      "bFilter": false,
      "sPaginationType": "full_numbers",
      "iDisplayLength": 10,
      "language": {
        "infoEmpty": "No records to display",
        "zeroRecords": "No records to display",
        "emptyTable": "No data available in table",
      },
    });

    $('#filterDoNumber').on('keyup', function () { // This is for news page
      deliveryTable.column(0).search(this.value).draw();
    });
    $('#filterPelanggan').on('keyup', function () { // This is for news page
      deliveryTable.column(3).search(this.value).draw();
    });
    }
  });
</script>