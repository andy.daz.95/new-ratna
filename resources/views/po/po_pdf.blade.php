<style type="text/css">
    table.table {
        border-collapse: collapse;
    }
    table.table, table.table th, table.table td{
        border: 1px solid black;
    }
</style>
<div>
    {{--<p style="text-align: center; margin:0px; font-weight: 600">Toko Ratna</p>--}}
    {{--<p style="text-align: center; margin:0px">0812-1212-1212</p>--}}
    <div>
        <img src="{{asset('images/ratna.png')}}" alt="no-image">
        <span style="float: right;font-weight: 600">PURCHASE ORDER</span>
    </div>
    <hr>
    <table style ="width:100%">
        <tr>
            <td style="width: 50%">Kepada YTH,</td>
            <td style="width: 25%">PO Number</td>
            <td>: {{$po->purchase_order_number}}</td>
        </tr>
        <tr>
            <td>{{$po->supplier->company_name}}</td>
            <td>Tanggal PO</td>
            <td>: {{$po->date_purchase_order}}</td>
        </tr>
        <tr>
            <td>{{$po->supplier->address}}</td>
            <td>Payment Type</td>
            <td>: {{$po->paymenttype->payment_description}}</td>
        </tr>
        <tr>
            <td>{{$po->supplier->postal_code}}</td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <table style="width:100%; clear: both; margin-top:10px" class="table table-bordered">
        <thead>
        <tr style="background-color: #8B8C89">
            <th class="color" style="text-align: center;">Nama</th>
            <th class="color" style="width: 100px; text-align: center;">Harga</th>
            <th class="color" style="text-align: center;">Diskon</th>
            <th class="color" style="text-align: center;">Sub Total</th>
        </tr>
        </thead>
        <tbody >
        @foreach($po->details as $key => $value)
            <tr >
                <td>
                    <span>{{$value->category->description.' '.$value->brand->name.' '.$value->type->name}}</span>
                </td>
                <td>
                    <span style="text-align:left;">Rp</span>
                    <span style="display: inline-block; float:right; clear:both;">{{number_format($value->price_buy)}}</span>
                </td>
                <td>
                    <span style="text-align:left;">Rp</span>
                    <span style="display: inline-block; float:right; clear:both;">{{number_format($value->discount_nominal)}}</span>
                </td>
                <td>
                    <span style="text-align:left;">Rp</span>
                    <span style="display: inline-block; float:right; clear:both;">{{number_format($value->sub_total)}}</span>
                </td>
            </tr>
        @endforeach
        <tr style="background-color: #8B8C89">
            <th colspan="3"><span>Grand Total</span></th>
            <th colspan="1"><span style="text-align:left;">Rp</span><span style="float:right;">{{number_format($po->grand_total_idr)}}</span></th>
        </tr>
        </tbody>
    </table>
</div>