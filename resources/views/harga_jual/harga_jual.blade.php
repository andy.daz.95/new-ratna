<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
  <div class="col l12 m12 s12">
    <ul class="collapsible" data-collapsible="accordion">
      <li>
        <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Formulir Penerimaan Barang</div>
        <div class="collapsible-body">
          <div class="container-fluid">
            <div class="row">
              <br>
              <form id="formHargaJual">
                <div class="input-field col l3">
                  <select id="nopo" name="po" class="selectpicker browser-default" data-live-search="true">
                    <option value="0">Select PO</option>
                    @foreach($po as $key => $value)
                      <option value="{{$value->purchase_order_id}}">{{$value->purchase_order_number.' - '.$value->notes}}</option>
                    @endforeach
                  </select>
                  <label>No Purchase Order</label>
                </div>
                <div class="input-field col l3">
                  <input id="supplier" name="supplier" type="text" class="f-input" disabled>
                  <label>Nama Supplier</label>
                </div>
                <div class="input-field col l3">
                  <input id="tglpo" name="tglpo" type="text" class="f-input" disabled>
                  <label>Tanggal PO</label>
                </div>
                <div class="col l12 m12 s12 margin-top">
                  <div class="table-responsive">
                    <table class="stoko-table no-border">
                      <thead>
                      <tr>
                        <th class="theader">Kategori Barang</th>
                        <th class="theader">Brand</th>
                        <th class="theader">Tipe Barang</th>
                        <th class="theader">Harga Beli Bersih</th>
                        <th class="theader">Harga Jual</th>
                        <th class="theader">Rate</th>
                      </tr>
                      </thead>
                      <tbody id="barang-data">
                      <tr id="no-item"><td colspan="6"><span> No Item Selected</span></td></tr>
                      <tr class="barang-row" hidden>
                        <td>
                          <div class="input-field">
                            <input id="id-1" type="text" class="f-input id-detail" name="id[]" hidden>
                            <input id="kategori-1" type="text" class="f-input" name="kategori[]">
                          </div>
                        </td>
                        <td>
                          <div class="input-field">
                            <input id="brand-1" type="text" class="f-input brand" name="brand[]">
                          </div>
                        </td>
                        <td>
                          <div class="input-field">
                            <input id="type-1" type="text" class="f-input type" name="type[]">
                          </div>
                        </td>

                        <td>
                          <div class="input-field">
                            <input id="netbuy-1" type="text" class="f-input netbuy"
                                   name="netbuy[]" disabled value="0">
                          </div>
                        </td>
                        <td>
                          <div class="input-field">
                            <input id="harga-jual-1" type="text"
                                   class="f-input hargajual" name="hargajual[]">
                          </div>
                        </td>
                        <td>
                          <div class="input-field">
                            <input id="rate-1" type="text" class="f-input rate"
                                   style="display: inline-block; width:60%"
                                   name="rate[]">
                            <input class="f-input"
                                   style="display: inline-block; width:35%;" value="%"
                                   name="rate[]" disabled>
                          </div>
                        </td>
                      </tr>
                      </tbody>
                    </table>
                  </div>
                  <br>
                  <div class="input-field right">
                    <a id="submit-po" href="" mode="save" class="btn-stoko btn-stoko-primary">Submit BPB</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
</div>
</div>

<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn-submit').attr('mode', 'save');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });
        $.ajax({
            url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
            dataType: "script",
        });

        firstload();

        $('#nopo').on('change',function(){
            var id = $(this).val();
            $.ajax({
                type:"Get",
                url:"getpoforhargajual",
                data:{id:id},
                success:function(response){
                    var $original = $('.barang-row:first');
                    if (response.length == 0) {
                        $original.clone();
                        $('.barang-row').remove();
                        $original.attr('hidden',true);
                        $original.appendTo('#barang-data');
                        $('#no-item').removeAttr('hidden');
                        reset();
                    }else{
                        $('#supplier').val(response.supplier.company_name);
                        $('#tglpo').val(response.date_purchase_order);
                        var $cloned = $original.clone();
                        $('.barang-row').remove();
                        $.each(response.detailswopricesale, function(i,v){
                            var newid = "id-"+(i+1);
                            var newKategori = "kategori-"+(i+1);
                            var newBrand = "brand-"+(i+1);
                            var newType = "type-"+(i+1);
                            var newnetbuy = "netbuy-"+(i+1);
                            var newHargaJual = "harga-jual-"+(i+1);
                            var newRate = "rate-"+(i+1);
                            $temp = $original.clone();
                            $temp.removeAttr('hidden');
                            $temp.find('#id-1').attr('id',newid);
                            $temp.find('#kategori-1').attr('id',newKategori);
                            $temp.find('#brand-1').attr('id',newBrand);
                            $temp.find('#type-1').attr('id',newType);
                            $temp.find('#netbuy-1').attr('id',newnetbuy);
                            $temp.find('#harga-jual-1').attr('id',newHargaJual);
                            $temp.find('#rate-1').attr('id',newRate);
                            $temp.appendTo('#barang-data');
                            $('#id-'+(i+1)).val(v.purchase_order_details_id);
                            $('#kategori-'+(i+1)).val(v.category.description).attr('disabled',true);
                            $('#brand-'+(i+1)).val(v.brand.name).attr('disabled',true);
                            $('#type-'+(i+1)).val(v.type.name).attr('disabled',true);
                            $('#netbuy-'+(i+1)).val(accounting.formatMoney(v.net_price_buy,'',0,',','.')).attr('disabled',true);
                        });

                        $('#no-item').attr('hidden',true);

                    }
                }
            });
        });

        $('#formHargaJual').on('keyup','.hargajual',function(event){
            event.stopImmediatePropagation();
            calculateratebynominal($(this));
        });

        $('#formHargaJual').on('keyup','.rate',function(event){
            event.stopImmediatePropagation();
            calculatenominalbyrate($(this));
        });

        $('#submit-po').click(function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var kode = $('#po').val();
            var validhargajual = [];
            mode = $(event.currentTarget).attr('mode');

            if($(event.currentTarget).attr('mode') == 'save')
            {
                var url = "updatehargajual";
                var successmessage = 'Harga Jual untuk PO '+kode+' telah berhasil di input!';
            }

            $('.barang-row').each(function () {
                var hargajual = $(this).find('.hargajual').val();
                var unformathargajual = accounting.unformat(hargajual);
                validhargajual.push(ceknumber(unformathargajual));
            });

            if (validhargajual.indexOf(false) > -1) {
                toastr.warning('Masukkan Harga Jual dengan Benar!');
            }else{
                $('.hargajual, .rate').removeAttr('disabled');
                $('.hargajual').each(function () {
                    var hargajual = $(this).val();
                    var unformathargajual = accounting.unformat(hargajual);
                    $(this).val(unformathargajual);
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $(this).closest('#formHargaJual').serialize(),
                    success: function (response) {
                        toastr.success(successmessage, {
                            "onShow": setTimeout(function () {
                                $('.side-nav .nav-item.active a').click();
                            }, 2600)
                        });
                    }
                })
            }
        });

        $('#clear').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('.side-nav .nav-item.active a').click()
        });

        $('#formHargaJual').on('focusout', '.hargajual', function (event) {
            event.stopImmediatePropagation();
            $(this).val(accounting.formatMoney($(this).val(),'',0,',','.'));
        });

        $('#formHargaJual').on('focusin', '.hargajual', function (event) {
            event.stopImmediatePropagation();
            $(this).val("");
            calculateratebynominal($(this));
        });

        function firstload(){
            $('.selectpicker').selectpicker('render');

            $.ajax({
                type:"GET",
                url:"lastbpbnumber",
                success:function(response){
                    $('#bpbnumber').val(response);
                }
            })
        }

        function reset() {
            $('#nopo').val(0);
            $('#supplier').val("");
            $('#notes').val("");
        }

        function calculateratebynominal(element) {
            var netbuy = accounting.unformat(element.closest('tr').find('.netbuy').val());
            var hargajual = element.closest('tr').find('.hargajual').val();
            var rate = (hargajual - netbuy) * 100 / netbuy;
            element.closest('tr').find('.rate').val(parseFloat(rate).toFixed(2));
        }
        function calculatenominalbyrate(element){
            var netbuy = accounting.unformat(element.closest('tr').find('.netbuy').val());
            var rate = element.closest('tr').find('.rate').val();
            var hargajual = parseInt(netbuy)+(netbuy * rate / 100);
            element.closest('tr').find('.hargajual').val(Math.round(hargajual));
        }
    });
</script>
