<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l5 m5 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">create</i>Formulir News</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <form id="formNews">
                <div class="row margin-top">
                  <div class="col l12">
                    <div class="row">
                      <div class="input-field col l6 m12 s12">
                        <input id="id" type="text" class="f-input" name="id" hidden>
                        <input id="title" type="text" class="f-input" name="title">
                        <label>News Title</label>
                      </div>
                      <div class="input-field col l6 m12 s12">
                        <input id="tanggal" type="text" class="f-input">
                        <input id="tgl" type="text" class="f-input" name="tanggal" hidden>
                        <label>Tanggal</label>
                      </div>
                      <div class="input-field col l12 m12 s12">
                        <textarea id="deskripsi" type="text" class="f-txt" name="deskripsi"></textarea>
                        <label>Deskripsi</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col l12 m12 s12">
                    <button id="btn-submit" type="submit" class="btn btn-raised btn-sm light-blue darken-2" mode="save">simpan</button>
                    <a href="#" class="btn btn-raised btn-sm orange reset">batal</a>
                    <a href="#" class="btn btn-raised btn-sm grey lighten-4 grey-text text-darken-4">cetak</a>
                  </div>
                </div>
                {{csrf_field()}}
              </form>
            </div>
          </div>
        </li>
      </ul>
    </div>
    <div class="col l7 m7 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Berita</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row margin-top">
                <div class="input-field col l5">
                  <input type="text" class="f-input" id="findNews">
                  <label>Search</label>
                </div>
                <div class="col l12 m12 s12">
                  <div class="">
                    <table id="newsTable" class="table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                        <tr>
                          <th>Tgl</th>
                          <th>News Title</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($news as $key => $value)
                        <tr value="{{$value->news_id}}">
                          <td>{{$value->date}}</td>
                          <td>{{$value->title}}</td>
                          <td>
                            <a href="#" class="btn btn-sm btn-raised light-blue darken-2 edit"><i class="material-icons">edit</i></a>
                            <a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- Modal Delete News -->
<div id="delete_data" class="modal">
  <div class="modal-content">
    <h5>Yakin ingin menghapus data berikut?</h5>
    <div class="row">
      <div class="col l12">
        <table id='modal-delete-data'>

        </table>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <form>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat orange white-text no-border-radius">batal</a>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat red white-text no-border-radius delete">hapus</a>
    </form>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#btn-submit').attr('mode', 'save');
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: 'js/custom.js',
      dataType: "script",
    });
    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });

    $('#tanggal').dcalendarpicker({
      format: 'dd-mm-yyyy'
    });

    $('#tanggal').dcalendarpicker().on('datechanged', function(e){
      e.stopPropagation();
      var tanggal = $(this).val();
      var tgl = moment(tanggal, 'DD-MM-YYYY').format('YYYY-MM-DD');
      $('#tgl').val(tgl);
    });

    newsTable = $('#newsTable').DataTable({ // This is for home page
      searching: true,
      responsive: true,
      "autoWidth":true,
      'sDom': 'ti',
      'pagingType': 'full_numbers_no_ellipses',
      "language": {
        "infoEmpty": "No records to display",
        "zeroRecords": "No records to display",
        "emptyTable": "No data available in table",
      },
    });

    $('#findNews').on('keyup', function () { // This is for news page
      newsTable.column(1).search(this.value).draw();
    });

    $('#formNews').submit(function(event){
      event.preventDefault();
      var mode = $(this).find('#btn-submit').attr('mode');
      console.log(mode);
      if (mode == 'save') 
      {
        var id = $(this).find('#id').val();  
        $.ajax({
          type:"POST",
          url:"createNews",
          data: $("#formNews").serialize(),
          success:function(response){
            toastr.success('News Telah Berhasil Dibuat!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
          }
        });  
      }else if(mode == 'edit'){
        var id = $(this).find('#id').val();  
        $.ajax({
          type:"POST",
          url:"updateNews",
          data: $("#formNews").serialize()+"&id="+id,
          success:function(response){
            toastr.success('News Telah Berhasil Diubah!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
          }
        });
      }
    });

    $('body').on('click','#formNews .reset', function(event){
      event.stopImmediatePropagation();
      $(this).closest('#formNews').find("input[type=text], textarea").val("");
      $('#btn-submit').attr('mode', 'save');
    });

    $('body').on('click','#newsTable .edit', function(){
      event.stopImmediatePropagation();
      var id = $(this).closest('tr').attr('value');
      $.ajax({
        type:"GET",
        url:"getNewsData",
        data: {id:id},
        success:function(response){
          $('#id').val(response.news_id);
          $('#title').val(response.title);
          $('#tanggal').val(moment(response.date, "YYYY-MM-DD").format('DD-MM-YYYY'));
          $('#deskripsi').val(response.description);
        }
      });
      $('#btn-submit').attr('mode', 'edit');
    });

    $(".delete-modal").on('click',function(event){
      event.preventDefault();
      var id = $(this).closest('tr').attr('value');

      $.ajax({
        type:"GET",
        url:"modalNewsData",
        async:false,
        data:{
          id:id,
        },
        success:function(response){
          $('#modal-delete-data').html(response);
        }
      });
      $('#delete_data').modal('open');
    });

    $(".delete").on('click',function(){
      var id = $(this).closest('.modal').find('#id-delete').attr('value');
      console.log(id);
      $.ajax({
        type:"Get",
        url:"deleteNews",
        data:{id:id},
        success:function(response){
          toastr.success('Berhasil Menghapus News!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
        }
      });
    });
  })
</script>
