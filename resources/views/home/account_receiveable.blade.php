<ul class="collapsible" data-collapsible="accordion">
    <li>
        <div class="collapsible-header red darken-1 white-text"><i class="material-icons">attach_money</i>Aging AR</div>
        <div class="collapsible-body">
            <div class="container-fluid">
                <div class="table-responsive bordered margin-top padding-bottom">
                    <table id="homeTable_3" class="table table-bordered display nowrap dataTable dtr-inline">
                        <thead>
                        <tr>
                            <th>Scale</th>
                            <th>#Customer</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{'>1 Year'}}</td>
                            <td>
                                {{
                                $invoiceunpaid->where('due_date','<',\Carbon\Carbon::now()->subDays(360))
                                ->groupBy('customer_id')
                                ->count()
                                }}
                            </td>
                            <td>
                                {{
                                $invoiceunpaid->where('due_date','<',\Carbon\Carbon::now()->subDays(360))
                                ->sum('total_amount')
                                }}
                            </td>
                        </tr>
                        <tr>
                            <td>{{'>6 Month'}}</td>
                            <td>
                                {{
                                $invoiceunpaid->where('due_date','>=',\Carbon\Carbon::now()->subDays(360)->toDateString())
                                ->where('due_date','<',\Carbon\Carbon::now()->subDays(180)->toDateString())
                                ->groupBy('customer_id')
                                ->count()
                                }}
                            </td>
                            <td>
                                {{
                                $invoiceunpaid->where('due_date','>=',\Carbon\Carbon::now()->subDays(360)->toDateString())
                                ->where('due_date','<',\Carbon\Carbon::now()->subDays(180)->toDateString())
                                ->sum('total_amount')
                                }}
                            </td>
                        </tr>
                        <tr>
                            <td>{{'3-6 Month'}}</td>
                            <td>
                                {{
                                $invoiceunpaid->where('due_date','>=',\Carbon\Carbon::now()->subDays(180)->toDateString())
                                ->where('due_date','<',\Carbon\Carbon::now()->subDays(90)->toDateString())
                                ->groupBy('customer_id')
                                ->count()
                                }}
                            </td>
                            <td>
                                {!!
                                $invoiceunpaid->where('due_date','>=',\Carbon\Carbon::now()->subDays(180)->toDateString())
                                ->where('due_date','<',\Carbon\Carbon::now()->subDays(90)->toDateString())
                                ->sum('total_amount')
                                !!}
                            </td>
                        </tr>
                        <tr>
                            <td>{{'2-3 Month'}}</td>
                            <td>
                                {{
                                $invoiceunpaid->where('due_date','>=',\Carbon\Carbon::now()->subDays(90)->toDateString())
                                ->where('due_date','<',\Carbon\Carbon::now()->subDays(60)->toDateString())
                                ->groupBy('customer_id')
                                ->count()
                                }}
                            </td>
                            <td>
                                {!!
                                $invoiceunpaid->where('due_date','>=',\Carbon\Carbon::now()->subDays(90)->toDateString())
                                ->where('due_date','<',\Carbon\Carbon::now()->subDays(60)->toDateString())
                                ->sum('total_amount')
                                !!}
                            </td>
                        </tr>
                        <tr>
                            <td>{{'1 Month'}}</td>
                            <td>
                                {{
                                $invoiceunpaid->where('due_date','>=',\Carbon\Carbon::now()->subDays(60)->toDateString())
                                ->where('due_date','<',\Carbon\Carbon::now()->subDays(30)->toDateString())
                                ->groupBy('customer_id')
                                ->count()
                                }}
                            </td>
                            <td>
                                {!!
                                $invoiceunpaid->where('due_date','>=',\Carbon\Carbon::now()->subDays(60)->toDateString())
                                ->where('due_date','<',\Carbon\Carbon::now()->subDays(30)->toDateString())
                                ->sum('total_amount')
                                !!}
                            </td>
                        </tr>
                        <tr>
                            <td>{{'0 Month'}}</td>
                            <td>
                                {!!
                                $invoiceunpaid->where('due_date','>=',\Carbon\Carbon::now()->subDays(30)->toDateString())
                                ->groupBy('customer_id')
                                ->count()
                                !!}
                            </td>
                            <td>
                                {!!
                                $invoiceunpaid->where('due_date','>=',\Carbon\Carbon::now()->subDays(30)->toDateString())
                                ->sum('total_amount')
                                !!}
                            </td>
                        </tr>
                        <tr>
                            <td>{{'Total'}}</td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </li>
</ul>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
    })
</script>
