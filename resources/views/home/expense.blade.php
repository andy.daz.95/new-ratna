<ul class="collapsible" data-collapsible="accordion">
    <li>
        <div class="collapsible-header red darken-1 white-text"><i class="material-icons">attach_money</i>Monthly Expense</div>
        <div class="collapsible-body">
            <div class="container-fluid">
                <div class="table-responsive bordered margin-top padding-bottom">
                    <table id="homeTable_3" class="table table-bordered display nowrap dataTable dtr-inline">
                        <thead>
                        <tr>
                            <th>Kategori Expense</th>
                            <th>Total</th>
                            <th>%</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($expensecat as $key => $value)
                            <tr>
                                @php
                                    $total = $expense->where('expense_category_id', $value->expense_category_id)->sum('nominal');
                                @endphp
                                <td>{{$value->expense_description}}</td>
                                <td>{{$total}}</td>
                                <td>{{$total / $totalexpense * 100}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </li>
</ul>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
    })
</script>
