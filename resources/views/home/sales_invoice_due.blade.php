<ul class="collapsible" data-collapsible="accordion">
    <li>
        <div class="collapsible-header red darken-1 white-text"><i class="material-icons">attach_money</i>Invoice Jatuh Tempo</div>
        <div class="collapsible-body">
            <div class="container-fluid">
                <div class="table-responsive bordered margin-top padding-bottom">
                    <table id="invoiceDueTable" class="table table-bordered display nowrap dataTable dtr-inline">
                        <thead>
                        <tr>
                            <th>Sales Invoice Number</th>
                            <th>Customer Name</th>
                            <th>Product Barcode</th>
                            <th>Due Date</th>
                            <th>Aging</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($invoicedue as $key => $value)
                            <tr value="{{$value->invoice_sales_id}}">
                                <td>{{$value->invoice_sales_number}}</td>
                                <td>{{$value->so->customer_name}}</td>
                                <td>{{$value->product->bar_code}}</td>
                                <td>{{$value->due_date}}</td>
                                @php
                                    $aging = Carbon\Carbon::createFromFormat('Y-m-d', $value->due_date)->diffInDays(\Carbon\Carbon::now(), false) > 0 ?
                                        Carbon\Carbon::createFromFormat('Y-m-d', $value->due_date)->diffInDays(\Carbon\Carbon::now(), false)
                                        : 0
                                @endphp
                                <td>
                                    {{$aging}}
                                </td>
                                @php
                                    if($value->is_emailed == 0)
                                        $color = '#4E4E4E';
                                    else
                                        $color = '#0f9d58';
                                @endphp
                                <td>
                                    @if($aging > 0)
                                        <a class="mail" style="color: {{$color}}" href=""><i class="material-icons">email</i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </li>
</ul>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });

        invoiceDueTable = $('#invoiceDueTable').DataTable({ // This is for home page
            searching: true,
            responsive: true,
            bSortClasses: false,
            'sDom': 'tip',
            "pageLength": 10,
            'pagingType': 'full_numbers',
            "language": {
                "infoEmpty": "No records to display",
                "zeroRecords": "No records to display",
                "emptyTable": "No data available in table",
            },
        });

        $('body').on('click', '.mail', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            var url = "sendmail/"+$(this).closest('tr').attr('value');
            var elm = $(this)
            $.ajax({
                url : url,
                method:"GET",
                success:function (response) {
                    toastr.success('Email Telah Dikirim');
                },complete:function(){
                    $(elm).css("color",'#0f9d58');
                }
            })
        });
    })
</script>
