<ul class="collapsible" data-collapsible="accordion">
    <li>
        <div class="collapsible-header red darken-1 white-text"><i class="material-icons">attach_money</i>Sales Achievement in Month</div>
        <div class="collapsible-body">
            <div class="container-fluid">
                <div class="table-responsive bordered margin-top padding-bottom">
                    <table class="table table-bordered display nowrap dataTable dtr-inline">
                        <thead>
                        <tr>
                            <th rowspan="2">{{\Carbon\Carbon::now()->format('F')}}</th>
                            @foreach($paymenttype as $key => $value)
                                <th colspan="2" style="text-align: center">{{$value->payment_description}}</th>
                            @endforeach
                        </tr>
                        <tr>
                            @foreach($paymenttype as $key => $value)
                                <th style="text-align: center">Qty</th>
                                <th style="text-align: center">Amount</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($productcat as $key => $value)
                                <tr>
                                    <td>{{$value->description}}</td>
                                    @foreach($paymenttype as $key2 => $value2)
                                        <td style="text-align: center">
                                            {{
                                            $detailsales->where('payment_type_id', $value2->payment_type_id)
                                            ->where('product_category_id',$value->product_category_id)
                                            ->count()
                                            }}
                                        </td>
                                        <td style="text-align: center">
                                            {{
                                            number_format($detailsales->where('payment_type_id', $value2->payment_type_id)
                                            ->where('product_category_id',$value->product_category_id)
                                            ->sum('sub_total'))
                                            }}
                                        </td>
                                    @endforeach
                                </tr>
                            @endforeach
                            <tr>
                                <td>Omzet</td>
                                @foreach($paymenttype as $key => $value)
                                    <td style="text-align: center">
                                        {{
                                            $detailsales->where('payment_type_id', $value->payment_type_id)
                                            ->count()
                                        }}
                                    </td>
                                    <td style="text-align: center">
                                        @php
                                            ${"omzet$value->payment_description"} = $detailsales->where('payment_type_id', $value->payment_type_id)
                                            ->sum('sub_total');
                                        @endphp
                                        {{
                                            number_format(${"omzet$value->payment_description"})
                                        }}
                                    </td>
                                @endforeach
                            </tr>
                            <tr>
                                <td>Harga Beli</td>
                                @foreach($paymenttype as $key => $value)
                                    @php ${"modal$value->payment_description"} = $detailsales->where('payment_type_id', $value->payment_type_id)
                                            ->sum('price_buy');
                                    @endphp

                                    <td colspan="2" style="text-align: center">
                                        {{ number_format(${"modal$value->payment_description"}) }}
                                    </td>
                                @endforeach
                            </tr>
                            <tr>
                                <td>GP</td>
                                @foreach($paymenttype as $key => $value)
                                    @php
                                        ${"grossprofit$value->payment_description"} = ${"omzet$value->payment_description"} - ${"modal$value->payment_description"} ;
                                    @endphp
                                    <td colspan="2" style="text-align: center">
                                        {{ number_format(${"grossprofit$value->payment_description"}) }}
                                    </td>
                                @endforeach
                            </tr>
                            <tr>
                                <td> % GP</td>
                                @foreach($paymenttype as $key => $value)
                                    @php
                                        if(${"omzet$value->payment_description"} > 0)
                                        ${"percentgp$value->payment_description"} = ${"grossprofit$value->payment_description"} / ${"omzet$value->payment_description"} * 100 ;
                                        else
                                        ${"percentgp$value->payment_description"} = 0;
                                    @endphp
                                    <td colspan="2" style="text-align: center">
                                        {{ number_format((float) ${"percentgp$value->payment_description"}, 2, '.', '')}}
                                    </td>
                                @endforeach
                            </tr>
                            <tr>
                                <td>Total Omzet</td>
                                @php
                                    $count = $paymenttype->count();
                                    $totalomzet = 0;
                                @endphp
                                @foreach($paymenttype as $key => $value)
                                    @php
                                        $totalomzet += ${"omzet$value->payment_description"};
                                    @endphp
                                @endforeach
                                <td colspan="{{$count*2}}" style="text-align: center">{{number_format($totalomzet)}}</td>
                            </tr>
                            <tr>
                                <td>Total Harga Beli</td>
                                @php
                                    $count = $paymenttype->count();
                                    $totalmodal = 0;
                                @endphp
                                @foreach($paymenttype as $key => $value)
                                    @php
                                        $totalmodal += ${"modal$value->payment_description"};
                                    @endphp
                                @endforeach
                                <td colspan="{{$count*2}}" style="text-align: center">{{number_format($totalmodal)}}</td>
                            </tr>
                            <tr>
                                <td>Total GP</td>
                                @php
                                    $count = $paymenttype->count();
                                    $totalgp = 0;
                                @endphp
                                @foreach($paymenttype as $key => $value)
                                    @php
                                        $totalgp += ${"grossprofit$value->payment_description"};
                                    @endphp
                                @endforeach
                                <td colspan="{{$count*2}}" style="text-align: center">{{number_format($totalgp)}}</td>
                            </tr>
                            <tr>
                                <td>Total % GP</td>
                                @php
                                    $count = $paymenttype->count();
                                @endphp
                                <td colspan="{{$count*2}}" style="text-align: center">{{ number_format((float) $totalgp / $totalomzet * 100, 2, '.', '' )}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </li>
</ul>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });

        $('body').on('click', '.mail', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            var url = "sendmail/"+$(this).closest('tr').attr('value');
            $.ajax({
                url : url,
                method:"GET",
                success:function (response) {
                    toastr.success('Email Telah Dikirim');
                }
            })
        });
    })
</script>
