<ul class="collapsible" data-collapsible="accordion">
  <li>
    <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Target Pembelian</div>
    <div class="collapsible-body">
      <div class="container-fluid">
        <div class="row">
          <table>
            <tr>
              <div id="append-canvas" class="row">
                <div class="canvas col l4 m4 s4" style="min-height: 240px">
                  <h5 class="title-top" style="text-align: center; position: relative; top:15px; font-size: 20px">Target Pembelian Supplier X</h5>
                  <div id="chartContainer" class="container chart" style="min-height: 180px">

                  </div>
                  <p class="title-bottom" style="text-align: center">100000000 out of 200000000000</p>
                </div>
              </div>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </li>
</ul>
<script type="text/javascript">
  $(document).ready(function(){
    $.ajax({
      method:"GET",
      url:"grafik_target",
      success:function(response){
        var $orginal = $('.canvas:first');
        $('.canvas:first').remove();
        $.each(response, function(key,value){
          var target_nominal = value.target.target_nominal;
          var pembelian = value.pembelian ? value.pembelian : 0  ;
          var percent = (parseInt(pembelian) / parseInt(target_nominal)* 100).toFixed(2);
          if(percent > 100)
          {
            percent = 100;
          }
          var data = [];
          var $newId = 'chartContainer'+key;
          var $cloned = $orginal.clone().addClass('cloned');
          $cloned.find('#chartContainer').attr('id',$newId);
          $cloned.find('.title-top').html(value.company_name+"</br> periode "+value.target.date_start+" to "+value.target.date_end);
          $cloned.find('.title-bottom').html(CanvasJS.formatNumber(pembelian, '#,###,###')+' out of '+ CanvasJS.formatNumber(target_nominal, '#,###,###'));
          $cloned.appendTo('#append-canvas');
          var chart = new CanvasJS.Chart("chartContainer"+key, {
            animationEnabled: true,
            backgroundColor: "rgba(0, 0, 0, 0)",
            title: {
              fontColor: "#848484",
              fontSize: 42,
              horizontalAlign: "center",
              text: percent+"%",
              verticalAlign: "center"
            },
            toolTip: {
              backgroundColor: "#ffffff",
              borderThickness: 0,
              cornerRadius: 0,
              fontColor: "#424242"
            },
            data: [
            {
              explodeOnClick: false,
              innerRadius: "90%",
              radius: "90%",
              startAngle: 270,
              type: "doughnut",
              dataPoints: [
              { y: percent, color: "#c70000", toolTipContent: "Pembelian: Rp <span>" + CanvasJS.formatNumber(pembelian, '#,###,###') + "</span>" },
              { y: (100 - percent), color: "#424242", toolTipContent: null }
              ]
            }
            ]
          });
          chart.render();
        });
      }
    });
  })
</script>
