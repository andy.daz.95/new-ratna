<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap-select.min.css">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
    <div class="row">
        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text"><i class="material-icons">search</i>Cari Sales Return</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <br/>
                                <div class="input-field col l3">
                                    <label>Filter Retur</label>
                                    <input id="filterReturNumber" type="text" class="f-input" placeholder="Filter Retur" />
                                </div>
                                <div class="input-field col l3">
                                    <label>Filter Pelanggan</label>
                                    <input id="filterPelanggan" type="text" class="f-input" placeholder="Filter Pelanggan" />
                                </div>
                                <div class="input-field col l3">
                                    <label>Filter SO</label>
                                    <input id="filterSoNumber" type="text" class="f-input" placeholder="Filter SO" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col l12 m12 s12">
                                    <div class="table-responsive">
                                        <table id="salesReturTable" class="highlight table table-bordered display nowrap dataTable dtr-inline" style="width: 100%;">
                                            <thead>
                                            <tr>
                                                <th class="theader">No Retur</th>
                                                <th class="theader">Customer</th>
                                                <th class="theader">No SO</th>
                                                <th class="theader">Note SO</th>
                                                <th class="theader">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {{--@foreach($data['retur'] as $key => $value)--}}
                                                {{--<tr mode="view" value="{{$value->sales_retur_id}}">--}}
                                                    {{--<td class="noretur">{{$value->retur_number}}</td>--}}
                                                    {{--<td>{{$value->delivery->so->customer->first_name.' '.$value->delivery->so->customer->last_name}}</td>--}}
                                                    {{--<td>{{$value->delivery->so->sales_order_number}}</td>--}}
                                                    {{--<td>{{$value->delivery->so->note}}</td>--}}
                                                    {{--<td>--}}
                                                        {{--<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>--}}
                                                    {{--</td>--}}
                                                {{--</tr>--}}
                                            {{--@endforeach--}}
                                            </tbody>
                                        </table>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Formulir Sales Retur</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <br/>
                                <form id="formRetur">
                                    <div class="input-field col l3">
                                        <label>No Retur SO</label>
                                        <input id="noretur" type="text" name="noretur" class="f-input" disabled>
                                    </div>
                                    <div class="input-field col l3">
                                        <label>No Delivery Order</label>
                                        <select id="nodo" name="nodo" class="browser-default selectpicker" data-live-search="true">
                                            <option value="0">Pilih Delivery Order</option>
                                            @foreach($data['delivery'] as $key => $value)
                                                <option value="{{$value->delivery_order_id}}">{{$value->so->sales_order_number.' - '.$value->so->customer_name.' - '.$value->delivery_order_number}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="input-field col l3">
                                        <label>Pelanggan</label>
                                        <input id="customer" type="text" name="customer" class="f-input" placeholder="Prima PT" disabled>
                                    </div>
                                    <div class="input-field col l3">
                                        <label>Tanggal Retur</label>
                                        <input id="tglretur" type="text" name="tglretur" class="f-input datepicker" placeholder="Tanggal Retur">
                                    </div>
                                    <div class="input-field col l3">
                                        <label>Tipe Retur</label>
                                        <select id="returtype" name="returtype" class="selectpicker browser-default" data-live-search="true" disabled>
                                            <option>Select Retur Type</option>
                                            @foreach($data['returtype'] as $key => $value)
                                                <option value="{{$value->retur_type_id}}">{{$value->retur_type_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col l12 m12 s12 margin-top">
                                        <div class="table-responsive">
                                            <table class="stoko-table no-border">
                                                <thead>
                                                <tr>
                                                    <th class="theader">Barcode</th>
                                                    <th class="theader">Kategori Barang</th>
                                                    <th class="theader">Brand</th>
                                                    <th class="theader">Tipe Barang</th>
                                                    <th class="theader">Subtotal</th>
                                                    <th class="theader">Retur</th>
                                                </tr>
                                                </thead>
                                                <tbody id="barang-data">
                                                <tr id="no-item"><td colspan="7"><span> No Item Selected</span></td></tr>
                                                <tr class="barang-row" hidden>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="barcode-1" type="text" class="f-input idbarcode" name="barcode[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="id-1" type="text" class="f-input id-detail" name="id[]" hidden>
                                                            <input id="id-barang-1" type="text" class="f-input id-barang" name="idbarang[]" hidden>
                                                            <input id="kategori-1" type="text" class="f-input" name="kategori[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="brand-1" type="text" class="f-input brand" name="brand[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="type-1" type="text" class="f-input type" name="type[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="subtotal-1" type="text" class="f-input subtotal" name="subtotal[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" id="checkbox-retur-1" class="filled-in retur" value="" />
                                                        <label id="label-retur-1" for="checkbox-retur-1"></label>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            </br>
                                            <div class="input-field">
                                                <label>Nilai Retur</label>
                                                <input id="totalvalue" type="text" name="totalvalue" class="f-input datepicker" placeholder="Nilai Retur" value="0" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col l12 s12 m12 main-button-group margin-top">
                                        <a href="#" class="btn-stoko teal white-text submit">simpan</a>
                                        <a href="#" class="btn-stoko red white-text">hapus</a>
                                        <a href="#" class="btn-stoko">batal</a>
                                        <a href="#" class="btn-stoko">print</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<div id="modal-delete-retur" class="modal">
    <div class="modal-content">
        <h5 id="delete-message"></h5>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
        <a id="confirm-delete-retur" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
    </div>
</div>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });

        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'js/bootstrap-select.min.js',
            dataType: "script",
        });

        firstload();

        $('#nodo').on('change',function(){
            var id = $(this).val();
            $.ajax({
                type:"Get",
                url:"getdeliveryforreturn",
                data:{id:id},
                success:function(response){
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    if(response['delivery'].details_not_retur.length == 0)
                    {
                        $original.clone();
                        $original.attr('hidden',true)
                        $original.appendTo('#barang-data');
                        $('#no-item').removeAttr('hidden');
                    }else{
                        $('#no-item').attr('hidden',true);
                        $('#customer').val(response['delivery'].so.customer.first_name+' '+response['delivery'].so.customer.last_name);
                        $.each(response['delivery'].details_not_retur, function(i,v){
                            var newid = "id-"+(i+1);
                            var newIdBarang = "id-barang-"+(i+1);
                            var newBarcode= "barcode-"+(i+1);
                            var newKategori = "kategori-"+(i+1);
                            var newBrand = "brand-"+(i+1);
                            var newType = "type-"+(i+1);
                            var newRetur = "checkbox-retur-"+(i+1);
                            var newSubtotal = "subtotal-"+(i+1);
                            var newLabelRetur = "label-retur-"+(i+1);
                            $temp = $original.clone();
                            $temp.removeAttr('hidden');
                            $temp.find('#id-1').attr('id',newid);
                            $temp.find('#id-barang-1').attr('id',newIdBarang);
                            $temp.find('#barcode-1').attr('id',newBarcode);
                            $temp.find('#kategori-1').attr('id',newKategori);
                            $temp.find('#brand-1').attr('id',newBrand);
                            $temp.find('#type-1').attr('id',newType);
                            $temp.find('#subtotal-1').attr('id',newSubtotal);
                            $temp.find('#checkbox-retur-1').attr('id',newRetur);
                            $temp.find('#label-retur-1').attr('id',newLabelRetur).attr('for',"checkbox-retur-"+(i+1));
                            $temp.appendTo('#barang-data');
                            $('#id-'+(i+1)).val(v.delivery_order_details).attr('disabled',true);
                            $('#barcode-'+(i+1)).val(v.product.bar_code).attr('disabled',true);
                            $('#id-barang-'+(i+1)).val(v.product_id).attr('disabled',true);
                            $('#kategori-'+(i+1)).val(v.product.category.description).attr('disabled',true);
                            $('#brand-'+(i+1)).val(v.product.brand.name).attr('disabled',true);
                            $('#type-'+(i+1)).val(v.product.type.name).attr('disabled',true);
                            $('#subtotal-'+(i+1)).val(v.sub_total).attr('disabled',true);
                        })

                        $('#returtype').html("");
                        $.each(response['returtype'],function(i,v){
                            $('#returtype').append("<option value='"+v.retur_type_id+"''>"+v.retur_type_name+"</option>").removeAttr('disabled');
                        });
                        $('.selectpicker').selectpicker('refresh');
                    }
                }
            });
        });

        $('#salesReturTable').on('click', 'tr, .edit', function(event){
            event.stopImmediatePropagation();
            id = $(this).attr('value');
            $.ajax({
                type:"GET",
                url:"getsalesreturn",
                data:{id:id},
                success:function(response){
                    console.log(response);
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    $('#noretur').val(response.retur_number).attr('disabled',true);
                    $('#nodo').append('<option class="remove-when-clear" value="'+response.delivery.delivery_order_id+'" selected="selected">'+response.delivery.delivery_order_number+'</option>').attr('disabled',true);
                    $('#customer').val(response.delivery.so.customer.first_name+' '+response.delivery.so.customer.last_name).attr('disabled',true);
                    $('#tglretur').val(response.date_retur).attr('disabled',true);
                    $('#returtype').val(response.retur_type_id).attr('disabled',true);
                    $.each(response.details, function(i,v){
                        var newid = "id-"+(i+1);
                        var newIdBarang = "id-barang-"+(i+1);
                        var newBarcode = "barcode-"+(i+1);
                        var newKategori = "kategori-"+(i+1);
                        var newBrand = "brand-"+(i+1);
                        var newType = "type-"+(i+1);
                        var newRetur = "checkbox-retur-"+(i+1);
                        var newSubtotal = "subtotal-"+(i+1);
                        var newLabelRetur = "label-retur-"+(i+1);
                        $temp = $original.clone();
                        $temp.removeAttr('hidden');
                        $temp.find('#id-1').attr('id',newid);
                        $temp.find('#id-barang-1').attr('id',newIdBarang);
                        $temp.find('#barcode-1').attr('id',newBarcode);
                        $temp.find('#kategori-1').attr('id',newKategori);
                        $temp.find('#brand-1').attr('id',newBrand);
                        $temp.find('#type-1').attr('id',newType);
                        $temp.find('#subtotal-1').attr('id',newSubtotal);
                        $temp.find('#checkbox-retur-1').attr('id',newRetur);
                        $temp.find('#label-retur-1').attr('id',newLabelRetur).attr('for',"checkbox-retur-"+(i+1));
                        $temp.appendTo('#barang-data');
                        $('#id-'+(i+1)).val(v.detaildelivery.delivery_order_details).attr('disabled',true);
                        $('#id-barang-'+(i+1)).val(v.detaildelivery.product_id).attr('disabled',true);
                        $('#barcode-'+(i+1)).val(v.detaildelivery.product.bar_code).attr('disabled',true);
                        $('#kategori-'+(i+1)).val(v.detaildelivery.product.category.description).attr('disabled',true);
                        $('#brand-'+(i+1)).val(v.detaildelivery.product.brand.name).attr('disabled',true);
                        $('#type-'+(i+1)).val(v.detaildelivery.product.type.name).attr('disabled',true);
                        $('#subtotal-'+(i+1)).val(v.detaildelivery.sub_total).attr('disabled',true);
                        $('#checkbox-retur-'+(i+1)).attr('disabled',true);
                    });
                    $('#no-item').attr('hidden',true);
                    $('.barang-row').removeAttr('hidden');
                    $('.selectpicker').selectpicker('refresh');
                }
            })
        });

        $('#formRetur .submit').click(function(event){
            event.preventDefault();
            event.stopImmediatePropagation();

            var id = [];
            var idbarang = [];
            var gudang = [];
            var nodo = $('#nodo').val();
            var noretur = $('#noretur').val();
            var tglretur = $('#tglretur').val();
            var returtype = $('#returtype').val();
            var returvalue = $('#totalvalue').val();
            $('.retur').each(function(i,v){
                if($(this).is(':checked'))
                {
                    id.push($(this).closest('tr').find('.id-detail').val());
                    idbarang.push($(this).closest('tr').find('.id-barang').val());
                    gudang.push($(this).closest('tr').find('.gudang').val());
                }
            });
            if(id.length == 0)
            {
                toastr.warning('Anda Belum memilih Retur');
            }else{
                hidemainbuttongroup();
                $.ajax({
                    type:"POST",
                    url:'createsalesreturn',
                    data:{id:id, idbarang:idbarang, nodo:nodo, noretur:noretur, returtype:returtype, tglretur:tglretur, returvalue:returvalue, gudang:gudang},
                    success:function(response){
                        toastr.success("Retur "+noretur+" Berhasil Dibuat",{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                    }
                })
            }

        });

        // $('#formRetur .submit').click(function(event){
        //   event.preventDefault();
        //   event.stopImmediatePropagation();

        //   var validqty = [];
        //   var lebihqty = [];

        //   $('.barang-row').each(function(){
        //     var quota = parseInt($(this).find('.quota').val());
        //     var returqty = parseInt($(this).find('.qty').val());
        //     validqty.push(ceknumber($(this).find('.qty').val()));
        //     if(returqty > quota){
        //       lebihqty.push(false);
        //     }else{
        //       lebihqty.push(true);
        //     }
        //   });

        //   if($('#noso').val() ==0){
        //     toastr.warning('Anda Belum Memilih SO!');
        //   }else if($('#returtype').val == 0 ){
        //     toastr.warning('Anda Belum Memilih SO!');
        //   }else if(validqty.indexOf(false) > -1)
        //   {
        //     toastr.warning('Quantity harus Angka!');
        //   }else if(lebihqty.indexOf(false) > -1){
        //     toastr.warning('Quantity melebihi quota!');
        //   }else{
        //     $('#noretur, .price , #returtype').removeAttr('disabled');
        //     var kode = $('#noretur').val();
        //     $.ajax({
        //       type:"POST",
        //       url:"createreturn",
        //       data:$(this).closest('#formRetur').serialize(),
        //       success:function(response){
        //         toastr.success('Sales Return '+kode+' has been Created!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});

        //       }
        //     })
        //   }
        // })

        $('body').on('keyup','.qty', function(event){
            event.stopImmediatePropagation();
            calculatetotal();
        });

        $('body').on('click','.retur', function(event){
            event.stopImmediatePropagation();
            calculatetotal();
        });

        $('#salesReturTable').on('click','.delete-modal', function(event){
            event.stopImmediatePropagation();
            var noretur = $(this).closest('tr').find('.noretur').html();
            var idretur = $(this).closest('tr').attr('value');
            $('#confirm-delete-retur').attr('value',idretur).attr('nomor',noretur);
            $("#delete-message").html("Yakin ingin menghapus data "+noretur+" ?")
            $('#modal-delete-retur').modal('open');
        });

        $('#confirm-delete-retur').on('click', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-retur').modal('close');
            var id = $(this).attr('value');
            var noretur = $(this).attr('nomor');
            $.ajax({
                type:"POST",
                url:"deletesalesreturn",
                data:{id:id},
                success:function(response){
                    toastr.success('Sales Retur '+noretur+' telah berhasil Dihapus!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                }
            })
        });

        //function
        function firstload()
        {
            $.ajax({
                type:"GET",
                url:"lastsalesreturnnumber",
                success:function(response){
                    $('#noretur').val(response);
                }
            })

            $('.selectpicker').selectpicker('render');

            $('#tglretur').dcalendarpicker({
                format: 'dd-mm-yyyy'
            });
            $('#tglretur').val(moment().format('DD-MM-YYYY'));

            // salesReturTable = $('#salesReturTable').DataTable({ // This is for home page
            //     searching: true,
            //     responsive: true,
            //     'aaSorting':[],
            //     'sDom':'tip',
            //     "bPaginate":true,
            //     "bFilter": false,
            //     "sPaginationType": "full_numbers",
            //     "iDisplayLength": 10,
            //     "language": {
            //         "infoEmpty": "No records to display",
            //         "zeroRecords": "No records to display",
            //         "emptyTable": "No data available in table",
            //     },
            // });

            salesReturTable = $('#salesReturTable').DataTable({ // This is for home page
                searching: true,
                processing: true,
                serverSide: true,
                "aaSorting": [],
                'sDom': 'tip',
                "pageLength": 5,
                ajax: {
                    url : 'getsalesreturntable',
                    data : function (d){
                        d.number = $('#filterReturNumber').val();
                        d.pelanggan = $('#filterPelanggan').val();
                        d.so = $('#filterSoNumber').val();
                    }
                },
                rowId : 'sales_retur_id',
                columns: [
                    { data: 'retur_number', name: 'retur_number', class:'noretur'},
                    { data: 'delivery.so.customer_name', name: 'customer_name'},
                    { data: 'delivery.so.sales_order_number', name: 'sales_order_number'},
                    { data: 'delivery.so.note', name: 'note'},
                    { data: 'action', name: 'action'},
                ],
            });

            $('#filterReturNumber').on('keyup', function () { // This is for news page
                salesReturTable.draw();
            });
            $('#filterPelanggan').on('keyup', function () { // This is for news page
                salesReturTable.draw();
            });
            $('#filterSoNumber').on('keyup', function () { // This is for news page
                salesReturTable.draw();
            });
        }
        function calculatetotal(){
            var total = 0;
            $('.retur').each(function(i,v){
                if($(this).is(':checked')){
                    subtotal = $(this).closest('tr').find('.subtotal').val();
                    total = total+ parseInt(subtotal);
                }
            });
            $('#totalvalue').val(total);
            // var total = 0;
            // $('.barang-row').each(function(key, value){
            //   totalqty = 0;
            //   $(this).find('.qty').each(function(key,value){
            //     totalqty += parseInt($(this).val());
            //   })
            //   total += totalqty * $(this).find('.price').val();
            // });
            // $('#totalvalue').val(total);
        }

        function hidemainbuttongroup(){
            $('.main-button-group a').attr('hidden',true);
        }
    });
</script>
