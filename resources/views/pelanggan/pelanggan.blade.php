<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/toastr.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap-select.min.css">
<style>
  .upload-btn-wrapper {
    position: relative;
    overflow: hidden;
    display: inline-block;
    margin-top:8px;
    width:100%;
  }

  .btn-upload {
    border: 1px solid #c0bebe;
    color: gray;
    border-radius: 2px;
    background-color: white;
    padding: 8px 20px;
    width:100%;
  }

  .upload-btn-wrapper input[type=file] {
    font-size: 100px;
    position: absolute;
    left: 0;
    top: 0;
    opacity: 0;
  }

  img.center {
    display: block;
    margin: 0 auto;
  }
</style>
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<div class="row">
  <div class="col l6 m12 s12">
    <ul class="collapsible" data-collapsible="accordion">
      <li>
        <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">create</i>Form Pelanggan</div>
        <div class="collapsible-body">
          <div class="container-fluid" style="padding-top: 15px">
            <form id="formPelanggan">
              <div class="row">
                <div class="col l12 m12 s12">
                  <div class="row" style="margin: 0px" hidden>
                    <div class="input-field col l12 m12 s12">
                      <input id="total-purchase" class="f-input" type="text" name="total-purchase" disabled/>
                      <label>Total Belanja</label>
                    </div>
                  </div>
                </div>
                <div class="col l6">
                  <div class="row">
                    <div class="input-field col l12 m12 s12">
                      <input id="kode" type="text" class="f-input" placeholder="Kode" name="kode" disabled>
                      <label>Kode</label>
                    </div>
                    <div class="input-field col l12 m12 s12">
                      <input id="namadepan" placeholder="Nama Depan" type="text" class="f-input" name="namadepan">
                      <label>Nama Depan</label>
                    </div>
                    <div class="input-field col l12 m12 s12">
                      <input id="email" placeholder="E-mail" type="text" class="f-input" name="email">
                      <label>E-mail</label>
                    </div>
                    <div class="input-field col l12 m12 s12">
                      <select id="provinsi" class="browser-default selectpicker" data-live-search="true" name="provinsi">
                        <option value="">Select Province</option>
                        @foreach($provinsi as $key => $value)
                          <option value="{{$value->id}}">{{$value->name}}</option>
                        @endforeach
                      </select>
                      <label>Provinsi</label>
                    </div>
                    <div class="input-field col l12 m12 s12">
                      <input id="kodepos" type="text" class="f-input" placeholder="Kode Pos" name="kodepos">
                      <label>Kode Pos</label>
                    </div>
                  </div>
                </div>
                <div class="col l6">
                  <div class="row">
                    <div class="input-field col l12 m12 s12">
                      <row>
                        <div class="group-input">
                          <label>Level Customer</label>
                          <select id="level" class="browser-default selectpicker" data-live-search="true" name="level" disabled>
                            @foreach($level as $key=>$value)
                              <option value="{{$value->customer_level_id}}">{{$value->customer_level_name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </row>
                    </div>
                    <div class="input-field col l12 m12 s12">
                      <input id="namabelakang" type="text" class="f-input" placeholder="Nama Belakang" name="namabelakang">
                      <label>Nama Belakang</label>
                    </div>
                    <div class="input-field col l12 m12 s12">
                      <input id="nik" placeholder="NIK" type="text" class="f-input" name="nik">
                      <label>NIK</label>
                    </div>
                    <div class="input-field col l12 m12 s12">
                      <select id="kota" class="browser-default selectpicker" data-live-search="true" name="kota">
                        <option value="">Select Province</option>
                      </select>
                      <label>Kota</label>
                    </div>
                    <div class="input-field col l12 m12 s12">
                      <input id="booknumber" placeholder="Nomor Buku" type="text" class="f-input" name="booknumber">
                      <label>Nomor Buku</label>
                    </div>
                  </div>
                </div>
                <div class="col l12 m12 s12">
                  <div class="row">
                    <div class="input-field col l12 m12 s12">
                      <textarea id="alamat" class="f-txt" placeholder="Alamat" name="alamat"></textarea>
                      <label>Alamat</label>
                    </div>
                    <div class="input-field col l4 m4 s12">
                      <input id="telp1" type="text" class="f-input" placeholder="Telp 1" name="telp1">
                      <label>Telp 1</label>
                    </div>
                    <div class="input-field col l4 m4 s12">
                      <input id="telp2" type="text" class="f-input" placeholder="Telp 2" name="telp2">
                      <label>Telp 2</label>
                    </div>
                    <div class="input-field col l4 m4 s12">
                      <input id="fax" type="text" class="f-input" placeholder="Fax" name="fax">
                      <label>Fax</label>
                    </div>
                    <div class="input-field col l12 m12 s12">
                      <label for="">Identitas (max size = 1Mb)</label>
                      <div class="upload-btn-wrapper">
                        <button class="btn-upload">Upload a file</button>
                        <input id="identity" type="file" name="identity" />
                      </div>
                      <p id="label-identity" for="identity"></p>
                    </div>
                    <div class="col l12">
                      <img id="identity-img" class="center" src='' alt="">
                    </div>
                    <div class="col l12">
                    </div>

                  </div>
                  <div class="row">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col l12 m12 s12">
                  <button id="btn-submit" type="submit" class="btn btn-raised btn-sm light-blue darken-2" mode="save">simpan</button>
                  <a href="#" class="btn btn-raised btn-sm orange reset">batal</a>
                </div>
              </div>
              {{csrf_field()}}
            </form>
          </div>
        </div>
      </li>
    </ul>
  </div>
  <div class="col l6 m12 s12">
    <ul class="collapsible" data-collapsible="accordion">
      <li>
        <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Pelanggan</div>
        <div class="collapsible-body">
          <div class="container-fluid">
            <div class="row">
              <div class="input-field col l6">
                <input id="filterNama" type="text" class="f-input" placeholder="Search">
                <label>Filter by Nama</label>
              </div>
              <div class="input-field col l6">
                <input id="filterNoBuku" type="text" class="f-input" placeholder="Search">
                <label>Filter by No Buku</label>
              </div>
              <div class="input-field col l6">
                <input id="filterNoPhone" type="text" class="f-input" placeholder="Search">
                <label>Filter by No HP</label>
              </div>
              <div class="col l12 m12 s12">
                <table id="pelangganTable" class="table table-bordered display nowrap dataTable dtr-inline">
                  <thead>
                  <tr>
                    <th>Pelanggan</th>
                    <th>No Buku</th>
                    <th>No HP</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  {{--@foreach($customer as $row => $value)--}}
                  {{--<tr value="{{$value->customer_id}}">--}}
                  {{--<td>{{$value->first_name.' '.$value->last_name}}</td>--}}
                  {{--<td>--}}
                  {{--<a class="btn btn-sm btn-raised light-blue darken-2 edit"><i class="material-icons">edit</i></a>--}}
                  {{--<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>--}}
                  {{--<a href="#" class="btn btn-danger block-pelanggan"><i class="material-icons">block</i></a>--}}
                  {{--</td>--}}
                  {{--</tr>--}}
                  {{--@endforeach--}}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
  <div class="col l6 m12 s12">
    <ul class="collapsible" data-collapsible="accordion">
      <li>
        <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Pelanggan</div>
        <div class="collapsible-body">
          <div class="container-fluid">
            <div class="row">
              <div class="col l12 m12 s12">
                </br>
                <table id="blockedPelangganTable" class="table table-bordered display nowrap dataTable dtr-inline">
                  <thead>
                  <tr>
                    <th>Pelanggan</th>
                    <th>Reason</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($blocked as $row => $value)
                    <tr value="{{$value->customer_id}}">
                      <td>{{$value->first_name.' '.$value->last_name}}</td>
                      <td>{{$value->block_reason}}</td>
                      <td>
                        <a href="#" class="btn btn-primary light-blue unblock-pelanggan"><i class="material-icons">rotate_left</i></a>
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
</div>
<!-- Modal Delete Pelanggan -->
<div id="delete_data" class="modal">
  <div class="modal-content">
    <h5>Yakin ingin menghapus data berikut?</h5>
    <div class="col l12">
      <div class="col l6">
        <table id="modal-delete-data">

        </table>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko red white-text delete">hapus</a>
  </div>
</div>
<div id="modal-block-pelanggan" class="modal">
  <div class="modal-content row">
    <div class="col l12">
      <h3><label for="">Konfirmasi</label></h3>
      <form id="formBlockPelanggan">
        <label id="block-pelanggan-message">Apakah Yakin akan menghapus <span id="block-pelanggan-detail"></span></label>
        <div class="input-field">
          <label for="">Alasan</label>
          <input type="text" name="reason" id="block-reason" class="f-input">
        </div>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect btn-stoko">Batal</a>
      <a id="confirm-block-pelanggan" class="modal-action modal-close waves-effect btn-stoko red white-text">Block</a>
    </div>
  </div>
</div>
<div id="modal-unblock-pelanggan" class="modal">
  <div class="modal-content row">
    <div class="col l12">
      <h3><label for="">Konfirmasi</label></h3>
      <label id="unblock-product-message">Apakah Yakin akan menngembalikan <span id="unblock-pelanggan-detail"></span></label>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect btn-stoko">Batal</a>
      <a id="confirm-unblock-pelanggan" class="modal-action modal-close waves-effect btn-stoko light-blue white-text">Unblock</a>
    </div>
  </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn-submit').attr('mode', 'save');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'js/bootstrap-select.min.js',
            dataType: "script",
        });

        $('.selectpicker').selectpicker('render');

        // pelangganTable = $('#pelangganTable').DataTable({ // This is for home page
        //     searching: true,
        //     responsive: true,
        //     'sDom': 'ti',
        //     'pagingType': 'full',
        //     "language": {
        //         "infoEmpty": "No records to display",
        //         "zeroRecords": "No records to display",
        //         "emptyTable": "No data available in table",
        //     },
        // });

        pelangganTable = $('#pelangganTable').DataTable({ // This is for home page
            searching: true,
            processing: true,
            serverSide: true,
            "aaSorting": [],
            'sDom': 'tip',
            "pageLength": 5,
            ajax: {
                url : 'getpelanggantable',
                data : function (d){
                    d.nama = $('#filterNama').val();
                    d.booknumber = $('#filterNoBuku').val();
                    d.phone = $('#filterNoPhone').val();
                }
            },
            rowId : 'customer_id',
            columns: [
                { data: 'fullname', name: 'fullname'},
                { data: 'book_number', name: 'book_number'},
                { data: 'phone', name: 'phone'},
                { data: 'action', name: 'action'},
            ],
        });

        blockedPelangganTable = $('#blockedPelangganTable').DataTable({ // This is for home page
            searching: true,
            responsive: true,
            'sDom': 'ti',
            'pagingType': 'full',
            "language": {
                "infoEmpty": "No records to display",
                "zeroRecords": "No records to display",
                "emptyTable": "No data available in table",
            },
        });

        $('#filterNama').on('keyup', function () { // This is for news page
            pelangganTable.draw();
        });

        $('#filterNoBuku').on('keyup', function () { // This is for news page
            pelangganTable.draw();
        });

        $('#filterNoPhone').on('keyup', function () { // This is for news page
            pelangganTable.draw();
        });

        $('#provinsi').on('change',function(){
            var provinsi = $('#provinsi option:selected').val();
            getCity(provinsi);
        });

        $('#formPelanggan').submit(function(event){
            event.preventDefault();
            var mode = $(this).find('#btn-submit').attr('mode');
            var empty = required(['namadepan', 'namabelakang','alamat','telp1','provinsi','kota']);
            var validangka = cekmanynumbers(['telp1','telp2','kodepos']);
            var validbank = 0;
            var validphone = 0;
            var validnik = 0;
            var customer = $('#kode').val();
            var identity = $('#identity').val();
            if(identity != "")
            {
                var filesize = $('#identity')[0].files[0].size;
            }else{
                var filesize = 0;
            }

            $.ajax({
                type:"GET",
                url:"validPelangganPhone",
                data: {phone: $('#telp1').val(), customer:customer},
                async : false,
                success:function(response){
                    validphone = response;
                }
            });

            $.ajax({
                type:"GET",
                url:"validPelangganNik",
                data: {nik: $('#nik').val(), customer:customer},
                async : false,
                success:function(response){
                    validnik = response;
                }
            });


            if($('#bank').val() != "")
            {
                var validbank = required(['atasnama','bank','norek']);
            }
            if(empty != '0')
            {
                toastr.warning(empty+' tidak boleh kosong!');
            }else if(validphone != 0){
                toastr.warning('no telepon telah terdaftar!');
            }else if(validnik != 0){
                toastr.warning('nik telah terdaftar!');
            }else if(validangka != '0'){
                toastr.warning(validangka+' harus angka');
            }else if(validbank != '0'){
                toastr.warning(validbank+' tidak boleh kosong');
            }else if(filesize > 1048576){
                toastr.warning("File Upload melebihi kuota 1 Mb")
            }
            else{
                $('#level').removeAttr('disabled');
                if (mode == 'save')
                {
                    var formData = new FormData($('#formPelanggan')[0]);
                    jQuery.each(jQuery('#identity')[0].files, function(i, v) {
                        formData.append('identity',v);
                    });

                    $.ajax({
                        type:"POST",
                        url:"createPelanggan",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success:function(response){
                            toastr.success('Pelanggan Telah Berhasil Dibuat!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                        }
                    });
                }else if(mode == 'edit'){

                    var formData = new FormData($('#formPelanggan')[0]);
                    jQuery.each(jQuery('#identity')[0].files, function(i, v) {
                        formData.append('identity',v);
                    });

                    var id = $(this).find('#kode').val();
                    formData.append('id',id);

                    $.ajax({
                        type:"POST",
                        url:"updatePelanggan",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success:function(response){
                            toastr.success('Pelanggan Telah Berhasil Diubah!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                        }
                    });
                }
            }
        });


        $('body').on('click','#formPelanggan .reset', function(event){
            event.stopImmediatePropagation();
            $(this).closest('#formPelanggan').find("input[type=text], textarea").val("");
            $('#btn-submit').attr('mode', 'save');
        });

        $('body').on('click','#pelangganTable .edit', function(event){
            event.stopImmediatePropagation();
            var id = $(this).closest('tr').attr('value');
            $.ajax({
                type:"GET",
                url:"getPelangganData",
                data: {id:id},
                success:function(response){
                    $('#provinsi').val(response.customer.provinsi);
                    $('#kode').val(response.customer.customer_id);
                    $('#booknumber').val(response.customer.book_number);
                    $('#email').val(response.customer.email);
                    $('#namadepan').val(response.customer.first_name);
                    $('#namabelakang').val(response.customer.last_name);
                    $('#telp1').val(response.customer.phone);
                    $('#telp2').val(response.customer.phone2);
                    $('#fax').val(response.customer.fax);
                    $('#level').val(response.customer.level.customer_level_id);
                    $('#kodepos').val(response.customer.postal_code);
                    $('#nik').val(response.customer.nik);
                    $('#alamat').val(response.customer.address);
                    $('#identity-img').attr('src',response.customer.identity);
                    $('#total-purchase').val(accounting.formatMoney(response.total_this_state,'',0,'.',','));
                    $('#total-purchase').closest('.row').removeAttr('hidden');
                    $('.selectpicker').selectpicker('refresh');

                    getCity(response.customer.provinsi, response.customer.city_id);
                }
            });
            $('#btn-submit').attr('mode', 'edit');
        });

        $('#pelangganTable').on('click','.delete-modal', function(event){
            event.preventDefault();
            var id = $(this).closest('tr').attr('value');
            $.ajax({
                type:"GET",
                url:"modalPelangganData",
                async:false,
                data:{
                    id:id,
                },
                success:function(response){
                    $('#modal-delete-data').html(response);
                }
            });
            $('#delete_data').modal('open');
        });

        $(".delete").on('click',function(){
            var id = $(this).closest('.modal').find('#id-delete').attr('value');
            console.log(id);
            $.ajax({
                type:"Get",
                url:"deletePelanggan",
                data:{id:id},
                success:function(response){
                    toastr.success('Berhasil Menghapus Pelanggan!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                }
            });
        });

        $('#pelangganTable').on('click','.block-pelanggan', function (event) {
            event.stopImmediatePropagation();
            event.preventDefault();
            var id = $(this).closest('tr').attr('value');
            $.ajax({
                method: "get",
                url: "getPelangganData",
                data: {id: id},
                success: function (response) {
                    $("#block-pelanggan-detail").html(response.customer.first_name+' '+response.customer.last_name +" dengan Kode " + response.customer.customer_id + " ?");
                    $("#confirm-block-pelanggan").attr('value', id);
                    $('#modal-block-pelanggan').modal('open');
                }
            })
        });

        $('#confirm-block-pelanggan').on('click', function (event) {
            event.stopImmediatePropagation();
            event.preventDefault();
            var id = $(this).attr('value');
            var reason = $('#formBlockPelanggan').find('#block-reason').val();
            $.ajax({
                method: "post",
                url: "blockPelanggan",
                data: {id: id, reason: reason},
                success: function (response) {
                    $('#modal-block-pelanggan').modal('close');
                    toastr.success('Berhasil Block Pelanggan!',
                        {
                            "onShow": setTimeout(function () {
                                $('.side-nav .nav-item.active a').click();
                            }, 2600)
                        });
                }
            })
        });

        $('.unblock-pelanggan').on('click', function (event) {
            event.stopImmediatePropagation();
            event.preventDefault();
            var id = $(this).closest('tr').attr('value');
            $.ajax({
                method: "get",
                url: "getPelangganData",
                data: {id: id},
                success: function (response) {
                    $("#unblock-pelanggan-detail").html(response.customer.first_name+' '+response.customer.last_name +" dengan Kode " + response.customer.customer_id + " ?");
                    $("#confirm-unblock-pelanggan").attr('value', id);
                    $('#modal-unblock-pelanggan').modal('open');
                }
            })
        });

        $('#confirm-unblock-pelanggan').on('click', function (event) {
            event.stopImmediatePropagation();
            event.preventDefault();
            var id = $(this).attr('value');
            $.ajax({
                method: "post",
                url: "unblockPelanggan",
                data: {id: id},
                success: function (response) {
                    $('#modal-unblock-pelanggan').modal('close');
                    toastr.success('Berhasil Unblock Pelanggan!',
                        {
                            "onShow": setTimeout(function () {
                                $('.side-nav .nav-item.active a').click();
                            }, 2600)
                        });
                }
            })
        });

        $('input[name="identity"]').change(function(){
            var fileName = $(this).val();
            var fileName = fileName.substring(fileName.lastIndexOf("\\") + 1, fileName.length);
            var size = bytesToSize($('#identity')[0].files[0].size);
            $('#label-identity').html(fileName+' ('+size+')');
        });

        $('.selectpicker').selectpicker('render');

        //function
        function getCity(provinsi,city)
        {
            city = city || 0;
            $.ajax({
                type:"GET",
                url:"cities",
                data:{
                    provinsi:provinsi, city:city,
                },
                success:function(response){
                    $('#kota').html(response);
                },
                complete:function(){
                    $('.selectpicker').selectpicker('refresh');
                }
            });
        }

        function bytesToSize(bytes) {
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
            if (bytes == 0) return '0 Byte';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
        };
    });
</script>
