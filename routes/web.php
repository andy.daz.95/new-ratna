<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();
Route::get('/logout', 'UserController@logout');
Route::get('/ceklogin', 'UserController@dologin')->name('dologin');

Route::get('/import', 'ImportController@showImportPage');
Route::post('/importCategory', 'ImportController@ImportCategory');
Route::post('/importBrand', 'ImportController@ImportBrand');
Route::post('/importType', 'ImportController@ImportType');
Route::post('/importProduct', 'ImportController@ImportProduct');
Route::post('/importCustomer', 'ImportController@ImportCustomer');
Route::post('/updateCustomer', 'ImportController@updateCustomer');

Route::group(['middleware' => 'auth'], function () {

	Route::get('/testsendmail/{id}', 'SalesInvoiceController@testMail');

  Route::get('/', 'HomeController@showMainPage');
  Route::get('/homes', 'HomeController@showHomePage');
  Route::get('/home/news','HomeController@loadhomenews');
  Route::get('/home/salesinvoicedue','HomeController@loadSalesInvoiceDue');
  Route::get('/home/stock','HomeController@loadStockOverview');
  Route::get('/home/expense','HomeController@loadExpense');
  Route::get('/home/salesachievement','HomeController@loadSalesAchievement');
  Route::get('/home/purchaseachievement','HomeController@loadPurchaseAchievement');
  Route::get('/home/accountreceiveable','HomeController@loadAccountReceiveable');
  Route::get('/home/piutang-so','HomeController@loadhomepiutangso');
  Route::get('/home/piutang-po','HomeController@loadhomepiutangpo');

	Route::get('/sendmail/{id}', 'SalesInvoiceController@sendMail');

//  Route::get('/user_profile', 'UserController@showProfilePage');
	Route::get('/user_profile', 'UserController@showProfilePage');
	Route::get('/manage_user', 'UserController@showManageUserPage');
	Route::get('/change_password', 'UserController@showChangePassword');
	Route::get('/get_account', 'UserController@getAccountData');
	Route::get('/get_account_manage', 'UserController@getAccountManage');
	Route::post('/update_account', 'UserController@updateAccount');
	Route::post('/update_account_manage', 'UserController@updateAccountManage');
	Route::post('/create_account', 'UserController@createAccount');
	Route::post('/update_password', 'UserController@changePassword');
	Route::post('/delete_account', 'UserController@deleteAccount');

  Route::get('/pelanggan', 'PelangganController@showPelangganPage');
  Route::get('/getpelanggantable', 'PelangganController@getPelangganTable');
  Route::get('/getPelangganData', 'PelangganController@getPelangganData');
  Route::get('/validPelangganPhone', 'PelangganController@validPelangganPhone');
  Route::get('/validPelangganNik', 'PelangganController@validPelangganNik');
  Route::get('/modalPelangganData', 'PelangganController@modalPelangganData');
  Route::get('/getcustomerbalance', 'PelangganController@getCustomerBalance');
  Route::get('/getPelangganInfo', 'PelangganController@getPelangganInfo');
  Route::get('/deletePelanggan', 'PelangganController@deletePelanggan');
	Route::post('/createPelanggan', 'PelangganController@createPelanggan');
	Route::post('/updatePelanggan', 'PelangganController@updatePelanggan');
	Route::post('/blockPelanggan', 'PelangganController@blockPelanggan');
	Route::post('/unblockPelanggan', 'PelangganController@unblockPelanggan');
	Route::get('/balance', 'PelangganController@showBalancePage');
  Route::get('/detail-balance', 'PelangganController@showBalanceDetail');

  Route::get('/supplier', 'SupplierController@showSupplierPage');
  Route::get('/getSupplierData', 'SupplierController@getSupplierData');
  Route::get('/filterSupplier', 'SupplierController@filterSupplier');
  Route::get('/modalSupplierData', 'SupplierController@modalSupplierData');
  Route::get('/deleteSupplier', 'SupplierController@deleteSupplier');
  Route::post('/createSupplier', 'SupplierController@createSupplier');
  Route::post('/updateSupplier', 'SupplierController@updateSupplier');

  Route::get('/target', 'SupplierController@showTargetPage');
  Route::post('/createtarget', 'SupplierController@createTarget');

  Route::get('/kategoribarang', 'KategoriBarangController@showKategoriBarangPage');
  Route::get('/getKategoriData', 'KategoriBarangController@getKategoriData');
  Route::get('/modalKategoriData', 'KategoriBarangController@modalkategoriData');
  Route::get('/deleteKategoriBarang', 'KAtegoriBarangController@deleteKategoriBarang');
  Route::post('/createKategoriBarang', 'KategoriBarangController@createKategoriBarang');
  Route::post('/updateKategoriBarang', 'KategoriBarangController@updateKategoriBarang');

  Route::get('/brandbarang', 'BrandBarangController@showBrandBarangPage');
  Route::get('/getbrandtable', 'BrandBarangController@getBrandTable');
  Route::get('/getBrandData', 'BrandBarangController@getBrandData');
  Route::get('/getBrandOnKategori', 'BrandBarangController@getBrandOnKategori');
  Route::get('/modalBrandData', 'BrandBarangController@modalBrandData');
  Route::get('/deleteBrandBarang', 'BrandBarangController@deleteBrandBarang');
  Route::post('/createBrandBarang', 'BrandBarangController@createBrandBarang');
  Route::post('/updateBrandBarang', 'BrandBarangController@updateBrandBarang');

  Route::get('/tipebarang', 'TipeBarangController@showTipeBarangPage');
  Route::get('/gettipetable', 'TipeBarangController@getTipeTable');
  Route::get('/getTipeData', 'TipeBarangController@getTipeData');
  Route::get('/getTipeOnBrand', 'TipeBarangController@getTipeOnBrand');
  Route::get('/getKategoriOnBrand', 'TipeBarangController@getKategoriOnBrand');
  Route::get('/modalTipeData', 'TipeBarangController@modalTipeData');
  Route::get('/deleteTipeBarang', 'TipeBarangController@deleteTipeBarang');
  Route::post('/createTipeBarang', 'TipeBarangController@createTipeBarang');
  Route::post('/createTipeBarangFromPo', 'TipeBarangController@createTipeBarangFromPo');
  Route::post('/updateTipeBarang', 'TipeBarangController@updateTipeBarang');

  Route::get('/barang', 'BarangController@showBarangPage');
  Route::get('/getBarangData', 'BarangController@getBarangData');
  Route::get('/getBarangUseBarcode', 'BarangController@getBarangUsingBarcode');
  Route::get('/filterBarang', 'BarangController@filterBarang');
  Route::get('/modalBarangData', 'BarangController@modalBarangData');
  Route::get('/deleteBarang', 'BarangController@deleteBarang');
  Route::post('/createBarang', 'BarangController@createBarang');
  Route::post('/updateBarang', 'BarangController@updateBarang');
	Route::get('/getbaranginfo','BarangController@getBarangInfo');

  Route::get('/satuan', 'SatuanController@showSatuanPage');
  Route::get('/getSatuanData', 'SatuanController@getSatuanData');
  Route::get('/filterSatuan', 'SatuanController@filterSatuan');
  Route::get('/modalSatuanData', 'SatuanController@modalSatuanData');
  Route::get('/deleteSatuan', 'SatuanController@deleteSatuan');
  Route::post('/createSatuan', 'SatuanController@createSatuan');
  Route::post('/updateSatuan', 'SatuanController@updateSatuan');

  Route::get('/news', 'NewsController@showNewsPage');
  Route::get('/getNewsData', 'NewsController@getNewsData');
  Route::get('/filterNews', 'NewsController@filterNews');
  Route::get('/modalNewsData', 'NewsController@modalNewsData');
  Route::get('/deleteNews', 'NewsController@deleteNews');
  Route::post('/createNews', 'NewsController@createNews');
  Route::post('/updateNews', 'NewsController@updateNews');

  Route::get('/bank', 'BankController@showBankPage');
  Route::get('/getBankData', 'BankController@getBankData');
  Route::get('/modalBankData', 'BankController@modalBankData');
	Route::get('/deleteBank', 'BankController@deleteBank');
	Route::post('/createBank', 'BankController@createBank');
	Route::post('/updateBank', 'BankController@updateBank');

	Route::get('/gudang', 'GudangController@showGudangPage');
	Route::get('/getGudangData', 'GudangController@getGudangData');
	Route::get('/filterGudang', 'GudangController@filterGudang');
	Route::get('/modalGudangData', 'GudangController@modalGudangData');
	Route::get('/deleteGudang', 'GudangController@deleteGudang');
	Route::post('/createGudang', 'GudangController@createGudang');
	Route::post('/updateGudang', 'GudangController@updateGudang');

	Route::get('/levelpelanggan', 'LevelPelangganController@showLevelPelangganPage');
	Route::get('/getCustomerLevelData', 'LevelPelangganController@getCustomerLevelData');
	Route::post('/createCustomerLevel', 'LevelPelangganController@createCustomerLevel');
	Route::post('/updateCustomerLevel', 'LevelPelangganController@updateCustomerLevel');

	Route::get('/expensecategory','ExpenseCategoryController@showExpenseCategoryPage');
	Route::get('/getexpensecategorydata','ExpenseCategoryController@getExpenseCategoryData');
	Route::get('/modalexpensecategorydata', 'ExpenseCategoryController@modalExpenseCategoryData');
	Route::post('/createexpensecategory','ExpenseCategoryController@createExpenseCategory');
  Route::post('/updateexpensecategory','ExpenseCategoryController@updateExpenseCategory');
  Route::get('/deleteexpensecategory','ExpenseCategoryController@deleteExpenseCategory');

	Route::get('/shippingterm','ShippingTermController@showShippingTermPage');
	Route::get('/getshippingtermdata','ShippingTermController@getShippingTermData');
	Route::get('/modalshippingtermdata', 'ShippingTermController@modalShippingTermData');
	Route::post('/createshippingterm','ShippingTermController@createShippingTerm');
	Route::post('/updateshippingterm','ShippingTermController@updateShippingTerm');
	Route::get('/deleteshippingterm','ShippingTermController@deleteShippingTerm');

  Route::get('/salesorder', 'SalesOrderController@showSalesOrderPage');
  Route::get('/getsalesordertable', 'SalesOrderController@getsalesOrderTable');
  Route::get('/getsalesorder', 'SalesOrderController@getsalesOrder');
  Route::get('/lastsalesordernumber', 'SalesOrderController@getLastSalesNumber');
  Route::post('/createsalesorder', 'SalesOrderController@createSalesOrder');
	Route::post('/createdraftsalesorder', 'SalesOrderController@createDraftSalesOrder');
	Route::post('/updatesalesorder', 'SalesOrderController@updateSalesOrder');
	Route::post('/deletesalesorder', 'SalesOrderController@deleteSalesOrder');
	Route::get('/downloadsalesorder/{id}', 'SalesOrderController@downloadSalesOrder');
	Route::post('/approvedraft', 'SalesOrderController@approveDraft');
	Route::post('/rejectdraft', 'SalesOrderController@rejectDraft');

	Route::get('/deliveryorder', 'DeliveryOrderController@showDeliveryOrderPage');
  Route::get('/getdeliveryordertable', 'DeliveryOrderController@getDeliveryOrderTable');
  Route::get('/getdeliveryorder', 'DeliveryOrderController@getDeliveryOrder');
  Route::get('/lastdonumber', 'DeliveryOrderController@getLastDeliveryNumber');
  Route::get('/getsofordelivery', 'DeliveryOrderController@getSalesOrderData');
  Route::get('/getStockInWarehouse', 'DeliveryOrderController@getStockInWarehouse');
  Route::post('/createdeliveryorder', 'DeliveryOrderController@createDeliveryOrder');
  Route::post('/updatedeliveryorder', 'DeliveryOrderController@updateDeliveryOrder');
  Route::post('/deletedeliveryorder', 'DeliveryOrderController@deleteDeliveryOrder');
  Route::get('/downloaddeliveryorder/{id}', 'DeliveryOrderController@downloadDeliveryOrder');

	Route::get('/salesinvoice','SalesInvoiceController@showSalesInvoicePage');
	Route::get('/getsalesinvoice','SalesInvoiceController@getSalesInvoice');
	Route::get('/getsoforinvoice','SalesInvoiceController@getSalesOrderData');
	Route::get('/lastsalesinvoicenumber','SalesInvoiceController@getLastInvoiceNumber');
	Route::post('/createsalesinvoice','SalesInvoiceController@createInvoice');
	Route::post('/updatesalesinvoice','SalesInvoiceController@updateInvoice');
	Route::post('/deletesalesinvoice','SalesInvoiceController@deleteInvoice');

	Route::get('/salespayment','SalesPaymentController@showSalesPaymentPage');
	Route::get('/getsalespaymenttable','SalesPaymentController@getSalesPaymentTable');
	Route::get('/getcreditdetails','SalesPaymentController@getCreditDetails');
	Route::get('/getsofromcustomer','SalesPaymentController@getCustomerData');
	Route::get('/getsalespayment','SalesPaymentController@getSalesPayment');
	Route::get('/lastsalespaymentnumber','SalesPaymentController@getLastPaymentNumber');
	Route::get('/getcustomerforpayment','SalesPaymentController@getCustomerData');
	Route::post('/createpayment','SalesPaymentController@CreatePayment');
	Route::post('/deletesalespayment','SalesPaymentController@deletePayment');
	Route::get('/downloadsalespayment/{id}', 'SalesPaymentController@downloadPaymentCredit');

	Route::get('/salesreturn','SalesReturnController@showSalesReturnPage');
  Route::get('/getsalesreturntable','SalesReturnController@getSalesReturnTable');
  Route::get('/getsalesreturn','SalesReturnController@getSalesReturn');
  Route::get('/lastsalesreturnnumber','SalesReturnController@getLastReturNumber');
  // Route::get('/getsoforreturn','SalesReturnController@getSalesOrderData');
	Route::get('/getdeliveryforreturn','SalesReturnController@getDeliveryOrderData');
	Route::post('/createsalesreturn','SalesReturnController@createReturn');
	Route::post('/deletesalesreturn','SalesReturnController@deleteReturn');

	Route::get('/balancemanagement','BalanceManagementController@showBalanceManagementPage');
  Route::get('/lastbalancetransactionnumber','BalanceManagementController@getLastTransactionNumber');
  Route::post('/createbalancetransaction','BalanceManagementController@createBalanceTransaction');
  Route::post('/deletebalancetransaction','BalanceManagementController@deleteBalanceTransaction');

  Route::get('/show_grafik_target','ReportController@showGrafikTarget');
  Route::get('/grafik_target','ReportController@getGrafikTarget');
  Route::get('/grafik_penjualan','ReportController@showGrafikPenjualan');
  Route::get('/grafik_penjualan/{startdate}/{enddate}','ReportController@getGrafikData');

  Route::get('/cities', 'ExtendController@listCities');
  Route::get('/modalListBarang', 'ExtendController@listBarang');
  Route::get('/getBarangModal', 'ExtendController@getBarangModal');

  Route::get('/refundhistory','SalesReturnController@showRefundHistory');

  Route::get('/purchaseorder', 'PurchaseOrderController@showPurchaseOrderPage');
  Route::get('/getpurchaseordertable', 'PurchaseOrderController@getPurchaseOrderTable');
  Route::get('/getpurchaseorder', 'PurchaseOrderController@getPurchaseOrder');
  Route::get('/lastpurchaseordernumber','PurchaseOrderController@getLastPurchaseNumber');
	Route::get('/getpoforhargajual', 'PurchaseOrderController@getPurchaseOrderData');
  Route::post('/createpurchaseorder', 'PurchaseOrderController@createPurchaseOrder');
  Route::post('/releasepurchaseorder', 'PurchaseOrderController@releasePurchaseOrder');
  Route::post('/rejectpurchaseorder', 'PurchaseOrderController@rejectPurchaseOrder');
  Route::post('/revisepurchaseorder', 'PurchaseOrderController@revisePurchaseOrder');
  Route::post('/updatepurchaseorder', 'PurchaseOrderController@updatePurchaseOrder');
  Route::post('/deletepurchaseorder', 'PurchaseOrderController@deletePurchaseOrder');
  Route::get('/downloadpurchaseorder/{id}', 'PurchaseOrderController@downloadPurchaseOrder');

	Route::get('/hargajual', 'PurchaseOrderController@showHargaJualPage');
	Route::post('/updatehargajual', 'PurchaseOrderController@updateHargaJual');

	Route::get('/penerimaanbarang', 'PenerimaanController@showPenerimaanBarangPage');
	Route::get('/getpenerimaantable', 'PenerimaanController@getPenerimaanTable');
	Route::get('/getpenerimaan', 'PenerimaanController@getPenerimaan');
	Route::get('/lastbpbnumber','PenerimaanController@getLastBpbNumber');
	Route::get('/getpoforpenerimaan', 'PenerimaanController@getPurchaseOrderData');
	Route::post('/cancelbarang','PenerimaanController@cancelBarang');
	Route::post('/createpenerimaanbarang','PenerimaanController@createPenerimaanBarang');
	Route::post('/updatepenerimaanbarang','PenerimaanController@updatePenerimaanBarang');
	Route::post('/deletepenerimaanbarang','PenerimaanController@deletePenerimaanBarang');
	Route::get('/downloadgrbarcode/{id}', 'PenerimaanController@downloadPenerimaanBarCode');
	Route::get('/excelgrbarcode/{id}', 'PenerimaanController@excelPenerimaanBarCode');

	Route::get('/purchaseinvoice', 'PurchaseInvoiceController@showPurchaseInvoicePage');
	Route::get('/getpurchaseinvoicetable','PurchaseInvoiceController@getPurchaseInvoiceTable');
	Route::get('/getpurchaseinvoice','PurchaseInvoiceController@getPurchaseInvoice');
	Route::get('/lastpurchaseinvoicenumber','PurchaseInvoiceController@getLastInvoiceNumber');
	Route::get('/getpoforinvoice','PurchaseInvoiceController@getPurchaseOrderData');
	Route::post('/createpurchaseinvoice','PurchaseInvoiceController@createInvoice');
	Route::post('/updatepurchaseinvoice','PurchaseInvoiceController@updateInvoice');
	Route::post('/deletepurchaseinvoice','PurchaseInvoiceController@deleteInvoice');

	Route::get('/purchasepayment', 'PurchasePaymentController@showPurchasePaymentPage');
	Route::get('/getpurchasepaymenttable','PurchasePaymentController@getPurchasePaymentTable');
	Route::get('/getpurchasepayment','PurchasePaymentController@getPurchasePayment');
	Route::get('/lastpurchasepaymentnumber','PurchasePaymentController@getLastPaymentNumber');
	Route::get('/getpurchaseinvoiceforpayment','PurchasePaymentController@getInvoiceData');
	Route::post('/createpurchasepayment','PurchasePaymentController@CreatePayment');
	Route::post('/updatepurchasepayment','PurchasePaymentController@updatePayment');
	Route::post('/deletepurchasepayment','PurchasePaymentController@deletePayment');
	Route::get('/downloadpurchasepayment/{id}', 'PurchasePaymentController@downloadPurchasePayment');

	Route::get('/purchasereturn','PurchaseReturnController@showPurchaseReturnPage');
  Route::get('/getpurchasereturntable','PurchaseReturnController@getPurchaseReturnTable');
  Route::get('/getpurchasereturn','PurchaseReturnController@getPurchaseReturn');
  Route::get('/lastpurchasereturnnumber','PurchaseReturnController@getLastReturNumber');
  Route::get('/getgoodreceiveforreturn','PurchaseReturnController@getGoodReceiveData');
  Route::post('/createpurchasereturn','PurchaseReturnController@createreturn');
  Route::post('/updatepurchasereturn','PurchaseReturnController@updatereturn');
  Route::post('/deletepurchasereturn','PurchaseReturnController@deletereturn');

  Route::get('/depositopayment','DepositoPaymentController@showDepositoPaymentPage');
  Route::get('/lastdepositopaymentnumber','DepositoPaymentController@getLastPaymentNumber');
  Route::get('/getsupplierdeposit','DepositoPaymentController@getSupplierDeposit');
  Route::post('/createdepositopayment','DepositoPaymentController@createDepositoPayment');
  Route::post('/deletedepositopayment','DepositoPaymentController@deleteDepositoPayment');

  Route::get('/stockoverview', 'InventoryController@showStockOverviewPage');
  Route::get('getstockoverviewtable','InventoryController@getStockOverviewTable');
	Route::get('/detailStock', 'InventoryController@detailStock');
	Route::get('/printproduct/{id}','InventoryController@printProduct');
	Route::post('/blockproduct', 'InventoryController@blockProduct');
	Route::post('/unblockproduct', 'InventoryController@unblockProduct');
	Route::post('/updatebarangfromoverview', 'InventoryController@updateBarangFromOverview');

	Route::get('/stocktransfer', 'InventoryController@showStockTransferPage');
  Route::get('/getstocktransfer', 'InventoryController@getStockTransfer');
  Route::get('/laststocktransfernumber', 'InventoryController@getLastTransferNumber');
  Route::get('/getBarangTransfer', 'InventoryController@getBarangTransfer');
  Route::post('/createstocktransfer', 'InventoryController@createStockTransfer');

  Route::get('/kartustock', 'InventoryController@showKartuStockPage');
  Route::get('/getkartustock', 'InventoryController@getKartuStock');

  Route::get('/stockcorrection', 'InventoryController@showStockCorrection');
	Route::get('/stockcorrection/export', 'InventoryController@exportStock');
	Route::post('/stockcorrection/import', 'InventoryController@importStock');

	Route::get('/expense', 'ExpenseController@showExpensePage');
	Route::get('/getexpensetable', 'ExpenseController@getExpenseTable');
	Route::get('/getexpense', 'ExpenseController@getExpense');
	Route::get('/lastexpensenumber', 'ExpenseController@getLastExpenseNumber');
	Route::post('/createexpense', 'ExpenseController@createExpense');
	Route::post('/updateexpense', 'ExpenseController@updateExpense');
	Route::post('/deleteexpense', 'ExpenseController@deleteExpense');

	Route::get('/existingreceiveable', 'ExistingReceiveableController@showExistingReceiveablePage');
	Route::get('/getexistingreceiveable', 'ExistingReceiveableController@getExistingReceiveable');
	Route::get('/getcustomerexistingreceiveable','ExistingReceiveableController@getCustomerExistingReceiveable');
	Route::post('/createexistingreceiveable', 'ExistingReceiveableController@createExistingReceiveable');
	Route::post('/deleteexistingreceiveable', 'ExistingReceiveableController@deleteExistingReceiveable');
	Route::get('/downloadexistingreceiveable/{id}', 'ExistingReceiveableController@downloadExistingReceiveable');

	Route::get('/historystockcorrection', 'InventoryController@showHistoryStockCorrection');

	Route::get('/log', 'LogController@showLogPage');

  Route::get('reportpo', 'ReportController@showReportPo');
  Route::get('detailreportpo', 'ReportController@getDetailPo');
  Route::get('downloadreportpo/{start}/{end}', 'ReportController@downloadReportPo');
  Route::get('reportso', 'ReportController@showReportSo');
	Route::get('detailreportso', 'ReportController@getDetailSo');
	Route::get('downloadreportso/{start}/{end}/{customer}', 'ReportController@downloadReportSo');
	Route::get('reportpiutang', 'ReportController@showReportPiutang');
	Route::get('downloadreportpiutang/{customer}', 'ReportController@downloadReportPiutang');
	Route::get('reportpembayaranpiutang', 'ReportController@showReportPembayaranPiutang');
	Route::get('downloadreportpembayaranpiutang/{start}/{end}/{customer}', 'ReportController@downloadReportPembayaranPiutang');
	Route::get('reportexpense', 'ReportController@showReportExpense');
	Route::get('downloadreportexpense/{start}/{end}', 'ReportController@downloadReportExpense');
	Route::get('reportcashbalance','ReportController@showReportCashBalance');
	Route::get('downloadreportcashbalance/{start}/{end}','ReportController@downloadReportCashBalance');
	Route::get('reportprofitloss','ReportController@showReportProfitLoss');
	Route::get('downloadreportprofitloss/{year}/{month}','ReportController@downloadReportProfitLoss');
});

