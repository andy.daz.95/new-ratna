<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detaildeliveryorder extends Model
{
    //
    protected $table = 'delivery_order_details';
    protected $primaryKey='delivery_order_details';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update'; 

    public function product(){
    	return $this->hasOne('App\product','product_id','product_id');
    }
}