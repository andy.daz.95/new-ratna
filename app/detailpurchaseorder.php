<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detailpurchaseorder extends Model
{
	//
	protected $table = 'purchase_order_details';
	protected $primaryKey='purchase_order_details_id';
	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'last_update';

	public function product(){
		return $this->hasOne('App\product','product_id','product_id');
	}
	public function category(){
		return $this->hasOne('App\productcategory','product_category_id','product_category_id')
			->where('status',1);
	}
	public function brand(){
		return $this->hasOne('App\brandbarang','product_brand_id','product_brand_id')
			->where('status',1);
	}
	public function type(){
		return $this->hasOne('App\tipebarang','product_type_id','product_type_id')
			->where('status',1);
	}
}