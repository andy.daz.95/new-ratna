<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class brandbarang extends Model
{
    //
    protected $table = 'product_brand';
    protected $primaryKey='product_brand_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function category(){
    	return $this->belongsTo('App\productcategory','product_category_id','product_category_id');
    }
}