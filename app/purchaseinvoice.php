<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class purchaseinvoice extends Model
{
    //
    protected $table = 'invoice_purchase';
    protected $primaryKey='invoice_purchase_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function payment(){
    	return $this->hasMany('App\purchasepayment','invoice_purchase_id','invoice_purchase_id');
		}
}