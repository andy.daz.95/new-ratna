<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class productcategory extends Model
{
    //
    protected $table = 'product_category';
    protected $primaryKey='product_category_id'; 
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}