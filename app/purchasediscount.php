<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class purchasediscount extends Model
{
    //
    protected $table = 'purchase_discount';
    protected $primaryKey='purchase_discount_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}