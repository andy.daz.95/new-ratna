<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class salesinvoice extends Model
{
    //
    protected $table = 'invoice_sales';
    protected $primaryKey='invoice_sales_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function payment(){
    	return $this->hasOne('App\salespayment','payment_sales_id','payment_sales_id');
		}

		public function product(){
    	return $this->hasOne('App\product','product_id','product_id');
    }

    public function so(){
    	return $this->hasOne('App\salesorder','sales_order_id','sales_order_id');
		}
}