<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class salespayment extends Model
{
    //
    protected $table = 'payment_sales';
    protected $primaryKey='payment_sales_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function so(){
    	return $this->hasOne('App\salesorder','sales_order_id','sales_order_id');
    }
    public function details(){
    	return $this->hasMany('App\detailsalespayment','payment_sales_id','payment_sales_id')
    	->where('status',1);
    }
    public function invoice(){
    	return $this->hasOne('App\salesinvoice','payment_sales_id','payment_sales_id');
		}
}