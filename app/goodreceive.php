<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class goodreceive extends Model
{
    //
    protected $table = 'good_receive';
    protected $primaryKey='good_receive_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function po(){
    	return $this->belongsTo('App\purchaseorder','purchase_order_id','purchase_order_id');
		}
}