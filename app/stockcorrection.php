<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class stockcorrection extends Model
{
    //
    protected $table = 'stock_correction';
    protected $primaryKey='stock_correction_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}