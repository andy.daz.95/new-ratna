<?php

namespace App\Console;

use App\customer;
use App\Traits\CustomerStateTrait;
use App\customerlevel;
use App\customerstate;
use App\salesorder;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your appplication.
     *
     * @var array
     */
    protected $commands = [
        //
				'\App\Console\Commands\SendEmails',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
			$schedule->call(function () {
				//cek state mana yang masih aktif dan next check date nya itu hari ini
				$need_to_check_state = customerstate::where('status',1)
					->whereDate('next_check_date', Carbon::now()->toDateString())
					->get();

				foreach ($need_to_check_state as $key => $value) {
					//cek total so dia di state tersebut
					$so = salesorder::where('status',1)
						->where('customer_state_id',$value->customer_state_id)
						->get();

					$totalso = $so->sum('grand_total_idr');
					$currentlevel = customerlevel::where('customer_level_id',$value->customer_level_id)->first();

					// if it is below maintain limit then it is downgrade
					if($totalso < $currentlevel->min_maintain)
					{
						CustomerStateTrait::createCustomerState($value->customer_id, $currentlevel->customer_level_id -1 == 0 ? 1 : $currentlevel->customer_level_id -1, 'downgrade', Carbon::now()->addYear()->toDateString());
					}
					else{
						CustomerStateTrait::createCustomerState($value->customer_id, $currentlevel->customer_level_id , 'unchanged', Carbon::now()->addYear()->toDateString());
					}

					//soft delete the previous state
					$customerstate = customer::find($value->customer_state_id);
					$customerstate->status = 2;
					$customerstate->save();
				}
			})->daily();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
