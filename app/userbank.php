<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userbank extends Model
{
    //
    protected $table = 'user_bank';
    protected $primaryKey='user_bank_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}