<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detailgoodreceive extends Model
{
    //
    protected $table = 'good_receive_details';
    protected $primaryKey='good_receive_details_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}