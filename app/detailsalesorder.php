<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detailsalesorder extends Model
{
    //
    protected $table = 'sales_order_details';
    protected $primaryKey='sales_order_details_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function product(){
    	return $this->hasOne('App\product','product_id','product_id');
    }
}