<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\customer;
use App\paymentmethod;
use App\existingreceiveabletransaction;
use DB;
use DOMPDF;

class ExistingReceiveableController extends Controller
{
	//
	public function showExistingReceiveablePage()
	{
		$data['customer'] = customer::where('status','<>',2)->get();
		$data['paymentmethod'] = paymentmethod::where('payment_method_id','<>','2')->where('status',1)->get();
		$data['transaction'] = existingreceiveabletransaction::where('status',1)
			->orderBy('existing_receiveable_transaction_id','desc')
			->get();
		return view('existing_receiveable.existing_receiveable', compact('data'));
	}

	public function getExistingReceiveable(Request $request)
	{
		return existingreceiveabletransaction::where('existing_receiveable_transaction_id',$request->id)->first();
	}
	public function getCustomerExistingReceiveable(Request $request)
	{
		return customer::where('customer_id',$request->id)->first();
	}

	public function createExistingReceiveable(Request $request)
	{
		$existingreceiveable = new existingreceiveabletransaction();
		$existingreceiveable->customer_id = $request->customer;
		$existingreceiveable->payment_method_id = $request->paymentmethod;
		$existingreceiveable->transaction_number = $request->nopayment;
		$existingreceiveable->transaction_date = Carbon::createFromFormat('d-m-Y',$request->tgltransaction)->toDateString();
		$existingreceiveable->product_name = $request->product;
		$existingreceiveable->payment = $request->pembayaran;
		$existingreceiveable->receiveable_before_paid = $request->existingreceiveable;
		$existingreceiveable->status = 1;
		$existingreceiveable->save();

		customer::where('customer_id',$request->customer)->update([
			'existing_receiveable' => DB::raw('existing_receiveable-'.$request->pembayaran),
		]);
	}

	public function deleteExistingReceiveable(Request $request)
	{
		$existingreceiveable = existingreceiveabletransaction::find($request->id);

		customer::where('customer_id',$existingreceiveable->customer_id)->update([
			'existing_receiveable' => DB::raw('existing_receiveable+'.$existingreceiveable->payment),
		]);

		$existingreceiveable->status = 2;
		$existingreceiveable->update();
	}

	public function downloadExistingReceiveable(Request $request, $id)
	{
		$transaction = existingreceiveabletransaction::where('existing_receiveable_transaction_id',$id)
			->first();

		$pdf = DOMPDF::loadView('existing_receiveable.transaction_pdf', compact('transaction'))
			->setPaper('A5','landscape');
		return $pdf->stream('Payment Credit.pdf');
	}
}
