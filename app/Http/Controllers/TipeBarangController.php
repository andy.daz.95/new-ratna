<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\brandbarang;
use App\tipebarang;
use DataTable;

class TipeBarangController extends Controller
{
	public function showTipeBarangPage()
	{
	//		$tipe = tipebarang::with('brand')->where('product_type.status','<>',2)->get();
		$brand = brandbarang::where('product_brand.status','<>',2)->get();
		return view('tipe_barang.tipe_barang', compact('tipe','brand'));
	}

	public function getTipeTable(Request $request)
	{
		$tipe = tipebarang::with('brand')->where('product_type.status','<>',2)->get();$tipe = tipebarang::with('brand')->where('product_type.status','<>',2);

		if($request->tipe && $request->tipe != "")
		{
			$tipe = $tipe->where('product_type.name', 'like', '%'.$request->tipe.'%');
		}

		$tipe = $tipe->get();

		return DataTable::of($tipe)
			->setRowAttr([
				'value' => function($tipe){
					return $tipe->product_type_id;
				},
			])
			->addColumn('action', function ($tipe){
				if(Session('roles')->name == 'master') {
					return
						'<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="'.$tipe->product_type_id.'"><i class="material-icons">edit</i></a>
							<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>';
				}
			})
			->smart(false)
			->make(true);
	}

	public function createTipeBarang(Request $request)
	{
		$tipe = new tipebarang;
		$tipe->name = $request->type;
		$tipe->product_brand_id = $request->brand;
		$tipe->status = 1;
		$tipe->save();
	}
	public function createTipeBarangFromPo(Request $request)
	{
		$tipe = new tipebarang;
		$tipe->name = $request->name;
		$tipe->product_brand_id = $request->brand;
		$tipe->status = 1;
		$tipe->save();

		$type = tipebarang::where('product_brand_id',$request->brand)->where('status','<>',2)->get();
		return $type;
	}

	public function getTipeData(Request $request){
		$id = $request->id;
		$tipe = tipebarang::where('product_type_id',$id)->with('brand.category')->select('*')->first();
		return $tipe;
	}

	public function getTipeOnBrand(Request $request){
		$id = $request->id;
		$tipe = tipebarang::where('product_brand_id',$id)->select('*')->get();
		return $tipe;
	}

	public function getKategoriOnBrand(Request $request){
		$id = $request->id;
		$kategori = brandbarang::leftjoin('product_category','product_brand.product_category_id','=','product_category.product_category_id')
			->where('product_brand_id',$id)
			->select('product_category.*')
			->first();
		return $kategori;
	}

	public function modalTipeData(Request $request){
		$id= $request->id;
		$data= tipebarang::where('product_type_id',$id)->first();
		return view('tipe_barang.modal_tipe', compact('data'));
	}

	public function deleteTipeBarang(Request $request)
	{
		$id = $request->id;
		tipebarang::where('product_type_id',$id)->update(['status'=>2]);
	}

	public function updateTipeBarang(Request $request)
	{
		$id = $request->id;
		tipebarang::where('product_type_id', $id)->update(
			[
				'name' => $request->type,
				'product_brand_id' => $request->brand,
			]
		);
	}
}