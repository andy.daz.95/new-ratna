<?php

namespace App\Http\Controllers;

use App\expensecategory;
use App\paymentmethod;
use Illuminate\Http\Request;
use App\expense;
use DataTable;

class ExpenseController extends Controller
{
	public function showExpensePage(){
		$data['expense'] = expense::with('expense_category')->where('status','<>',2)->get();
		$data['expensecategory'] = expensecategory::where('status',1)->get();
		$data['paymentmethod'] = paymentmethod::where('payment_method_id','<>','2')->get();

		return view('expense.expense', compact('data'));
	}

	public function getExpenseTable(Request $request){
		$expense = expense::
		leftjoin('expense_category','expense_category.expense_category_id','=','expense.expense_category_id')
			->selectRaw('expense.*,expense_category.expense_description')
			->orderby('expense_category_id','desc');

		if ($request->category && $request->category != "")
		{
			$expense = $expense->where('expense_category.expense_description','like','%'.$request->category.'%');
		}
		if($request->user && $request->user != "")
		{
			$expense = $expense->where('expense.pengguna', 'like', '%'.$request->user.'%');
		}
		$expense = $expense->get();

		return DataTable::of($expense)
			->setRowAttr([
				'value' => function($expense){
					return $expense->expense_id;
				},
			])
			->addColumn('action', function ($expense){
					if(Session('roles')->name == 'master') {
						return
						'<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="'.$expense->expense_id.'"><i class="material-icons">edit</i></a>
							<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>';
					}
			})
			->editColumn('nominal', function ($expense) {
				return number_format($expense->nominal, 0);
			})
			->smart(false)
			->make(true);
	}

	public function getLastExpenseNumber()
	{
		$lastexpensetoday = expense::where('expense_number','like','EXP/'.date('dmy').'/%')->orderby('expense_id','desc')->first();
		if(empty($lastexpensetoday))
		{
			$newexpensenumber = "EXP/".date('dmy')."/1";
			return $newexpensenumber;
		}
		else{
			$tmpexpense = explode('/',$lastexpensetoday->expense_number);
			$lastnumber = $tmpexpense[2];
			$newexpensenumber = "EXP/".date('dmy')."/".($lastnumber+1);
			return $newexpensenumber;
		}
	}

	public function getExpense(Request $request)
	{
		$expense = expense::where('expense_id',$request->id)->first();
		return $expense;
	}

	public function createExpense(Request $request)
	{
		$expense = new expense();
		$expense->expense_category_id = $request->expensecategory;
		$expense->payment_method_id = $request->paymentmethod;
		$expense->expense_number = $request->noexpense;
		$expense->date_expense = date('Y-m-d', strtotime($request->tglexpense));
		$expense->pengguna = $request->pengguna;
		$expense->description = $request->description;
		$expense->nominal = $request->nominal;
		$expense->status = 1;
		$expense->save();
	}

	public function updateExpense(Request $request)
	{
		$expense = expense::find($request->idexpense);
		$expense->expense_category_id = $request->expensecategory;
		$expense->payment_method_id = $request->paymentmethod;
		$expense->date_expense = date('Y-m-d', strtotime($request->tglexpense));
		$expense->description = $request->description;
		$expense->nominal = $request->nominal;
		$expense->pengguna = $request->pengguna;
		$expense->save();
	}

	public function deleteExpense(Request $request)
	{
		$expense = expense::find($request->id);
		$expense->status = 2;
		$expense->save();
	}
}
