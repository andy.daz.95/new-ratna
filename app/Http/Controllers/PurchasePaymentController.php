<?php

namespace App\Http\Controllers;

use App\paymentmethod;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\purchasepayment;
use App\detailpurchasepayment;
use App\purchaseorder;
use App\detailpurchaseorder;
use App\purchaseinvoice;
use App\detailpurchaseinvoice;
use App\log;
use App\bank;
use App\supplier;
use App;
use DB;
use PDF;
use DataTable;
use DOMPDF;


class PurchasePaymentController extends Controller
{
	public function showPurchasePaymentPage(){
		$data['payment'] = purchasepayment::leftjoin('invoice_purchase','invoice_purchase.invoice_purchase_id','=','payment_purchase.invoice_purchase_id')
			->leftjoin('purchase_order','purchase_order.purchase_order_id','=','invoice_purchase.purchase_order_id')
			->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
			->leftjoin('payment_method','payment_purchase.payment_method_id','=','payment_method.payment_method_id')
			->leftjoin('bank','bank.bank_id','=','payment_method.bank_id')
			->leftjoin('payment_method_type','payment_method_type.payment_method_type_id','=','payment_method.payment_method_type_id')
			->where('payment_purchase.status','<>',2)
			->select('payment_purchase.*', 'supplier.company_name','bank.bank_name','payment_method_type.description')
			->orderBy('payment_purchase_id','desc')
			->get();

		$data['invoice'] = purchaseinvoice::leftjoin('purchase_order','purchase_order.purchase_order_id','=','invoice_purchase.purchase_order_id')
			->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
			->select('invoice_purchase.invoice_purchase_id', 'invoice_purchase.invoice_purchase_number', 'supplier.company_name')
			->where('invoice_purchase.status','<>',2)
			->where(function ($q) {
				$q->whereExists(function ($query) {
					$query->select(DB::raw(1))
						->from('payment_purchase')
						->whereRaw('payment_purchase.invoice_purchase_id = invoice_purchase.invoice_purchase_id')
						->havingRaw('invoice_purchase.total_amount > SUM(payment_purchase.total_paid + payment_purchase.deposit_reduction)')
						->where('payment_purchase.status',1)
						->groupby('invoice_purchase_id');
				})->orwhereNotExists(function ($query) {
					$query->select(DB::raw(1))
						->from('payment_purchase')
						->whereRaw('payment_purchase.invoice_purchase_id = invoice_purchase.invoice_purchase_id')
						->where('payment_purchase.status',1);
				});
			})
			->get();

		$data['paymentmethod'] = paymentmethod::where('payment_method_id','<>','2')
			->with('bank')
			->with('payment_method_type')
			->where('status',1)
			->get();

		return view('purchase_payment.purchase_payment', compact('data'));
	}

	public function getPurchasePaymentTable(Request $request)
	{
		$payment = purchasepayment::leftjoin('invoice_purchase','invoice_purchase.invoice_purchase_id','=','payment_purchase.invoice_purchase_id')
			->leftjoin('purchase_order','purchase_order.purchase_order_id','=','invoice_purchase.purchase_order_id')
			->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
			->leftjoin('payment_method','payment_purchase.payment_method_id','=','payment_method.payment_method_id')
			->leftjoin('bank','bank.bank_id','=','payment_method.bank_id')
			->leftjoin('payment_method_type','payment_method_type.payment_method_type_id','=','payment_method.payment_method_type_id')
			->where('payment_purchase.status','<>',2)
			->select('payment_purchase.*', 'supplier.company_name','bank.bank_name','payment_method_type.description')
			->orderBy('payment_purchase_id','desc');

		if($request->number && $request->number != "")
		{
			$payment = $payment->where('payment_purchase.payment_purchase_number', 'like', '%'.$request->number.'%');
		}
		if($request->supplier && $request->supplier != "")
		{
			$payment = $payment->where('supplier.company_name', 'like', '%'.$request->supplier.'%');
		}
		$payment = $payment->get();

		return DataTable::of($payment)
			->setRowAttr([
				'value' => function($payment){
					return $payment->payment_purchase_id;
				},
			])
			->addColumn('action', function ($payment){
				if(Session('roles')->name == 'master') {
					return
						'<a class="btn btn-sm btn-raised red delete-modal tooltipped" data-position="bottom" data-tooltip="delete"><i class="material-icons">delete</i></a>';
				}
			})
			->editColumn('total_paid', function ($payment) {
				return number_format($payment->total_paid + $payment->deposit_reduction, 0);
			})
			->addColumn('payment_method', function ($payment) {
				return "{$payment->bank_name} {$payment->description}";
			})
			->smart(false)
			->make(true);
	}

	public function getLastPaymentNumber(Request $request)
	{
		$lastpaymenttoday = purchasepayment::where('payment_purchase_number','like','PP/'.date('dmy').'/%')->orderby('payment_purchase_id','desc')->first();
		if(empty($lastpaymenttoday))
		{
			$newpaymentnumber = "PP/".date('dmy')."/1";
			return $newpaymentnumber;
		}
		else{
			$tmppayment = explode('/',$lastpaymenttoday->payment_purchase_number);
			$lastnumber = $tmppayment[2];
			$newpaymentnumber = "PP/".date('dmy')."/".($lastnumber+1);
			return $newpaymentnumber;
		}
	}

	public function getPurchasePayment(Request $request)
	{
		$payment = purchasepayment::leftjoin('invoice_purchase','invoice_purchase.invoice_purchase_id','=','payment_purchase.invoice_purchase_id')
			->leftjoin('purchase_order','purchase_order.purchase_order_id','=','invoice_purchase.purchase_order_id')
			->leftjoin('supplier','supplier.supplier_id','purchase_order.supplier_id')
			->leftjoin('payment_type','purchase_order.payment_type_id','=','payment_type.payment_type_id')
			->leftjoin('invoice_purchase_details','invoice_purchase_details.invoice_purchase_id','=','invoice_purchase.invoice_purchase_id')
			->select('payment_purchase.*','invoice_purchase.invoice_purchase_id','invoice_purchase.invoice_purchase_number','supplier.company_name', 'payment_type.*','purchase_order.*')
			->where('payment_purchase.payment_purchase_id', $request->id)
			->first();

		$podetail = detailpurchaseorder::join('product_category','product_category.product_category_id','purchase_order_details.product_category_id')
			->join('product_brand','product_brand.product_brand_id','purchase_order_details.product_brand_id')
			->join('product_type','product_type.product_type_id','purchase_order_details.product_type_id')
			->where('purchase_order_id',$payment->purchase_order_id)
			->select('purchase_order_details.*','product_category.description as c_name','product_brand.name as b_name','product_type.name as t_name')
			->get();

		return compact('payment','podetail');
	}

	public function getInvoiceData(Request $request)
	{
		if($request->id == 0)
		{
			return;
		}

		$invoice = purchaseinvoice::leftjoin('purchase_order','purchase_order.purchase_order_id','=','invoice_purchase.purchase_order_id')
			->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
			->leftjoin('payment_type','purchase_order.payment_type_id','=','payment_type.payment_type_id')
			->select('purchase_order.*','invoice_purchase.*','supplier.company_name','supplier.deposit','payment_type.*')
			->where('invoice_purchase.invoice_purchase_id', $request->id)
			->first();

		$podetail = detailpurchaseorder::join('product_category','product_category.product_category_id','purchase_order_details.product_category_id')
			->join('product_brand','product_brand.product_brand_id','purchase_order_details.product_brand_id')
			->join('product_type','product_type.product_type_id','purchase_order_details.product_type_id')
			->where('purchase_order_id',$invoice->purchase_order_id)
			->select('purchase_order_details.*','product_category.description as c_name','product_brand.name as b_name','product_type.name as t_name')
			->get();

		$paymentdata = purchasepayment::where('invoice_purchase_id',$invoice->invoice_purchase_id)
			->where('status',1)
			->get();

		$payment = $paymentdata->sum('total_paid') + $paymentdata->sum('deposit_reduction');

		return compact('invoice','podetail','payment');
	}

	public function createPayment(Request $request)
	{
		$purchasepayment = new purchasepayment;
		$purchasepayment->invoice_purchase_id = $request->noinvoice;
		$purchasepayment->payment_purchase_number = $request->nopayment;
		$purchasepayment->total_paid = $request->pembayaran;
		$purchasepayment->payment_method_id = $request->paymentmethod;
		$purchasepayment->discount_nominal = $request->diskon;
		$purchasepayment->deposit_reduction = $request->deposito;
		$purchasepayment->date_payment_purchase = Carbon::now()->format('Y-m-d');
		$purchasepayment->status = 1;
		$purchasepayment->save();

		//kalau ada deposito nya berarti dikurangin kolom deposit di supplier
		if($request->deposito > 0)
		{
			$supplier = purchaseinvoice::join('purchase_order','purchase_order.purchase_order_id','=','invoice_purchase.purchase_order_id')
				->where('invoice_purchase.invoice_purchase_id', $request->noinvoice)
				->first()
				->supplier_id;

			supplier::where('supplier_id',$supplier)->update([
				'deposit' => DB::raw('deposit-'.$request->deposito),
			]);
		}

		$log = new log;
		$log->actor = "andy.daz";
		$log->activity = "menambahkan";
		$log->object = "Sales Payment";
		$log->object_details = $purchasepayment->payment_purchase_number;
		$log->status = 1;
		$log->save();
	}

	public function deletePayment(Request $request)
	{
		purchasepayment::where('payment_purchase.payment_purchase_id', $request->id)
			->update(["status"=>2]);
		detailpurchasepayment::where('payment_purchase_details.payment_purchase_id', $request->id)
			->update(["status"=>2]);
	}


	public function downloadPurchasePayment(Request $request, $id)
	{
		$paymentheader = purchasepayment::leftjoin('payment_purchase_details', 'payment_purchase_details.payment_purchase_id','=','payment_purchase.payment_purchase_id')
			->leftjoin('invoice_purchase','payment_purchase.invoice_purchase_id','=','invoice_purchase.invoice_purchase_id')
			->join('purchase_order','invoice_purchase.purchase_order_id','=','purchase_order.purchase_order_id')
			->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
			->where('payment_purchase.payment_purchase_id',$id)
			->first();

		$invoice = $paymentheader->invoice_purchase_id;
		$totalinvoice = $paymentheader->total_amount;
		$totalpayment = purchasepayment::where('invoice_purchase_id',$invoice)->sum('total_paid');
		$remaining = $totalinvoice - $totalpayment;

		// return "totalpayment = ".$totalpayment."<> totalinvoice = ".$totalinvoice." <> totaldiskon = ".$totaldiskon;

		$pdf = DOMPDF::loadView('purchase_payment.purchase_payment_pdf', compact('paymentheader','totalinvoice','totalpayment','totaldiskon','remaining'));
		return $pdf->stream('Payment Purchase.pdf');
	}

}
