<?php

namespace App\Http\Controllers;

use App\tipebarang;
use Illuminate\Http\Request;
use App\productcategory;
use App\satuan;
use App\warehouse;
use App\product;
use App\pricepurchase;
use App\productwarehouse;

class BarangController extends Controller
{
    public function showBarangPage()
    {
    	$gudang = warehouse::all();
    	$productcategory = productcategory::all();
    	$satuan = satuan::all();
    	$product = product::leftjoin('product_warehouse','product.product_id','=','product_warehouse.product_id')->leftjoin('price_purchase','price_purchase.product_id','=','product.product_id')->selectRaw('product.*, Sum(quantity)as t_qty, price_purchase.hpp')->groupby('product.product_id')->get();
    	return view('barang.barang', compact('productcategory','satuan','product','gudang'));
    }

    public function createBarang(Request $request)
    {
    	$product = new product;
    	$product->unit_id = $request->satuan;
    	$product->product_code = $request->kode;
        $product->product_name = $request->nama;
    	$product->total_quantity = 0;
    	$product->weight = $request->weight? $request->weight : 0;
    	$product->diameter = $request->diameter ? $request->diameter : 0;
    	$product->price_sale = $request->hargajual? $request->hargajual : 0;
    	$product->status = 1;
    	$product->save();
        
        $qtygudang  = array_map(function($val){ if($val == null){return 0;}else{return $val;}},$request->qtygudang);
        for ($i=0; $i < sizeof($request->idgudang); $i++) { 
            $productwarehouse = new productwarehouse;
            $productwarehouse->product_id = $product->product_id;
            $productwarehouse->warehouse_id = $request->idgudang[$i];
            $productwarehouse->quantity = $qtygudang[$i];
            $productwarehouse->save();
        }

        $pricepurchase = new pricepurchase;
        $pricepurchase->product_id = $product->product_id;
        $pricepurchase->last_quantity = 0;
        $pricepurchase->balance = 0;
        $pricepurchase->hpp = 0;
        $pricepurchase->status = 1;
        $pricepurchase->save();

    }

    public function filterBarang(Request $request){
    	$filter = $request->filter;
    	$product = product::where('company_name','like','%'.$filter.'%')->paginate(10);

    	return view('product.product_table', compact('product'));
    }

    public function getBarangData(Request $request){
        $id = $request->id;    
        $product = product::where('product_id',$id)->select('*')->first();
        $product_warehouse = productwarehouse::where('product_id',$id)->get();

        return compact('product', 'product_warehouse');
    }

	public function getBarangInfo(Request $request){
		$id = $request->id;
		$product = product::with('category')
							->with('brand')
							->with('type')
							->where('product_id',$id)
							->first();

		$brand = $product->brand->product_brand_id;

		$type = tipebarang::where('product_brand_id',$brand)->get();

		return compact('product','type');
	}


    public function getBarangUsingBarcode(Request $request){
    	$barcode = $request->barcode;    
    	$product = product::with('category')
                    ->with('brand')
                    ->with('type')
                    ->where('bar_code',$barcode)
                    ->whereIn('status',[1,3,5])
                    ->first();
    	return $product;
    }

    public function modalBarangData(Request $request){
        $id= $request->id;
        $data= product::where('product_id',$id)->first();
        return view('barang.modal_barang', compact('data'));
    }

    public function deleteBarang(Request $request)
    {
        $id = $request->id;
        product::where('product_id',$id)->delete();
    }

    public function updateBarang(Request $request)
    {
        $id = $request->id;
        product::where('product_id', $id)->update(
            [
            'unit_id' => $request->satuan,
            'supplier_id' => 6,
            'product_code' => $request->kode,
            'product_name' => $request->nama,
            'total_quantity' => $request->qty,
            'weight' => $request->weight,
            'diameter' => $request->diameter,
            'price_sale' => $request->hargajual,
            ]
        );
    }
}