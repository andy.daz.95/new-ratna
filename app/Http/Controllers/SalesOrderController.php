<?php
namespace App\Http\Controllers;

use App\customerlevel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\SalesInvoiceController;
use App\Http\Controllers\SalesPaymentController;
use App\Http\Controllers\DeliveryOrderController;
use App\account;
use App\customer;
use App\customerstate;
use App\paymenttype;
use App\paymentmethod;
use App\salesorder;
use App\salesinvoice;
use App\salespayment;
use App\detailsalesorder;
use App\detailsalespayment;
use App\deliveryorder;
use App\detaildeliveryorder;
use App\product;
use App\log;
use App;
use Indonesia;
use PDF;
use DB;
use DOMPDF;
use DataTable;
use App\Traits\NumberToLetterTrait;

class SalesOrderController extends CustomerStateController
{
	use NumberToLetterTrait;
	public function showSalesOrderPage()
	{
		$customer = customer::where('status', '<>', 2)->get();
		$staff = account::has('staff')->get();
		$payment_term = paymenttype::all();
		$payment_method = paymentmethod::with('payment_method_type')->with('bank')->where('status', '<>', 2)->get();
		// return $payment_method;
//		$so = salesorder::with('customer')
//			->with('staff')
//			->with('payment_type')
//			->where('sales_order.status', '<>', 2)
//			->select('sales_order.*')
//			->orderBy('sales_order_id','desc')
//			->get();

		return view('so.sales_order', compact('so', 'customer', 'payment_term', 'payment_method', 'staff'));
	}

	public function getSalesOrderTable(Request $request)
	{
		$so = $so = salesorder::with('customer')
			->with('staff')
			->with('payment_type')
			->where('sales_order.status', '<>', 2)
			->select('sales_order.*')
			->orderBy('sales_order_id','desc');

		if($request->number && $request->number != "")
		{
			$so = $so->where('sales_order.sales_order_number', 'like', '%'.$request->number.'%');
		}
		if($request->pelanggan && $request->pelanggan != "")
		{
			$so = $so->whereHas('customer', function($query) use ($request){
				$query->where('customer.first_name', 'like', '%'.$request->pelanggan.'%')
				->orWhere('customer.last_name', 'like', '%'.$request->pelanggan.'%');
			});
		}

		$so = $so->get();

		return DataTable::of($so)
			->setRowAttr([
				'value' => function($so){
					return $so->sales_order_id;
				},
			])
			->addColumn('action', function ($so){
				$editbutton = '<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="'.$so->sales_order_id.'"><i class="material-icons">edit</i></a>';
				$deletebutton = '<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>';
				$draftbutton = '<a class="btn btn-sm btn-raised draft" mode="'. (Session('roles')->name == 'master' ? 'draft' : '' ).'" value="'.$so->sales_order_id.'"><i class="material-icons">search</i>
                      </a>';
				if(
					salesorder::where('sales_order_id',$so->sales_order_id)->whereHas('details_delivery', function ($query) {
						$query->where('is_retur', 1);
					})->count() > 0
				)
				{
					return '<a class="btn btn-sm btn-raised green"><i class="material-icons">keyboard_return</i></a>';
				}else{
					if(Session('roles')->name == 'master' && $so->is_draft == 1) {
						return
							$editbutton.$deletebutton.$draftbutton;
					}else if ($so->is_draft == 1){
						return
						$draftbutton;
					}else if (Session('roles')->name == 'master' ){
						return
							$editbutton.$deletebutton;
					}
				}
			})
			->smart(false)
			->make(true);
	}

	public function getLastSalesNumber(Request $request)
	{
		$lastsalestoday = salesorder::where('sales_order_number', 'like', 'SO/' . date('dmy') . '/%')->orderby('sales_order_id', 'desc')->first();
		if (empty($lastsalestoday)) {
			$newsonumber = "SO/" . date('dmy') . "/1";
			return $newsonumber;
		} else {
			$tmpsales = explode('/', $lastsalestoday->sales_order_number);
			$lastnumber = $tmpsales[2];
			$newsonumber = "SO/" . date('dmy') . "/" . ($lastnumber + 1);
			return $newsonumber;
		}
	}

	public function getSalesOrder(Request $request)
	{
		$so = salesorder::with('details.product.category')
			->with('details.product.brand')
			->with('details.product.type')
			->with('first_payment.details')
			->where('sales_order.sales_order_id', $request->id)
			->where('sales_order.status', '<>', 2)
			->first();

		return $so;
	}

	public function createSalesOrder(Request $request)
	{
		$total = array_sum($request->subtotal);
		$tax = 0;

		$salesorder = new salesorder;
		$salesorder->customer_id = $request->pelanggan;
		$salesorder->payment_type_id = $request->pembayaran;
		$salesorder->sales_id = $request->sales;

		//fungsi dari customer state adalah mencatata transaksi ini di state terakhir (level pelanggan),
		//sehingga nanti apabila ada yang di cancel lebih mudah di track dan lebih mudah untuk di turunkan level pelangganya
		if($request->pelanggan != 1)
		{
			$customer_state = customerstate::where('customer_id',$request->pelanggan)
				->orderBy('customer_state_id','desc')
				->first()
				->customer_state_id;
			$salesorder->customer_state_id = $customer_state;
		}

		$salesorder->shipping_term_id = 1;
		$salesorder->customer_name = $request->customername;
		$salesorder->phone_number = $request->customerphone;
		$salesorder->sales_order_number = $request->nosalesorder;
		$salesorder->date_sales_order = date('Y-m-d', strtotime($request->tglso));
		$salesorder->date_delivery_order = date('Y-m-d', strtotime('+7 day', strtotime($request->tglso)));
		$salesorder->term_payment = $request->terms;
		$salesorder->sub_total = $request->total;
		$salesorder->tax = $tax;
		$salesorder->note = $request->notes;
		$salesorder->grand_total_idr = $request->total;
		$salesorder->is_draft = 0;
		$salesorder->status = 1;
		$salesorder->save();

		//naikkan level pelanggan kalau dia sudah mencapai limit naik level
		if(isset($customer_state)){
			//cek semua total belanja dia di state saat ini.
			$so = $salesorder::where('customer_state_id',$customer_state)
				->where('status',1)
				->get();

			$total_this_state = $so->sum('grand_total_idr');

			$customerlevel = customerstate::where('customer_state_id',$customer_state)
				->with('level')
				->first();

			$level_up_limit = $customerlevel->level->level_up_limit;

			if($total_this_state > $level_up_limit)
			{
				$current_level = $customerlevel->level->customer_level_id;
				$current_date = customerstate::where('customer_id',$request->pelanggan)
					->orderBy('customer_state_id','desc')
					->first()
					->next_check_date;

				$newcheckdate = Carbon::createFromFormat('Y-m-d',$current_date)->addYear()->toDateString();
				$new_level = $current_level + 1;
				$this->createCustomerState($request->pelanggan, $new_level, 'upgrade', $newcheckdate);
			}
		}

		for ($i = 0; $i < sizeof($request->idbarang); $i++) {
			$detailso = new detailsalesorder;
			$detailso->sales_order_id = $salesorder->sales_order_id;
			$detailso->product_id = $request->idbarang[$i];
			$detailso->price = $request->hargajual[$i];
			$detailso->discount_percentage = $request->percentfirst[$i];
			$detailso->discount_percentage_2 = $request->percentsecond[$i];
			$detailso->discount_nominal = $request->diskon[$i];
			$detailso->sub_total = $request->subtotal[$i];
			$detailso->status = 1;
			$detailso->save();

			product::where('product_id', $request->idbarang[$i])
				->update([
					'status' => 2,
					'selled_date' => Carbon::now()->format('Y-m-d')
				]);
		}

		$this->createlog('menambahkan', 'Sales Order', $salesorder->sales_order_number);

		if ($request->pembayaran == 1 || $request->pembayaran == 3) // Langsung && Online
		{
			// buat invoice beserta payment
			$date = date('Y-m-d', strtotime($request->tglso));

			$salesinvoice = $this->createinvoice($salesorder->sales_order_id, $date, $request->total);
			$this->createlog('menambahkan', 'Sales Invoice', $salesinvoice->invoice_sales_number);

			$salespayment = $this->createpayment($salesorder->sales_order_id, $date, $total, true, 0, $request->addcharge, $request->addnote);

			// masukkan metode pembayaran dalam paymnet details
			for ($i = 0; $i < sizeof($request->metode); $i++) {

				$this->createdetailpayment($salespayment->payment_sales_id, $request->metode[$i], $request->bayar[$i]);

				// Kurangin Balance Customer kalau menggunakan metode saldo
				if ($request->metode[$i] == 2) {
					customer::where('customer_id', $request->pelanggan)->update([
						'total_balance' => DB::raw('total_balance-' . $request->bayar[$i]),
					]);
				}
			}

			// update invoice yang telah dibayar dengan memasukkan id payment
			salesinvoice::where('invoice_sales_id', $salesinvoice->invoice_sales_id)->update([
				'payment_sales_id' => $salespayment->payment_sales_id,
			]);

			$this->createlog('menambahkan', 'Sales Payment', $salespayment->payment_sales_number);

		} else if ($request->pembayaran == 2 || $request->pembayaran == 4) { //cicilan & Booking

			$term = $request->terms;

			//membuat invoice untuk setiap barang, dan di buat berdasarkan term yang dipilih.
			$first_term_invoice = [];
			for ($i = 0; $i < sizeof($request->idbarang); $i++) {
				for ($j = 1; $j <= $term; $j++) {

					$date = date('Y-m-d', strtotime($request->tglso));
					$salesinvoice = $this->createinvoice($salesorder->sales_order_id, $date, $request->cicilan[$i],$request->idbarang[$i], $j);

					$this->createlog('menambahkan', 'Sales Invoice', $salesinvoice->invoice_sales_number);

					// get invoice dengan term pertama
					if ($j == 1) {
						array_push($first_term_invoice, $salesinvoice->invoice_sales_id);
					}
				}
			}

			if ($request->pembayaran != 4) {
				$totalpayment = array_sum($request->bayar);
				$installment = $request->installment;
				$remaining = $totalpayment - $installment;

				$date = date('Y-m-d', strtotime($request->tglso));
				$salespayment = $this->createpayment($salesorder->sales_order_id, $date, $totalpayment,true, $remaining, $request->addcharge, $request->addnote);

				// masukkan metode pembayaran dalam paymnet details
				for ($i = 0; $i < sizeof($request->metode); $i++) {
					$this->createdetailpayment($salespayment->payment_sales_id, $request->metode[$i],$request->bayar[$i]);

					if ($request->metode[$i] == 2) {
						customer::where('customer_id', $request->pelanggan)->update([
							'total_balance' => DB::raw('total_balance-' . $request->bayar[$i]),
						]);
					}
				}

				if ($remaining > 0) {
					customer::where('customer_id', $request->pelanggan)->update([
						'total_balance' => DB::raw('total_balance+' . $remaining),
					]);
				}

				salesinvoice::whereIn('invoice_sales_id', $first_term_invoice)->update([
					'payment_sales_id' => $salespayment->payment_sales_id,
				]);
			}
		}

		// Kalau Pembayarnya Langsung / Cicil, langsung dibuatkan delivery order secara otomatis
		if($request->pembayaran == 1 || $request->pembayaran == 2){
			$lastdelivery = (new DeliveryOrderController)->getLastDeliveryNumber(); // Generate No Do Terakhir

			$date= date('Y-m-d',strtotime($request->tglso));
			$deliveryorder = $this->createdeliveryorder($salesorder, $date);

			$detailso = detailsalesorder::where('sales_order_id',$salesorder->sales_order_id)
				->where('status',1)
				->get();

			foreach ($detailso as $key => $value)
			{
				$this->createdetaildelivery($deliveryorder->delivery_order_id, $value->sales_order_details_id, $value->product_id);
			}
		}

		// return sales order id untuk keperluan print
		return $salesorder->sales_order_id;
	}

	public function updateSalesOrder(Request $request)
	{
		$salesorder = salesorder::find($request->idso);
		$lasttotal = $salesorder->grand_total_idr;

		$salesorder->date_sales_order = date('Y-m-d', strtotime($request->tglso));
		$salesorder->grand_total_idr = $request->total;
		$salesorder->note = $request->notes;
		$salesorder->save();

		//hapus detailso sebelumnya , kembalikan barang jadi available , dan buat baru
		$detailso = detailsalesorder::where('sales_order_id', $request->idso)
			->update(['status' => 2]);

//		foreach ($detailso->get() as $key => $value){
//			product::where('product_id', $value->product_id);
//		}

		for ($i = 0; $i < sizeof($request->idbarang); $i++) {
			$detailso = new detailsalesorder;
			$detailso->sales_order_id = $salesorder->sales_order_id;
			$detailso->product_id = $request->idbarang[$i];
			$detailso->price = $request->hargajual[$i];
			$detailso->discount_percentage = $request->percentfirst[$i];
			$detailso->discount_percentage_2 = $request->percentsecond[$i];
			$detailso->discount_nominal = $request->diskon[$i];
			$detailso->sub_total = $request->subtotal[$i];
			$detailso->status = 1;
			$detailso->save();

			product::where('product_id', $request->idbarang[$i])->update(['status' => 2]);
		}

		$payment = salespayment::where('sales_order_id',$salesorder->sales_order_id)
			->where('status',1)
			->get();

		// hapus invoice dan payment yang dibuat sebelumnya
		$this->deletepayment($payment,$salesorder);

		if ($request->pembayaran == 1 || $request->pembayaran == 3) // Langsung && Online
		{
			// buat invoice beserta payment
			$date = date('Y-m-d', strtotime($request->tglso));

			$salesinvoice = $this->createinvoice($salesorder->sales_order_id, $date, $request->total);
			$this->createlog('menambahkan', 'Sales Invoice', $salesinvoice->invoice_sales_number);

			$salespayment = $this->createpayment($salesorder->sales_order_id, $date, $request->total, true, 0, $request->addcharge, $request->addnote);

			// masukkan metode pembayaran dalam paymnet details
			for ($i = 0; $i < sizeof($request->metode); $i++) {

				$this->createdetailpayment($salespayment->payment_sales_id, $request->metode[$i], $request->bayar[$i]);

				// Kurangin Balance Customer kalau menggunakan metode saldo
				if ($request->metode[$i] == 2) {
					customer::where('customer_id', $request->pelanggan)->update([
						'total_balance' => DB::raw('total_balance-' . $request->bayar[$i]),
					]);
				}
			}

			// update invoice yang telah dibayar dengan memasukkan id payment
			salesinvoice::where('invoice_sales_id', $salesinvoice->invoice_sales_id)->update([
				'payment_sales_id' => $salespayment->payment_sales_id,
			]);

			$this->createlog('menambahkan', 'Sales Payment', $salespayment->payment_sales_number);

		} else if ($request->pembayaran == 2 || $request->pembayaran == 4) { //cicilan & Booking

			$term = $request->terms;

			//membuat invoice untuk setiap barang, dan di buat berdasarkan term yang dipilih.
			$first_term_invoice = [];
			for ($i = 0; $i < sizeof($request->idbarang); $i++) {
				for ($j = 1; $j <= $term; $j++) {

					$date = date('Y-m-d', strtotime($request->tglso));
					$salesinvoice = $this->createinvoice($salesorder->sales_order_id, $date, $request->cicilan[$i],$request->idbarang[$i], $j);

					$this->createlog('menambahkan', 'Sales Invoice', $salesinvoice->invoice_sales_number);

					// get invoice dengan term pertama
					if ($j == 1) {
						array_push($first_term_invoice, $salesinvoice->invoice_sales_id);
					}
				}
			}

			if($request->pembayaran != 4) {
				$totalpayment = array_sum($request->bayar);
				$installment = $request->installment;
				$remaining = $totalpayment - $installment;

				$date = date('Y-m-d', strtotime($request->tglso));
				$salespayment = $this->createpayment($salesorder->sales_order_id, $date, $totalpayment,true, $remaining,$request->addcharge, $request->addnote);

				// masukkan metode pembayaran dalam paymnet details
				for ($i = 0; $i < sizeof($request->metode); $i++) {
					$this->createdetailpayment($salespayment->payment_sales_id, $request->metode[$i],$request->bayar[$i]);

					if ($request->metode[$i] == 2) {
						customer::where('customer_id', $request->pelanggan)->update([
							'total_balance' => DB::raw('total_balance-' . $request->bayar[$i]),
						]);
					}
				}

				if ($remaining > 0) {
					customer::where('customer_id', $request->pelanggan)->update([
						'total_balance' => DB::raw('total_balance+' . $remaining),
					]);
				}

				salesinvoice::whereIn('invoice_sales_id', $first_term_invoice)->update([
					'payment_sales_id' => $salespayment->payment_sales_id,
				]);
			}
		}
	}

	public function deleteSalesOrder(Request $request)
	{
		$salesorder = salesorder::where('sales_order_id', $request->id)->first();

		//hapus detailso sebelumnya , kembalikan barang jadi available , dan buat baru
		$detailso = detailsalesorder::where('sales_order_id', $request->id);

		foreach ($detailso->get() as $key => $value){
			product::where('product_id', $value->product_id)->update(['status' => 1]);
		}

		$payment = salespayment::where('sales_order_id',$salesorder->sales_order_id)
			->where('status',1)
			->get();

		$this->deletepayment($payment, $salesorder);

		//hapus delivery order
		$this->deletedeliveryorder($salesorder);

		$salesorder->status = 2;
		$salesorder->save();

		$log = new log;
		$log->actor = "andy.daz";
		$log->activity = "menghapus";
		$log->object = "Sales Order";
		$log->object_details = $salesorder->sales_order_number;
		$log->status = 1;
		$log->save();
	}

	public function createDraftSalesOrder(Request $request)
	{
		$total = array_sum($request->subtotal);
		// $tax= 0.01 * $total;
		$tax = 0;
		// $grandtotal = $total + $tax;

		$salesorder = new salesorder;
		$salesorder->customer_id = $request->pelanggan;
		$salesorder->payment_type_id = $request->pembayaran;
		$salesorder->sales_id = $request->sales;
		$salesorder->shipping_term_id = 1;
		$salesorder->sales_order_number = $request->nosalesorder;
		$salesorder->customer_name = $request->customername;
		$salesorder->phone_number = $request->customerphone;
		$salesorder->date_sales_order = date('Y-m-d', strtotime($request->tglso));
		$salesorder->date_delivery_order = date('Y-m-d', strtotime('+7 day', strtotime($request->tglso)));
		$salesorder->term_payment = $request->terms;
		$salesorder->sub_total = $request->total;
		$salesorder->tax = $tax;
		$salesorder->note = $request->notes;
		$salesorder->grand_total_idr = $request->total;
		$salesorder->is_draft = 1;
		$salesorder->status = 1;
		$salesorder->save();

		for ($i = 0; $i < sizeof($request->idbarang); $i++) {
			$detailso = new detailsalesorder;
			$detailso->sales_order_id = $salesorder->sales_order_id;
			$detailso->product_id = $request->idbarang[$i];
			$detailso->price = $request->hargajual[$i];
			$detailso->discount_percentage = $request->percentfirst[$i];
			$detailso->discount_percentage_2 = $request->percentsecond[$i];
			$detailso->discount_nominal = $request->diskon[$i];
			$detailso->sub_total = $request->subtotal[$i];
			$detailso->status = 1;
			$detailso->save();

			product::where('product_id', $request->idbarang[$i])->update(['status' => 5]);
		}

		$log = new log;
		$log->actor = "andy.daz";
		$log->activity = "menambahkan";
		$log->object = "Draft Sales Order";
		$log->object_details = $salesorder->sales_order_number;
		$log->status = 1;
		$log->save();

		$term = $request->terms;
		$first_term_invoice = [];
		for ($i = 0; $i < sizeof($request->idbarang); $i++) {
			for ($j = 1; $j <= $term; $j++) {
				$lastinvoice = (new SalesInvoiceController)->getLastInvoiceNumber();
				$salesinvoice = new salesinvoice;
				$salesinvoice->sales_order_id = $salesorder->sales_order_id;
				$salesinvoice->product_id = $request->idbarang[$i];
				$salesinvoice->invoice_sales_number = $lastinvoice;
				$salesinvoice->date_sales_invoice = date('Y-m-d', strtotime($request->tglso));
				$salesinvoice->due_date = date('Y-m-d', strtotime("+" . ($j - 1) . "months", strtotime($request->tglso)));
				$salesinvoice->total_amount = $request->cicilan[$i];
				$salesinvoice->term_number = $j;
				$salesinvoice->status = 1;
				$salesinvoice->save();

				$log = new log;
				$log->actor = "andy.daz";
				$log->activity = "menambahkan";
				$log->object = "Sales Invoice";
				$log->object_details = $salesinvoice->invoice_sales_number;
				$log->status = 1;
				$log->save();

				// get invoice dengan term pertama
				if ($j == 1) {
					array_push($first_term_invoice, $salesinvoice->invoice_sales_id);
				}
			}
		}

		$totalpayment = array_sum($request->bayar);
		$installment = $request->installment;
		$remaining = $totalpayment - $installment;

		$lastpayment = (new SalesPaymentController)->getLastPaymentNumber();

		$salespayment = new salespayment;
		$salespayment->sales_order_id = $salesorder->sales_order_id;
		$salespayment->payment_sales_number = $lastpayment;
		$salespayment->date_sales_payment = date('Y-m-d', strtotime($request->tglso));
		$salespayment->total_paid = $totalpayment;
		$salespayment->is_first = 1;
		$salespayment->status = 1;
		$salespayment->save();

		// masukkan metode pembayaran dalam paymnet details
		for ($i = 0; $i < sizeof($request->metode); $i++) {
			$detailpayment = new detailsalespayment;
			$detailpayment->payment_sales_id = $salespayment->payment_sales_id;
			$detailpayment->payment_method_id = $request->metode[$i];
			$detailpayment->paid = $request->bayar[$i];
			$detailpayment->status = 1;
			$detailpayment->save();

			if ($request->metode[$i] == 2) {
				customer::where('customer_id', $request->pelanggan)->update([
					'total_balance' => DB::raw('total_balance-' . $request->bayar[$i]),
				]);
			}
		}

		if ($remaining > 0) {
			customer::where('customer_id', $request->pelanggan)->update([
				'total_balance' => DB::raw('total_balance+' . $remaining),
			]);
		}

		salesinvoice::whereIn('invoice_sales_id', $first_term_invoice)->update([
			'payment_sales_id' => $salespayment->payment_sales_id,
		]);
	}

	public function approveDraft(Request $request){
		salesorder::where('sales_order_id',$request->id)->update([
			'is_draft' => 0,
		]);

		$detailso = detailsalesorder::where('sales_order_id',$request->id)
			->where('status',1)
			->get();

		foreach ($detailso as $key => $value){
			product::where('product_id',$value->product_id)->update([
				'status' => 2,
			]);
		}
	}

	public function rejectDraft(Request $request){
		salesorder::where('sales_order_id',$request->id)->update([
			'status' => 2,
		]);

		$detailso = detailsalesorder::where('sales_order_id',$request->id)
			->where('status',1)
			->get();

		foreach ($detailso as $key => $value){
			product::where('product_id',$value->product_id)->update([
				'status' => 1,
			]);
		}
	}

	public function downloadSalesOrder(Request $request, $id)
	{
		$so = salesorder::with('details.product.category')
			->with('details.product.brand')
			->with('details.product.type')
			->with('first_payment.details.method.payment_method_type')
			->with('first_payment.details.method.bank')
			->with('payment_type')
			->where('sales_order_id',$id)
			->first();

		$terbilang = $this->terbilang($so->grand_total_idr);

		$pdf = DOMPDF::loadView('so.so_pdf', compact('so','terbilang'))->setPaper('A5','landscape');
		return $pdf->stream('SO.pdf');
	}

	public function deletepayment($payment, $salesorder){
		foreach($payment as $key => $value)
		{
			customer::where('customer_id', $salesorder->customer_id)->update([
				'total_balance' => DB::raw('total_balance-'. $value->overpaid),
			]);

			// cari payment yang menggunakas saldo lalu tambahkan saldo tersebut ke customer
			$detailpaymentsaldo = detailsalespayment::where('payment_sales_id',$value->payment_sales_id)
				->where('status',1)
				->where('payment_method_id',2)
				->get();

			foreach($detailpaymentsaldo as $key => $value2)
			{
				customer::where('customer_id', $salesorder->customer_id)->update([
					'total_balance' => DB::raw('total_balance+' . $value2->paid),
				]);

			}

			detailsalespayment::where('payment_sales_id',$value->payment_sales_id)->update([
				'status' => 2
			]);

			salesinvoice::where('sales_order_id',$value->payment_sales_id)->update([
				'status' => 2
			]);

			salespayment::where('payment_sales_id',$value->payment_sales_id)->update([
				'status' => 2
			]);
		}
	}

	public function deletedeliveryorder($salesorder){
		$deliveryorder = deliveryorder::where('sales_order_id',$salesorder->sales_order_id)
			->where('status',1);

		foreach ($deliveryorder->get() as $key => $value){
			$detaildo = detaildeliveryorder::where('delivery_order_id',$value->delivery_order_id)
				->where('status',1);

			$detaildo->update([
				'status' => 2
			]);
		}

		$deliveryorder->update([
			'status' => 2
		]);
	}

	public function createlog($action, $object, $number){
		$log = new log;
		$log->actor = "andy.daz";
		$log->activity = $action;
		$log->object = $object;
		$log->object_details = $number;
		$log->status = 1;
		$log->save();
	}

	public function createinvoice($so, $date, $amount, $product = null, $term = 1){
		$lastinvoice = (new SalesInvoiceController)->getLastInvoiceNumber();

		$salesinvoice = new salesinvoice;
		$salesinvoice->sales_order_id = $so;
		$salesinvoice->product_id = $product != null ? $product : null;
		$salesinvoice->invoice_sales_number = $lastinvoice;
		$salesinvoice->date_sales_invoice = $date;
		$salesinvoice->due_date = $term != null ? date('Y-m-d', strtotime("+" . ($term - 1) . "months", strtotime($date))) : null;
		$salesinvoice->total_amount = $amount;
		$salesinvoice->term_number = $term != null ? $term : 1;
		$salesinvoice->is_emailed = 0;
		$salesinvoice->status = 1;
		$salesinvoice->save();

		return $salesinvoice;
	}

	public function createpayment($so, $date, $total, $isfirst = null, $remaining =0, $addcharge = 0, $addnote = null){
		$lastpayment = (new SalesPaymentController)->getLastPaymentNumber();

		$salespayment = new salespayment;
		$salespayment->sales_order_id = $so;
		$salespayment->payment_sales_number = $lastpayment;
		$salespayment->date_sales_payment = $date;
		$salespayment->total_paid = $total;
		$salespayment->additional_charge = $addcharge;
		$salespayment->additional_note = $addnote;
		$salespayment->is_first = $isfirst != null ? 1 : 0;
		$salespayment->status = 1;
		$salespayment->overpaid = $remaining;
		$salespayment->save();

		return $salespayment;
	}

	public function createdetailpayment($payment, $method, $paid){

		$detailpayment = new detailsalespayment;
		$detailpayment->payment_sales_id = $payment;
		$detailpayment->payment_method_id = $method;
		$detailpayment->paid = $paid;
		$detailpayment->status = 1;
		$detailpayment->save();
	}

	public function createdeliveryorder($salesorder , $date){
		$lastdelivery = (new DeliveryOrderController)->getLastDeliveryNumber(); // Generate No Do Terakhir

		$deliveryorder = new deliveryorder;
		$deliveryorder->sales_order_id = $salesorder->sales_order_id;
		$deliveryorder->delivery_order_number = $lastdelivery;
		$deliveryorder->date_delivery  = $date;
		$deliveryorder->term_delivery_order = 1;
		$deliveryorder->status = 1;
		$deliveryorder->save();

		return $deliveryorder;
	}

	public function createdetaildelivery($deliveryorder, $sodetails, $product ){
		$detaildeliveryorder = new detaildeliveryorder;
		$detaildeliveryorder->delivery_order_id = $deliveryorder;
		$detaildeliveryorder->sales_order_details_id = $sodetails;
		$detaildeliveryorder->product_id = $product;
		$detaildeliveryorder->is_send = 1;
		$detaildeliveryorder->status = 1;
		$detaildeliveryorder->save();
	}
}

