<?php

namespace App\Http\Controllers;

use App\customer;
use App\customerlevel;

use Illuminate\Http\Request;

class LevelPelangganController extends Controller
{
    public function showLevelPelangganPage(){
    	$level = customerlevel::where('status','<>',2)->get();    	
    	return view('customer_level.customer_level', compact('level'));
    }

    public function getCustomerLevelData(Request $request){
    	$level = customerlevel::where('customer_level_id',$request->id)->first();
    	return $level;    
    }

    public function createCustomerLevel(Request $request){
    	$level =  new customerlevel;
    	$level->customer_level_name =  $request->level;
    	$level->nominal_from =  $request->nominalstart;
    	$level->nominal_end =  $request->nominalend;
    	$level->limit_credit =  $request->limit;
    	$level->save();
    }

    public function updateCustomerLevel(Request $request){
    	$level =  customerlevel::find($request->id);
			$level->nominal_from =  $request->nominalstart;
			$level->nominal_end =  $request->nominalend;
    	$level->customer_level_name =  $request->level;
    	$level->limit_credit =  $request->limit;
    	$level->save();
    }
}
