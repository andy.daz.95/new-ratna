<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\supplier;
use App\productcategory;
use App\brandbarang;
use App\tipebarang;
use App\product;
use App\purchaseorder;
use App\purchasediscount;
use App\detailpurchaseorder;
use App\paymenttype;
use App\log;
use App;
use DOMPDF;
use DataTable;


class PurchaseOrderController extends Controller
{
	public function showPurchaseOrderPage()
	{
		$data['supplier'] = supplier::all();
		$data['category'] = productcategory::all();
		$data['brand'] = brandbarang::all();
		$data['tipe'] = tipebarang::all();
		$data['payment_term'] = paymenttype::ForPo()->get();

		return view('po.po', compact('data'));
	}

	public function getPurchaseOrderTable(Request $request)
	{
		$data['release'] = purchaseorder::join('supplier', 'purchase_order.supplier_id', '=', 'supplier.supplier_id')
			->join('payment_type', 'purchase_order.payment_type_id', '=', 'payment_type.payment_type_id')
			->join('account', 'purchase_order.created_by', '=', 'account.account_id')
			->with('state')
			->has('release')
			->where('purchase_order.status', '=', 1)
			->orderBy('purchase_order_id','desc');

		$data['request'] = purchaseorder::join('supplier', 'purchase_order.supplier_id', '=', 'supplier.supplier_id')
			->join('payment_type', 'purchase_order.payment_type_id', '=', 'payment_type.payment_type_id')
			->join('account', 'purchase_order.created_by', '=', 'account.account_id')
			->has('unreleased')
			->with('state')
			->with('unset_price_sale')
			->with('good_receive')
			->orderBy('purchase_order_id','desc')
			->where('purchase_order.status', '=', 1);

		if ($request->ponumber && $request->ponumber != "")
		{
			$data['request'] = $data['request']->where('purchase_order.purchase_order_number','like','%'.$request->ponumber.'%');
			$data['release'] = $data['release']->where('purchase_order.purchase_order_number','like','%'.$request->ponumber.'%');
		}
		if ($request->date && $request->date != "")
		{
			$data['request'] = $data['request']->where('purchase_order.date_purchase_order','like','%'.$request->date.'%');
			$data['release'] = $data['release']->where('purchase_order.date_purchase_order','like','%'.$request->date.'%');
		}
		if ($request->supplier && $request->supplier != "")
		{
			$data['request'] = $data['request']->where('supplier.company_name','like','%'.$request->supplier.'%');
			$data['release'] = $data['release']->where('supplier.company_name','like','%'.$request->supplier.'%');
		}

		$data['request'] = $data['request']->get();
		$data['release'] = $data['release']->get();

		//merge the release and request collections
		$data = $data['request']->merge($data['release']);

		return DataTable::of($data)
			->setRowAttr([
				'value' => function($data){
					return $data->purchase_order_id;
				},
			])
			->addColumn('action', function ($data) {
				if (Session('roles')->name == 'master' && $data->state->purchase_order_state_id == 1 && sizeof($data->good_receive) == 0) {
					return '<a class="btn btn-sm btn-raised light-blue darken-2 edit tooltipped" mode="edit" data-position="bottom" data-tooltip="edit" value='.$data->purchase_order_id.'><i class="material-icons">edit</i></a>
						<a class="btn btn-sm btn-raised red delete-modal tooltipped" data-position="bottom" data-tooltip="delete"><i class="material-icons">delete</i></a>';
				}
				if (Session('roles')->role_id == 1 && $data->state->purchase_order_state_id == 4)
				{
					return '<a class="btn btn-sm btn-raised green release tooltipped" data-position= "bottom" data-tooltip= "release" ><i class="material-icons" > lock_open</i></a>
					<a class="btn btn-sm btn-raised red reject tooltipped" data-position= "bottom" data-tooltip= "block"><i class="material-icons"> block</i></a>';
				}
				if(Session('roles')->role_id != 1 && $data->state->purchase_order_state_id == 3)
				{
					return '<a class="btn btn-sm btn-raised light-blue darken-2 revise" mode="revise" value="{{$value->purchase_order_id}}"><i class="material-icons">edit</i></a>';
				}
			})
			->editColumn('grand_total_idr', function ($data) {
				return number_format($data->grand_total_idr, 0);
			})
			->smart(false)
			->make(true);

	}

	public function getLastPurchaseNumber(Request $request)
	{
		$lastpurchasetoday = purchaseorder::where('purchase_order_number', 'like', 'PO/' . date('dmy') . '/%')->orderby('purchase_order_id', 'desc')->first();
		if (empty($lastpurchasetoday)) {
			$newponumber = "PO/" . date('dmy') . "/1";
			return $newponumber;
		} else {
			$tmppurchase = explode('/', $lastpurchasetoday->purchase_order_number);
			$lastnumber = $tmppurchase[2];
			$newponumber = "PO/" . date('dmy') . "/" . ($lastnumber + 1);
			return $newponumber;
		}
	}

	public function getPurchaseOrder(Request $request)
	{
		$po = purchaseorder::rightjoin('purchase_order_details', 'purchase_order.purchase_order_id', '=', 'purchase_order_details.purchase_order_id')
			->leftjoin('product_category', 'product_category.product_category_id', '=', 'purchase_order_details.product_category_id')
			->leftjoin('product_brand', 'product_brand.product_brand_id', '=', 'purchase_order_details.product_brand_id')
			->leftjoin('product_type', 'product_type.product_type_id', '=', 'purchase_order_details.product_type_id')
			->where('purchase_order.purchase_order_id', $request->id)
			->where('purchase_order.status', 1)
			->where('purchase_order_details.status',1)
			->select('purchase_order.*', 'purchase_order_details.*', 'product_brand.name as b_name', 'product_type.name as t_name')
			->get();
		// return $po;
		return compact('po');
	}

	public function createPurchaseOrder(Request $request)
	{
		$total = array_sum($request->hargabeli);
		$diskonitem = array_sum($request->diskon);
		$grandtotal = $total - $diskonitem - $request->diskontransaksinominal;

		$purchaseorder = new purchaseorder;
		$purchaseorder->supplier_id = $request->supplier;
		$purchaseorder->payment_type_id = $request->pembayaran;
		$purchaseorder->shipping_term_id = 1;
		$purchaseorder->purchase_order_number = $request->nopo;
		$purchaseorder->payment_term = $request->terms;
		$purchaseorder->date_purchase_order = date('Y-m-d', strtotime($request->tglpo));
		$purchaseorder->grand_total_idr = $grandtotal;
		$purchaseorder->discount_transaction_nominal = $request->diskontransaksinominal;
		$purchaseorder->discount_transaction_percentage = $request->diskontransaksipercent;
		$purchaseorder->created_by = Session('user')->account_id;
		$purchaseorder->notes = $request->notes;
		$purchaseorder->status = 1;

		if (Session('roles')->role_id == 1) // Admin
		{
			$purchaseorder->purchase_order_state_id = 1; // langsung release
		} else{
			$purchaseorder->purchase_order_state_id = 4; // tunggu approval dari admin
		}
		$purchaseorder->save();

		$jumlahbarang = sizeof($request->kategori);
		$diskonnotabagirata =  $request->diskontransaksinominal / $jumlahbarang;

		for ($i = 0; $i < $jumlahbarang; $i++) {
			$detailpo = new detailpurchaseorder;
			$detailpo->purchase_order_id = $purchaseorder->purchase_order_id;
			$detailpo->product_category_id = $request->kategori[$i];
			$detailpo->product_brand_id = $request->brand[$i];
			$detailpo->product_type_id = $request->type[$i];
			$detailpo->price_buy = $request->hargabeli[$i];
			$detailpo->discount_nominal = $request->diskon[$i];
			$detailpo->discount_percentage = $request->percentfirst[$i];
			$detailpo->discount_percentage_2 = $request->percentsecond[$i];
			$detailpo->sub_total = $request->subtotal[$i];
			$detailpo->net_price_buy = $request->subtotal[$i] - $diskonnotabagirata;
			$detailpo->price_sale = 0;  // price sale dan profit rate di set 0; akan di updat oleh master di menu lain;
			$detailpo->profit_rate = 0;
			$detailpo->is_received = 0; //  belum receive
			$detailpo->status = 1;
			$detailpo->save();
		}

		$log = new log;
		$log->actor = "andy.daz";
		$log->activity = "menambahkan";
		$log->object = "Purchase Order";
		$log->object_details = $purchaseorder->purchase_order_number;
		$log->status = 1;
		$log->save();
	}

	public function releasePurchaseOrder(Request $request)
	{
		//ubah status jadi 1 atau release
		$po = purchaseorder::find($request->id);
		$po->purchase_order_state_id = 1;
		$po->save();
	}

	public function rejectPurchaseOrder(Request $request)
	{
		//ubah status jadi 1 atau release
		$po = purchaseorder::find($request->id);
		$po->purchase_order_state_id = 3;
		$po->save();
	}

	public function revisePurchaseOrder(Request $request)
	{
		$total = array_sum($request->hargabeli);
		$diskonitem = array_sum($request->diskon);
		$grandtotal = $total - $diskonitem - $request->diskontransaksinominal;

		$purchaseorder = purchaseorder::find($request->id);
		$purchaseorder->supplier_id = $request->supplier;
		$purchaseorder->payment_type_id = $request->pembayaran;
		$purchaseorder->shipping_term_id = 1;
		$purchaseorder->purchase_order_number = $request->nopo;
		$purchaseorder->payment_term = $request->terms;
		$purchaseorder->date_purchase_order = date('Y-m-d', strtotime($request->tglpo));
		$purchaseorder->grand_total_idr = $grandtotal;
		$purchaseorder->discount_transaction_nominal = $request->diskontransaksinominal;
		$purchaseorder->discount_transaction_percentage = $request->diskontransaksipercent;
		$purchaseorder->created_by = Session('user')->account_id;
		$purchaseorder->notes = $request->notes;
		$purchaseorder->status = 1;
		$purchaseorder->purchase_order_state_id = 4;
		$purchaseorder->save();

		detailpurchaseorder::where('purchase_order_id', $request->id)->delete();

		$jumlahbarang = sizeof($request->kategori);
		$diskonnotabagirata =  $request->diskontransaksinominal / $jumlahbarang;

		for ($i = 0; $i < $jumlahbarang; $i++) {
			$detailpo = new detailpurchaseorder;
			$detailpo->purchase_order_id = $purchaseorder->purchase_order_id;
			$detailpo->product_category_id = $request->kategori[$i];
			$detailpo->product_brand_id = $request->brand[$i];
			$detailpo->product_type_id = $request->type[$i];
			$detailpo->price_buy = $request->hargabeli[$i];
			$detailpo->discount_nominal = $request->diskon[$i];
			$detailpo->discount_percentage = $request->percentfirst[$i];
			$detailpo->discount_percentage_2 = $request->percentsecond[$i];
			$detailpo->sub_total = $request->subtotal[$i];
			$detailpo->net_price_buy = $request->subtotal[$i] - $diskonnotabagirata;
			$detailpo->price_sale = 0;  // price sale dan profit rate di set 0; akan di updat oleh master di menu lain;
			$detailpo->profit_rate = 0;
			$detailpo->is_received = 0; //  belum receive
			$detailpo->is_retur = 0; //  default (tidak di ret ur)
			$detailpo->status = 1;
			$detailpo->save();
		}

		$log = new log;
		$log->actor = "andy.daz";
		$log->activity = "merevisi";
		$log->object = "Purchase Order";
		$log->object_details = $purchaseorder->purchase_order_number;
		$log->status = 1;
		$log->save();
	}


	public function updatePurchaseOrder(Request $request)
	{

		$total = array_sum($request->hargabeli);
		$diskonitem = array_sum($request->diskon);
		$grandtotal = $total - $diskonitem - $request->diskontransaksinominal;


		$purchaseorder = purchaseorder::find($request->id);
		$purchaseorder->supplier_id = $request->supplier;
		$purchaseorder->payment_type_id = $request->pembayaran;
		$purchaseorder->purchase_order_number = $request->nopo;
		$purchaseorder->payment_term = $request->terms;
		$purchaseorder->date_purchase_order = date('Y-m-d', strtotime($request->tglpo));
		$purchaseorder->grand_total_idr = $grandtotal;
		$purchaseorder->discount_transaction_nominal = $request->diskontransaksinominal;
		$purchaseorder->discount_transaction_percentage = $request->diskontransaksipercent;
		$purchaseorder->notes = $request->notes;
		$purchaseorder->save();

		detailpurchaseorder::where('purchase_order_id',$request->id)->update([
			'status' => 2,
		]);

		$jumlahbarang = sizeof($request->kategori);
		$diskonnotabagirata =  $request->diskontransaksinominal / $jumlahbarang;
		for ($i = 0; $i < $jumlahbarang; $i++) {
			$detailpo = new detailpurchaseorder;
			$detailpo->purchase_order_id = $purchaseorder->purchase_order_id;
			$detailpo->product_category_id = $request->kategori[$i];
			$detailpo->product_brand_id = $request->brand[$i];
			$detailpo->product_type_id = $request->type[$i];
			$detailpo->price_buy = $request->hargabeli[$i];
			$detailpo->discount_nominal = $request->diskon[$i];
			$detailpo->discount_percentage = $request->percentfirst[$i];
			$detailpo->discount_percentage_2 = $request->percentsecond[$i];
			$detailpo->sub_total = $request->subtotal[$i];
			$detailpo->net_price_buy = $request->subtotal[$i] - $diskonnotabagirata;
			$detailpo->price_sale = 0;  // price sale dan profit rate di set 0; akan di updat oleh master di menu lain;
			$detailpo->profit_rate = 0;
			$detailpo->is_received = 0; //  belum receive
			$detailpo->status = 1;
			$detailpo->save();
		}

		$log = new log;
		$log->actor = "andy.daz";
		$log->activity = "mengubah";
		$log->object = "Purchase Order";
		$log->object_details = $purchaseorder->purchase_order_number;
		$log->status = 1;
		$log->save();

	}

	 public function deletePurchaseOrder(Request $request)
	 {
	     purchaseorder::where('purchase_order_id', $request->id)->update(['status'=>2]);
	     detailpurchaseorder::where('purchase_order_id',$request->id)->update(['status'=>2]);
	 }

	public function showHargaJualPage()
	{
		$po = purchaseorder::has('unset_price_sale')
			->where('purchase_order_state_id',1)
			->get();
		return view('harga_jual.harga_jual', compact('po'));
	}

	public function updateHargaJual(Request $request){
//  	return $request->all();
		for ($i = 0; $i < sizeof($request->id); $i++) {
			$detailpo = detailpurchaseorder::find($request->id[$i]);
			$detailpo->price_sale = $request->hargajual[$i];  // price sale dan profit rate di set 0; akan di updat oleh master di menu lain;
			$detailpo->profit_rate = $request->rate[$i];
			$detailpo->save();
		}
	}

	public function getPurchaseOrderData(Request $request)
	{
		if($request->id != '0')
		{
			$id = $request->id;

			$po = purchaseorder::with('detailswopricesale.category')
				->with('detailswopricesale.brand')
				->with('detailswopricesale.type')
				->with('supplier')
				->where('purchase_order_id', $id)
				->first();

			return $po;
		}
	}

	public function downloadPurchaseOrder(Request $request, $id)
	{
		$po = purchaseorder::with('details.category')
			->with('details.brand')
			->with('details.type')
			->with('supplier')
			->with('paymenttype')
			->where('purchase_order_id', $id)
			->first();

		$pdf = DOMPDF::loadView('po.po_pdf', compact('po'));
		return $pdf->stream('PO.pdf');
	}
}
