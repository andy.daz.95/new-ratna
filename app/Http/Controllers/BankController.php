<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\bank;
use App\paymentmethodtype;
use App\paymentmethod;

class BankController extends Controller
{
	public function showBankPage()
	{
		$bank = bank::where('status',1)->get();
		$paymentmethodtype = paymentmethodtype::where('status',1)->whereNotIn('payment_method_type_id',[1,2])->get();
		return view('bank.bank', compact('bank','paymentmethodtype'));
	}
	public function createBank(Request $request)
	{
		$bank = new bank;
		$bank->bank_name = $request->bank;
		$bank->status = 1;
		$bank->save();

		$paymentmethodtype = paymentmethodtype::where('status',1)
			->whereIn('payment_method_type_id',[3,4,5])
			->get();

		foreach ($paymentmethodtype as $key => $value)
		{
			$paymentmethod = new paymentmethod;
			$paymentmethod->bank_id = $bank->bank_id;
			$paymentmethod->payment_method_type_id = $value->payment_method_type_id;
			if(in_array($value->payment_method_type_id, $request->type))
			{
				$paymentmethod->status = 1;
			}else{
				$paymentmethod->status = 2;
			}
			$paymentmethod->save();
		}
	}

	public function getBankData(Request $request){
		$id = $request->id;
		$bank = bank::where('bank_id',$id)->select('*')->first();
		$paymentmethod = paymentmethod::with('payment_method_type')->where('bank_id',$id)->where('status',1)->get();
		return compact('bank','paymentmethod');
	}

	public function modalBankData(Request $request){
		$id= $request->id;
		$data= bank::where('bank_id',$id)->first();
		return view('bank.modal_bank', compact('data'));
	}

	public function deleteBank(Request $request)
	{
		$id = $request->id;
		bank::where('bank_id',$id)->delete();

		paymentmethod::where('bank_id',$id)
			->update([
				'status' => 2,
			]);
	}

	public function updateBank(Request $request)
	{
		$id = $request->id;
		$bank = bank::where('bank_id', $id)->update(
			[
				'bank_name' => $request->bank,
			]
		);

		$paymentmethodtype = paymentmethodtype::where('status',1)
			->whereIn('payment_method_type_id',[3,4,5])
			->get();

		foreach ($paymentmethodtype as $key => $value)
		{
			$paymentmethod = paymentmethod::where('bank_id',$id)
				->where('payment_method_type_id', $value->payment_method_type_id)
				->first();

			if(in_array($value->payment_method_type_id, $request->type))
			{
				$paymentmethod->status = 1;
			}else{
				$paymentmethod->status = 2;
			}
			$paymentmethod->save();
		}
	}
}