<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\customer;
use App\salesinvoice;
use App\salesorder;
use App\salespayment;
use App\paymentmethod;
use App\detailsalespayment;
use App\detailsalesorder;
use App\product;
use App\log;
use DB;
use DOMPDF;
use DataTable;
use App\Traits\NumberToLetterTrait;

class SalesPaymentController extends Controller
{
	use NumberToLetterTrait;
	public function showSalesPaymentPage()
	{
		$data['payment'] = salespayment::leftjoin('sales_order','sales_order.sales_order_id','=','payment_sales.sales_order_id')
			->with('so.customer')
			->orderBy('payment_sales_id','desc')
			->where('payment_sales.status','<>',2)
			->where('sales_order.payment_type_id',2)
			->get();

		$data['payment_method'] = $payment_method = paymentmethod::with('payment_method_type')
			->with('bank')
			->where('status','<>',2)
			->get();

		$data['customer'] = customer::has('so')
			->where('status','<>',2)
			->get();

		return view('sales_payment.sales_payment', compact('data'));
	}

	public function getSalesPaymentTable(Request $request)
	{
		$payment = salespayment::leftjoin('sales_order','sales_order.sales_order_id','=','payment_sales.sales_order_id')
			->with('so.customer')
			->orderBy('payment_sales_id','desc')
			->where('payment_sales.status','<>',2)
			->where('sales_order.payment_type_id',2);

		if($request->number && $request->number != "")
		{
			$payment = $payment->where('payment_sales.payment_sales_number', 'like', '%'.$request->number.'%');
		}
		if($request->date && $request->date != "")
		{
			$payment = $payment->where('payment_sales.date_sales_payment', 'like', '%'.$request->date.'%');
		}
		if($request->pelanggan && $request->pelanggan != "")
		{
			$payment = $payment->whereHas('so.customer', function($query) use ($request){
				$query->where('customer.first_name', 'like', '%'.$request->pelanggan.'%')
					->orWhere('customer.last_name', 'like', '%'.$request->pelanggan.'%');
			});
		}
		$payment = $payment->get();

		return DataTable::of($payment)
			->setRowAttr([
				'value' => function($payment){
					return $payment->payment_sales_id;
				},
			])
			->addColumn('action', function ($payment){
				if(Session('roles')->name == 'master' && $payment->is_first == 0) {
					return
						'<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>';
				}
			})
			->smart(false)
			->make(true);
	}



	public function getLastPaymentNumber()
	{
		$lastpaymenttoday = salespayment::where('payment_sales_number','like','SP/'.date('dmy').'/%')->orderby('payment_sales_id','desc')->first();
		if(empty($lastpaymenttoday))
		{
			$newpaymentnumber = "SP/".date('dmy')."/1";
			return $newpaymentnumber;
		}
		else{
			$tmppayment = explode('/',$lastpaymenttoday->payment_sales_number);
			$lastnumber = $tmppayment[2];
			$newpaymentnumber = "SP/".date('dmy')."/".($lastnumber+1);
			return $newpaymentnumber;
		}
	}

	public function getCreditDetails(Request $request){
		$salesinvoice = salesinvoice::where('sales_order_id',$request->so)->get();
		$product = detailsalesorder::where('sales_order_id',$request->so)->select('product_id')->get()->pluck('product_id')->toArray();

		$creditdata = product::has('invoice')
			->with('invoice.payment')
			->with('category')
			->with('brand')
			->with('type')
			->whereIn('product_id',$product)
			->get();

		$details = view('sales_payment.sales_payment_detail', compact('creditdata'))->render();
		$so = salesorder::where('sales_order_id',$request->so)
			->with('payment_type')
			->first();

		return compact('details','so');
	}

	public function getSalesPayment(Request $request)
	{
		$id = $request->id;
		$payment = salespayment::with('details')
			->with('so.payment_type')
			->where('payment_sales_id',$id)
			->first();

		$paymentdata = salesinvoice::with('product.category')
			->with('product.brand')
			->with('product.type')
			->where('payment_sales_id',$id)
			->first();

		$details = view('sales_payment.sales_payment_detail', compact('paymentdata'))->render();
		return compact('details','payment');
	}

	public function getCustomerData(Request $request)
	{
		$data['so'] = salesorder::has('credit_sales')
			->has('open_invoice')
			->where('customer_id',$request->customer)
			->where('status',1)
			->where('is_draft',0)
			->get();

//      return $data;
		$data['customer'] = customer::where('customer_id',$request->customer)->first();
		return $data;
	}

	public function createPayment(Request $request)
	{

		$totalpayment = array_sum($request->bayar);
		$total = $request->total;
		$remaining = $totalpayment - $total;

		$salespayment = new salespayment;
		$salespayment->sales_order_id = $request->so;
		$salespayment->payment_sales_number = $request->nopayment;
		$salespayment->date_sales_payment = date('Y-m-d', strtotime($request->tglpayment));
		$salespayment->total_paid = $totalpayment;
		$salespayment->note = $request->note;
		$salespayment->is_first = 0;
		$salespayment->additional_charge = $request->addcharge;
		$salespayment->additional_note = $request->addnote;
		$salespayment->status = 1;
		$salespayment->overpaid = $remaining;
		$salespayment->save();

		// masukkan metode pembayaran dalam paymnet details
		for ($i=0; $i <sizeof($request->metode); $i++)
		{
			$detailpayment = new detailsalespayment;
			$detailpayment->payment_sales_id = $salespayment->payment_sales_id;
			$detailpayment->payment_method_id = $request->metode[$i];
			$detailpayment->paid = $request->bayar[$i];
			$detailpayment->status = 1;
			$detailpayment->save();

			if($request->metode[$i] == 2){
				customer::where('customer_id',$request->customer)->update([
					'total_balance' => DB::raw('total_balance-'.$request->bayar[$i]),
				]);
			}
		}

		if($remaining > 0)
		{
			customer::where('customer_id',$request->customer)->update([
				'total_balance' => DB::raw('total_balance+'.$remaining),
			]);
		}

		salesinvoice::whereIn('invoice_sales_id',$request->checkinvoice)->update([
			'payment_sales_id' => $salespayment->payment_sales_id,
		]);

		$log = new log;
		$log->actor = "andy.daz";
		$log->activity = "menambahkan";
		$log->object = "Sales Payment";
		$log->object_details = $salespayment->payment_sales_number;
		$log->status = 1;
		$log->save();

		return $salespayment->payment_sales_id;
	}

	public function deletePayment(Request $request){
		$salespayment = salespayment::with('so')->find($request->id);

		$detailpayment = detailsalespayment::where('payment_sales_id',$salespayment->payment_sales_id)
			->where('status',1)
			->get();

		foreach ($detailpayment as $key => $value){
			if($value->payment_method_id == 2){
				customer::where('customer_id',$salespayment->so->customer_id)->update([
					'total_balance' => DB::raw('total_balance+'.$value->paid),
				]);
			}
			// update detail status jadi 2
			detailsalespayment::where('payment_Sales_details_id', $value->payment_Sales_details_id)
				->update([
					'status' => 2
				]);
		}

		if($salespayment->overpaid > 0){
			customer::where('customer_id',$salespayment->so->customer_id)->update([
				'total_balance' => DB::raw('total_balance-'.$salespayment->overpaid),
			]);
		}

		salesinvoice::where('payment_sales_id',$salespayment->payment_sales_id)->update([
			'payment_sales_id' => null,
		]);

		$salespayment->status = 2;
		$salespayment->save();
	}

	public function downloadPaymentCredit(Request $request, $id)
	{
		$payment = salespayment::with('details')
			->with('invoice.so.customer')
			->with('invoice.product')
			->where('payment_sales_id',$id)
			->first();

		$product = $payment->invoice->product_id;

		$listinvoice = salesinvoice::where('product_id',$product)
			->get();

		$terbilang = $this->terbilang($payment->invoice->total_amount);

//		dump($payment->invoice->first()->total_amount);

		$pdf = DOMPDF::loadView('sales_payment.payment_credit_pdf', compact('payment','listinvoice','terbilang'))
			->setPaper('A5','landscape');
		return $pdf->stream('Payment Credit.pdf');
	}
}