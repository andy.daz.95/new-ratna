<?php

namespace App\Http\Controllers;

use App\customerstate;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CustomerStateController extends Controller
{

	public function getCustomerState($id)
	{
		return customerstate::find($id);
	}

	public function createCustomerState($id, $level, $type, $checkdate)
	{
		$state = new customerstate();
		$state->customer_id = $id;
		$state->customer_level_id = $level;
		$state->update_type = $type;
		$state->date_changed = Carbon::now()->toDateString();
		$state->next_check_date = $checkdate;
		$state->status = 1;
		$state->save();
	}

	public function deleteCustomerState($id)
	{
		customerstate::find($id)->update(['status'=>2]);
	}
}
