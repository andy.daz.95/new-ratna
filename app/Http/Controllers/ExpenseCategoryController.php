<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\expensecategory;

class ExpenseCategoryController extends Controller
{
	//
	public function showExpenseCategoryPage(){
		$expense_category = expensecategory::where('status',1)->get();
		return view('expense_category.expense_category',compact('expense_category'));
	}

	public function createExpenseCategory(Request $request){
		$expensecategory = new expensecategory;
		$expensecategory->expense_description = $request->description;
		$expensecategory->status = 1;
		$expensecategory->save();
	}

	public function getExpenseCategoryData(Request $request){
		$expensecategory = expensecategory::find($request->id);
		return $expensecategory;
	}

	public function modalExpenseCategoryData(Request $request){
		$id= $request->id;
		$data = expensecategory::where('expense_category_id',$id)->first();
		return view('expense_category.modal_expense_category', compact('data'));
	}

	public function updateExpenseCategory(Request $request){
		$expensecategory = expensecategory::find($request->id);
		$expensecategory->expense_description = $request->description;
		$expensecategory->update();
	}

	public function deleteExpenseCategory(Request $request){
		$expensecategory = expensecategory::find($request->id);
		$expensecategory->status = 2;
		$expensecategory->update();
	}
}
