<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\purchaseorder;
use App\detailpurchaseorder;
use App\purchaseinvoice;
use App\detailpurchaseinvoice;
use App\log;
use App;
use DB;
use PDF;
use DataTable;

class PurchaseInvoiceController extends Controller
{
    public function showPurchaseInvoicePage()
    {
        // ->whereRaw('tableb.total_paid < purchase_order.grand_total_idr - purchase_order.reduction')
        // ->orWhereNull('tableb.total_paid')

        $data['invoice'] = purchaseinvoice::
            leftjoin('purchase_order','purchase_order.purchase_order_id','=','invoice_purchase.purchase_order_id')
            ->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
            ->where('invoice_purchase.status','<>',2)
            ->selectRaw('invoice_purchase.*,supplier.*,purchase_order.purchase_order_number, purchase_order.notes as po_note, date_purchase_order,date_add(invoice_purchase.date_purchase_invoice, Interval 14 Day) as jatuh_tempo')
            ->get();
        return view('purchase_invoice.purchase_invoice', compact('data'));
    }

	public function getPurchaseInvoiceTable(Request $request)
	{

		$invoice = purchaseinvoice::leftjoin('purchase_order','purchase_order.purchase_order_id','=','invoice_purchase.purchase_order_id')
			->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
			->where('invoice_purchase.status','<>',2)
			->selectRaw('invoice_purchase.*,supplier.*,purchase_order.purchase_order_number, purchase_order.notes as po_note, date_purchase_order,date_add(invoice_purchase.date_purchase_invoice, Interval 14 Day) as jatuh_tempo');

		if($request->number && $request->number != "")
		{
			$invoice = $invoice->where('invoice_purchase.invoice_purchase_number', 'like', '%'.$request->number.'%');
		}
		if($request->po && $request->po != "")
		{
			$invoice = $invoice->where('purchase_order.purchase_order_number', 'like', '%'.$request->po.'%')
				->orWhere('purchase_order.notes', 'like', '%'.$request->po.'%');
		}
		if($request->supplier && $request->supplier != "")
		{
			$invoice = $invoice->where('supplier.company_name', 'like', '%'.$request->supplier.'%');
		}

		$invoice = $invoice->get();

		return DataTable::of($invoice)
			->setRowAttr([
				'value' => function($invoice){
					return $invoice->invoice_purchase_id;
				},
			])
			->editColumn('purchase_order_number', function($invoice){
				return "{$invoice->purchase_order_number} / {$invoice->po_note}";
			})
			->editColumn('total_amount', function ($expense) {
				return number_format($expense->total_amount, 0);
			})
			->smart(false)
			->make(true);
	}

    public function getPurchaseInvoice(Request $request)
    {
        $invoice = purchaseinvoice::find($request->id);

        $podetail = detailpurchaseorder::join('product_category','product_category.product_category_id','purchase_order_details.product_category_id')
                ->join('product_brand','product_brand.product_brand_id','purchase_order_details.product_brand_id')
                ->join('product_type','product_type.product_type_id','purchase_order_details.product_type_id')
                ->where('purchase_order_id',$invoice->purchase_order_id)
                ->select('purchase_order_details.*','product_category.description as c_name','product_brand.name as b_name','product_type.name as t_name')
								->where('purchase_order_details.is_received',1)
                ->get();

        $discount_transaction = purchaseorder::find($invoice->purchase_order_id)->first()->discount_transaction;

        return compact('podetail','discount_transaction');
    }

    public function getLastInvoiceNumber()
    {
        $lastinvoicetoday = purchaseinvoice::where('invoice_purchase_number','like','PIN/'.date('dmy').'/%')->orderby('invoice_purchase_id','desc')->first();
        if(empty($lastinvoicetoday))
        {
            $newinvoicenumber = "PIN/".date('dmy')."/1";
            return $newinvoicenumber;
        }
        else{
            $tmpinvoice = explode('/',$lastinvoicetoday->invoice_purchase_number); 
            $lastnumber = $tmpinvoice[2];
            $newinvoicenumber = "PIN/".date('dmy')."/".($lastnumber+1);
            return $newinvoicenumber;
        }
    }
}
