<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\brandbarang;
use App\productcategory;
use DataTable;

class BrandBarangController extends Controller
{
    public function showBrandBarangPage()
    {
    	$category = productcategory::all();
    	return view('brand_barang.brand_barang', compact('brand','category'));
    }

    public function getBrandTable(Request $request)
		{
			$brand = brandbarang::with('category')->where('product_brand.status','<>',2);

			if($request->brand && $request->brand != "")
			{
				$brand = $brand->where('product_brand.name', 'like', '%'.$request->brand.'%');
			}

			$brand = $brand->get();

			return DataTable::of($brand)
				->setRowAttr([
					'value' => function($brand){
						return $brand->product_brand_id;
					},
				])
				->addColumn('action', function ($brand){
					if(Session('roles')->name == 'master') {
						return
							'<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="'.$brand->product_brand_id.'"><i class="material-icons">edit</i></a>
							<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>';
					}
				})
				->smart(false)
				->make(true);
		}

    public function createBrandBarang(Request $request)
	{
		$brand = new brandbarang;
		$brand->name = $request->brand;
		$brand->product_category_id = $request->category;
		$brand->code = $request->code;
		$brand->status = 1;
		$brand->save();
	}

	public function getBrandData(Request $request){
		$id = $request->id;
		$brand = brandbarang::where('product_brand_id',$id)->with('category')->select('*')->first();
		return $brand;
	}

	public function getBrandOnKategori(Request $request){
		$id = $request->id;
		$brand = brandbarang::where('product_category_id',$id)->select('*')->get();
		return $brand;
	}

	public function modalBrandData(Request $request){
		$id= $request->id;
		$data= brandbarang::where('product_brand_id',$id)->first();
		return view('brand_barang.modal_brand', compact('data'));
	}

	public function deleteBrandBarang(Request $request)
	{
		$id = $request->id;
		brandbarang::where('product_brand_id',$id)->update(['status'=>2]);
	}

	public function updateBrandBarang(Request $request)
	{
		$id = $request->id;
		brandbarang::where('product_brand_id', $id)->update(
			[
				'name' => $request->brand,
				'product_category_id' => $request->category,
				'code' => $request->code,
			]
			);
	}
}