<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\PurchaseInvoiceController;
use App\purchaseorder;
use App\purchasereturn;
use App\purchaseinvoice;
use App\productwarehouse;
use App\goodreceive;
use App\pricepurchase;
use App\detailgoodreceive;
use App\detailpurchaseorder;
use App\warehouse;
use App\log;
use App\product;
use App\productcategory;
use App\brandbarang;
use App\tipebarang;
use App;
use DB;
use DOMPDF;
use Excel;
use DataTable;

class PenerimaanController extends Controller
{
	public function showPenerimaanBarangPage(){

		$data['po'] = purchaseorder::where('purchase_order.status',1)
			->whereExists(function ($query) {
				$query->select(DB::raw(1))
					->from('purchase_order_details')
					->whereRaw('purchase_order_details.purchase_order_id = purchase_order.purchase_order_id')
					->where('is_received',0);
			})
			->whereNotExists(function ($query) {
				$query->select(DB::raw(1))
					->from('purchase_order_details')
					->whereRaw('purchase_order_details.purchase_order_id = purchase_order.purchase_order_id')
					->where('price_sale',0)
					->where('status',1);
			})
			->get();

		// return $data['po'];
		$data['warehouse'] = warehouse::where('status',"<>",2)->get();
//		$data['bpb'] = goodreceive::leftjoin('purchase_order','purchase_order.purchase_order_id','=','good_receive.purchase_order_id')
//			->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
//			->where('good_receive.status','<>',2)
//			->select('*','purchase_order.notes as po_note')
//			->orderBy('good_receive_id','desc')
//			->get();

		return view('penerimaan_barang.penerimaan_barang', compact('data'));
	}

	public function getPenerimaanTable(Request $request)
	{

		$penerimaan = goodreceive::leftjoin('purchase_order','purchase_order.purchase_order_id','=','good_receive.purchase_order_id')
			->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
			->where('good_receive.status','<>',2)
			->select('*','purchase_order.notes as po_note')
			->orderBy('good_receive_id','desc');

		if($request->number && $request->number != "")
		{
			$penerimaan = $penerimaan->where('good_receive.good_receive_code', 'like', '%'.$request->number.'%');
		}
		if($request->po && $request->po != "")
		{
			$penerimaan = $penerimaan->where('purchase_order.purchase_order_number', 'like', '%'.$request->po.'%')
			->orWhere('purchase_order.notes', 'like', '%'.$request->po.'%');
		}
		if($request->supplier && $request->supplier != "")
		{
			$penerimaan = $penerimaan->where('supplier.company_name', 'like', '%'.$request->supplier.'%');
		}

		$penerimaan = $penerimaan->get();

		return DataTable::of($penerimaan)
			->setRowAttr([
				'value' => function($penerimaan){
					return $penerimaan->good_receive_id;
				},
			])
			->addColumn('action', function ($penerimaan){
				$product =  product::join('good_receive_details','product.good_receive_details_id','=','good_receive_details.good_receive_details_id')
					->where('good_receive_details.good_receive_id', $penerimaan->good_receive_id)
					->select('product_id')
					->get();

				$productArr = implode(',', $product->pluck('product_id')->flatten()->toArray());
				if(Session('roles')->name == 'master') {
					return
						'<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="'.$penerimaan->good_receive_id.'"><i class="material-icons">edit</i></a>
							<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>
							<a href="excelgrbarcode/'.$productArr.'" target="_blank" class="btn btn-sm btn-raised orange"><i class="material-icons">print</i></a>';
				}else{
					return '<a href="excelgrbarcode/'.$productArr.'" target="_blank" class="btn btn-sm btn-raised orange"><i class="material-icons">print</i></a>';
				}
			})
			->editColumn('purchase_order_number', function($penerimaan){
				return "{$penerimaan->purchase_order_number} / {$penerimaan->po_note}";
			})
			->smart(false)
			->make(true);

	}

	public function getLastBpbNumber(Request $request)
	{
		$lastbpbtoday = goodreceive::where('good_receive_code','like','GR/'.date('dmy').'/%')->orderby('good_receive_id','desc')->first();
		if(empty($lastbpbtoday))
		{
			$newbpbnumber = "GR/".date('dmy')."/1";
			return $newbpbnumber;
		}
		else{
			$tmpbpb = explode('/',$lastbpbtoday->good_receive_code);
			$lastnumber = $tmpbpb[2];
			$newbpbnumber = "GR/".date('dmy')."/".($lastnumber+1);
			return $newbpbnumber;
		}
	}

	public function getPenerimaan(Request $request)
	{
		$po = goodreceive::where('good_receive_id',$request->id)->first()->purchase_order_id;
		$bpb = goodreceive::leftjoin('good_receive_details','good_receive_details.good_receive_id','=','good_receive.good_receive_id')
			->leftjoin('purchase_order_details','purchase_order_details.purchase_order_details_id','=','good_receive_details.purchase_order_details_id')
			->leftjoin('purchase_order','purchase_order.purchase_order_id','=','good_receive.purchase_order_id')
			->leftjoin('supplier','purchase_order.supplier_id','=','supplier.supplier_id')
			->leftjoin('product_category','product_category.product_category_id','=','purchase_order_details.product_category_id')
			->leftjoin('product_brand','product_brand.product_brand_id','=','purchase_order_details.product_brand_id')
			->leftjoin('product_type','product_type.product_type_id','=','purchase_order_details.product_type_id')
			->leftjoin('warehouse','warehouse.warehouse_id','=','good_receive_details.warehouse_id')
			->select('good_receive.*','purchase_order.*','supplier.*','purchase_order_details.*','product_category.description as c_name','product_brand.name as b_name','product_type.name as t_name','good_receive_details.warehouse_id')
			->where('good_receive.good_receive_id',$request->id)
			->get();
		return $bpb;
	}

	public function getPurchaseOrderData(Request $request)
	{
		//$podata; $podetail; $grdetail;
		if($request->id != '0')
		{
			$id = $request->id;

			$podata = purchaseorder::join('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
				->leftjoin('good_receive','good_receive.purchase_order_id','=','purchase_order.purchase_order_id')
				->where('purchase_order.purchase_order_id',$id)
				->select('purchase_order.*','supplier.company_name as s_name','good_receive.good_receive_id as g_id')
				->first();

			$podetail = detailpurchaseorder::join('product_category','product_category.product_category_id','purchase_order_details.product_category_id')
				->join('product_brand','product_brand.product_brand_id','purchase_order_details.product_brand_id')
				->join('product_type','product_type.product_type_id','purchase_order_details.product_type_id')
				->where('purchase_order_id',$podata->purchase_order_id)
				->where('purchase_order_details.is_received',0)
				->select('purchase_order_details.*','product_category.description as c_name','product_brand.name as b_name','product_type.name as t_name')
				->get();

			return compact('podata', 'podetail');
		}
	}

	public function cancelBarang(Request $request)
	{
		for ($i=0; $i <sizeof($request->cancel); $i++)
		{
			$detailpo = detailpurchaseorder::where('purchase_order_details_id',$request->cancel[$i])->first();
			$detailpo->is_received = 2;
			$detailpo->save();
		}

		$po = $detailpo->purchase_order_id;

		purchaseorder::where('purchase_order_id',$po)->update([
			'reduction' => DB::raw('reduction+'.$detailpo->net_price_buy),
		]);
	}

	public function createPenerimaanBarang(Request $request)
	{
		// return date('m');
		$goodreceive = new goodreceive;
		$goodreceive->purchase_order_id = $request->po;
		$goodreceive->good_receive_code = $request->nobpb;
		$goodreceive->good_receive_date  = date('Y-m-d',strtotime($request->tglbpb));
		$goodreceive->note = $request->notes;
		$goodreceive->reference_number = "";
		$goodreceive->status = 1;
		$goodreceive->save();

		//array untuk nampung produk yang baru dibuat, untuk diprint barcodenya
		$recentproduct = [];
		for ($i=0; $i <sizeof($request->id); $i++)
		{
			//get detail po  untuk dapetin category, brand dan type
			$detailpo = detailpurchaseorder::where('purchase_order_details_id',$request->id[$i])->first();

			$detailgr = new detailgoodreceive;
			$detailgr->good_receive_id = $goodreceive->good_receive_id;
			$detailgr->warehouse_id = $request->gudang[$i];
			$detailgr->purchase_order_details_id = $request->id[$i];
			$detailgr->status = 1;
			$detailgr->save();

			//save product dulu nanti baru di update bar_code setelah kita dapet id yang auto increment
			$product = new product;
			$product->product_category_id = $detailpo->product_category_id;
			$product->good_receive_details_id = $detailgr->good_receive_details_id;
			$product->warehouse_id = $request->gudang[$i];
			$product->product_brand_id = $detailpo->product_brand_id;
			$product->product_type_id = $detailpo->product_type_id;
			$product->price_sale = $detailpo->price_sale;
			$product->price_buy = $detailpo->net_price_buy;
			$product->profit_rate = $detailpo->profit_rate;
			$product->status = 1;
			$product->save();

			array_push($recentproduct, $product->product_id);

			//dapetin category dan brand code nya
			$categorycode = productcategory::find($product->product_category_id)->code;
			$brandcode = brandbarang::find($product->product_brand_id)->code;
			$runningnumber = str_pad($product->product_id, 4, '0', STR_PAD_LEFT);
			$month = date('m');
			$year = date('y');

			$first = substr($month, 0,1);
			$second = substr($year, 0,1);
			$third = substr($month, 1,1);
			$fourth = substr($year, 1,1);

			//update status is received di detail po nya
			$detailpo->is_received = 1;
			$detailpo->save();

			//update bar codenya
			$product->where('product_id',$product->product_id)->update(['bar_code'=> $categorycode.$brandcode.'-'.$runningnumber.'-'.$first.$second.$third.$fourth]);
		}

		//cek lagi apakah sudah receive semua, kalau udah buat invoice
		$detailpo = detailpurchaseorder::where('purchase_order_id',$request->po)
			->where('purchase_order_details.status','<>',2)
			->where('is_received',0)
			->count();

		//cek apakah uda ada invoice yang dibuat untuk po tersebut klo belom baru buat invoice
		$invoice = purchaseinvoice::where('purchase_order_id',$request->po)
			->where('status',1)
			->count();

		if($detailpo == 0 && $invoice == 0)
		{
			$lastinvoice = (new PurchaseInvoiceController)->getLastInvoiceNumber();
			$po = purchaseorder::where('purchase_order_id',$request->po)->first();
			$total = $po->grand_total_idr;
			$diskontransaction = $po->discount_transaction;
			$diskonsatuan = detailpurchaseorder::where('purchase_order_id',$request->po)
				->selectRaw("sum(discount_nominal) as diskonsatuan")
				->groupby('purchase_order_id')->first()->diskonsatuan;
			//check ada potong nota / potong invoice di po kolom reduction
			$reduction = $po->reduction;

			$invoice = new purchaseinvoice;
			$invoice->purchase_order_id = $request->po;
			$invoice->invoice_purchase_number =  $lastinvoice;
			$invoice->total_discount = $diskontransaction + $diskonsatuan;
			$invoice->total_amount = $total - $reduction;
			$invoice->date_purchase_invoice = date('Y-m-d');
			$invoice->status = 1;
			$invoice->save();
		}

		$log = new log;
		$log->actor = "andy.daz";
		$log->activity = "menambahkan";
		$log->object = "bukti penerimaan barang";
		$log->object_details = $goodreceive->good_receive_code;
		$log->status = 1;
		$log->save();

		return $recentproduct;
	}

	public function updatePenerimaanBarang(request $request){

		$goodreceive = goodreceive::find($request->idbpb);
		$goodreceive->good_receive_date  = date('Y-m-d',strtotime($request->tglbpb));
		$goodreceive->note = $request->notes;
		$goodreceive->update();

		$log = new log;
		$log->actor = "andy.daz";
		$log->activity = "mengubah";
		$log->object = "bukti penerimaan barang";
		$log->object_details = $goodreceive->good_receive_code;
		$log->status = 1;
		$log->save();
	}

	public function deletePenerimaanBarang(request $request){

		$goodreceive = goodreceive::find($request->id);

		$detailgr = detailgoodreceive::where('good_receive_id', $goodreceive->good_receive_id)
			->where('status',1)
			->get();

		//kembalikan is_received di purchase order details to 0
		foreach ($detailgr as $key => $value) {
			detailpurchaseorder::where('purchase_order_details_id', $value->purchase_order_details_id)->update([
				'is_received' => 0
			]);

			detailgoodreceive::where('good_receive_details_id',$value->good_receive_details_id)->update([
				'status' => 2
			]);
		}

		//hapus semua invoice yang terkait dengan po tersebut
		purchaseinvoice::where('purchase_order_id', $goodreceive->purchase_order_id)
			->update([
				'status' => 2
			]);

		$goodreceive->status = 2;
		$goodreceive->update();

		$log = new log;
		$log->actor = "andy.daz";
		$log->activity = "menghapus";
		$log->object = "bukti penerimaan barang";
		$log->object_details = $goodreceive->good_receive_code;
		$log->status = 1;
		$log->save();
	}

	public function downloadPenerimaanBarCode(Request $request, $id)
	{
		// array = explode()
		// return $id;

		$arrayproduct = explode(",",$id);
		// return $arrayproduct;
		$product = product::leftjoin('product_category','product_category.product_category_id','=','product.product_category_id')
			->leftjoin('product_brand','product_brand.product_brand_id','=','product.product_brand_id')
			->leftjoin('product_type','product_type.product_type_id','=','product.product_type_id')
			->whereIn('product_id',$arrayproduct)
			->select('product.*','product_brand.name as b_name','product_type.name as t_name')
			->get();

		$pdf = DOMPDF::loadView('penerimaan_barang.bar_code_pdf', compact('product'));
		return $pdf->stream('BarCode.pdf');
	}

	public function excelPenerimaanBarCode(Request $request, $id)
	{
		$arrayproduct = explode(",",$id);
		$product = product::leftjoin('product_category','product_category.product_category_id','=','product.product_category_id')
			->leftjoin('product_brand','product_brand.product_brand_id','=','product.product_brand_id')
			->leftjoin('product_type','product_type.product_type_id','=','product.product_type_id')
			->leftjoin('good_receive_details','good_receive_details.good_receive_details_id','=','product.good_receive_details_id')
			->leftjoin('good_receive','good_receive.good_receive_id','=','good_receive_details.good_receive_id')
			->leftjoin('purchase_order','purchase_order.purchase_order_id','=','good_receive.purchase_order_id')
			->whereIn('product_id',$arrayproduct)
			->select('product.*','product_brand.name as b_name','product_type.name as t_name','purchase_order.purchase_order_number','purchase_order.notes as po_notes')
			->get();


		Excel::create($product[0]->purchase_order_number.'-'.$product[0]->po_notes, function($excel) use($product){
			$excel->sheet('list barang', function($sheet) use ($product){
				$sheet->loadView('penerimaan_barang.bar_code_excel',['product'=>$product]);
			});
		})->export('xlsx');
	}
}
