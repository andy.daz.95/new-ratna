<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\product;
use App\stockmovement;
use App\warehouse;
use App\productwarehouse;
use App\goodreceive;
use App\deliveryorder;
use App\purchaseorder;
use App\salesorder;
use App\stockcorrection;
use Excel;
use DB;
use DOMPDF;
use DataTable;

class InventoryController extends Controller
{
	public function showStockOverviewPage(){

		$data['blocked_product'] = product::
			with('category')
			->with('brand')
			->with('type')
			->whereIn('status',[3,4])
			->get();

		return view('stock.stockoverview', compact('data'));
	}

	public function getStockOverviewTable(Request $request)
	{
		$product = product::
		leftjoin('product_warehouse','product.product_id','=','product_warehouse.product_id')
			->leftjoin('product_category','product.product_category_id','=','product_category.product_category_id')
			->leftjoin('product_brand','product.product_brand_id','=','product_brand.product_brand_id')
			->leftjoin('product_type','product.product_type_id','=','product_type.product_type_id')
			->leftjoin('good_receive_details','good_receive_details.good_receive_details_id','=','product.good_receive_details_id')
			->leftjoin('good_receive','good_receive.good_receive_id','=','good_receive_details.good_receive_id')
			->leftjoin('purchase_order','purchase_order.purchase_order_id','=','good_receive.purchase_order_id')
			->selectRaw('product.*,product_category.description as c_description, product_brand.name as b_name, product_type.name as t_name, purchase_order.purchase_order_number')
			->groupby('product.product_id')
			->orderby('c_description','asc');

		if ($request->category && $request->category != "")
		{
			$product= $product->where('product_category.description','like','%'.$request->category.'%');
		}
		if($request->brand && $request->brand != "")
		{
			$product = $product->where('product_brand.name', 'like', '%'.$request->brand.'%');
		}
		if($request->barcode && $request->barcode != "")
		{
			$product = $product->where('product.bar_code', 'like', '%'.$request->barcode.'%');
		}

		if($request->status && $request->status != "")
		{
			$product = $product->where('product.status',$request->status);
		}else{
			$product = $product->where('product.status',1);
		}
		$product = $product->get();

		return DataTable::of($product)
			->setRowAttr([
				'value' => function($product) {
					return $product->product_id;
				},
			])
			->addColumn('action', function ($product){
					return
				'<a href="" class="btn btn-primary edit"><i class="material-icons">edit</i></a>
				<a href="printproduct/'.$product->product_id.'" target="_blank" class="btn btn-primary orange print-product"><i class="material-icons">print</i></a>
				<a href="#" class="btn btn-danger block-product"><i class="material-icons">block</i></a>';
			})
			->smart(false)
			->make(true);
	}

	public function exportStock(){
		$product = product::where('product.status','=',1)
			->leftjoin('product_category','product.product_category_id','=','product_category.product_category_id')
			->leftjoin('product_brand','product.product_brand_id','=','product_brand.product_brand_id')
			->leftjoin('product_type','product.product_type_id','=','product_type.product_type_id')
			->selectRaw("product.product_id as id,product_category.description as category,product_brand.name as brand, product_type.name as type,product.bar_code, '' as Alasan, '0' as Hapus ")
			->orderby('category','asc')
			->orderby('brand','asc')
			->orderby('type','asc')
			->get();

		Excel::create('Product List', function($excel) use($product){
            $excel->sheet('List', function($sheet) use ($product){
                $sheet->fromArray($product);
            });
//            $excel->sheet('Tambah Barang', function($sheet){
//            	$sheet->row(1, array(
//            		'Category', 'Brand', 'Type'
//            	));
//            });
        })->export('xlsx');
	}

	public function importStock(Request $request){
		if($request->hasFile('file')){
			$path = $request->file('file')->getRealPath();
			$sheet1 = Excel::selectSheetsByIndex(0)->load($path, function($reader){})->get(); // untuk ambil sheet pertam

			//Product yang hilang akan dihapus dan dapat dikembalikan lagi di menu stock overview bila sewaktu waktu ditemukan
			if(!empty($sheet1) && $sheet1->count()){
				foreach ($sheet1 as $key => $value) {
					if($value->hapus == '1')
					{
						$product = product::find($value->id);
						$product->delete_notes = $value->alasan;
						$product->delete_date = date('Y-m-d');
						$product->status = 4;
						$product->save();
					}
				}
			}
		}
	}

	public function blockProduct(Request $request){
		$product = product::find($request->id);
		$product->block_reason = $request->reason;
		$product->status = 3;
		$product->save();
	}
	public function unblockProduct(Request $request){
		$product = product::find($request->id);

		//kalau status nya di blokir hapus block reason, sedangkan kalau status nya hapus, dihapus delete date sama delete notes
		if ($product->status == 3){ // Blokir
			$product->block_reason = Null;
			$product->status = 1;
		}else if($product->status == 4){ // Hapus
			$product->delete_notes = Null;
			$product->delete_date = Null;
			$product->status = 1;
		}

		$product->save();
	}
	public function updateBarangFromOverview(Request $request){
		$product = product::find($request->id);
		$product->product_type_id = $request->producttype;
		$product->price_sale = $request->hargajual;
		$product->profit_rate = $request->rate;
		$product->save();
	}

	// public function detailStock(Request $request){
	// 	$data['detail'] = product::leftjoin('product_warehouse','product.product_id','=','product_warehouse.product_id')->selectRaw('product.*, product_warehouse.quantity, product_warehouse.warehouse_id')->where('product.product_id',$request->id)->get();
	// 	return $data;
	// }

	// public function showStockTransferPage(){
	// 	$data['gudang'] = warehouse::all();
	// 	$data['barang'] = product::all();
	// 	$data['stockmovement'] = stockmovement::leftjoin('warehouse as warehouse_from', 'warehouse_from.warehouse_id','=','stock_movement.warehouse_from')->leftjoin('warehouse as warehouse_to','warehouse_to.warehouse_id','=','stock_movement.warehouse_to')->select('stock_movement.*', 'warehouse_from.warehouse_name as warehouse_from','warehouse_to.warehouse_name as warehouse_to')->paginate(10);
	// 	return view('stock.stock_transfer', compact('data'));
	// }


	// public function getBarangTransfer(Request $request)
	// {
	// 	$product = product::where('product_id',$request->id)->first();
	// 	$qtyfrom = productwarehouse::where('product_id',$request->id)->where('warehouse_id', $request->gudangfrom)->first()->quantity;
	// 	$qtyto = productwarehouse::where('product_id',$request->id)->where('warehouse_id', $request->gudangto)->first()->quantity;
	// 	return ['qtyfrom' => $qtyfrom, 'qtyto'=> $qtyto, 'product'=>$product];
	// }

	// public function getLastTransferNumber(Request $request)
 //    {
 //    	$lasttransfertoday = stockmovement::where('stock_movement_number','like','SM/'.date('dmy').'/%')->orderby('stock_movement_id','desc')->first();
 //    	if(empty($lasttransfertoday))
 //    	{
 //    		$newtransfernumber = "SM/".date('dmy')."/1";
 //    		return $newtransfernumber;
 //    	}
 //    	else{
 //    		$tmptransfer = explode('/',$lasttransfertoday->stock_movement_number); 
 //    		$lastnumber = $tmptransfer[2];
 //    		$newtransfernumber = "SM/".date('dmy')."/".($lastnumber+1);
 //    		return $newtransfernumber;
 //    	}
 //    }

 //    public function getStockTransfer(Request $request)
 //    {
 //    	$stockmovement = stockmovement::leftjoin('product','product.product_id','=','stock_movement.product_id')->where('stock_movement_id', $request->id)->first();
 //    	return $stockmovement;
 //    }

	// public function createStockTransfer(Request $request)
	// {
	// 	$stockmovement = new stockmovement;
	// 	$stockmovement->product_id = $request->idbarang;
	// 	$stockmovement->warehouse_from = $request->gudangfrom;
	// 	$stockmovement->warehouse_to = $request->gudangto;
	// 	$stockmovement->date_stock_movement = date('Y-m-d', strtotime($request->tglsm)); 
	// 	$stockmovement->quantity = $request->qty; 
	// 	$stockmovement->stock_movement_number = $request->nostocktransfer;
	// 	$stockmovement->save();

	// 	productwarehouse::where('product_id',$request->idbarang)->where('warehouse_id', $request->gudangfrom)->update([
	// 		'quantity' => DB::raw('quantity-'.$request->qty),
	// 		]);
	// 	productwarehouse::where('product_id',$request->idbarang)->where('warehouse_id', $request->gudangto)->update([
	// 		'quantity' => DB::raw('quantity+'.$request->qty),
	// 		]);
	// }

	// public function showKartuStockPage(Request $request)
	// {
	// 	$data['barang'] = product::all();
	// 	$data['gudang'] = warehouse::all();
	// 	return view('stock.kartu_stock', compact('data'));
	// }

	// public function getKartuStock(Request $request)
	// {
	// 	$gr = goodreceive::leftjoin('good_receive_details','good_receive.good_receive_id','=','good_receive_details.good_receive_id')->where('good_receive_details.product_id',$request->barang)->selectraw("good_receive.good_receive_id as id, good_receive.good_receive_code as tcode, good_receive.good_receive_date as date, good_receive_details.quantity_received as qty, 'in' as stat");
	// 	$do = deliveryorder::leftjoin('delivery_order_details','delivery_order_details.delivery_order_id','=','delivery_order.delivery_order_id')->where('delivery_order_details.product_id',$request->barang)->selectraw("delivery_order.delivery_order_id, delivery_order.delivery_order_number, delivery_order.date_delivery, delivery_order_details.quantity_sent, 'out' as stat");
	// 	$smin = stockmovement::selectraw("stock_movement.stock_movement_id, stock_movement.stock_movement_number, stock_movement.date_stock_movement, stock_movement.quantity, 'out' as stat")->where('stock_movement.product_id',$request->barang);
	// 	$smout = stockmovement::selectraw("stock_movement.stock_movement_id, stock_movement.stock_movement_number, stock_movement.date_stock_movement, stock_movement.quantity, 'in' as stat")->where('stock_movement.product_id',$request->barang);
	// 	$scout = stockcorrection::selectraw("stock_correction.stock_correction_id, stock_correction.stock_correction_number, stock_correction.date_stock_correction, (stock_correction.projected_qty - stock_correction.actual_qty) as quantity, 'out' as stat")->where('stock_correction.product_id',$request->barang)->whereColumn('stock_correction.projected_qty','>','stock_correction.actual_qty');
	// 	$scin = stockcorrection::selectraw("stock_correction.stock_correction_id, stock_correction.stock_correction_number, stock_correction.date_stock_correction, (stock_correction.actual_qty - stock_correction.projected_qty) as quantity, 'in' as stat")->where('stock_correction.product_id',$request->barang)->whereColumn('stock_correction.projected_qty','<','stock_correction.actual_qty');

	// 	if ($request->gudang !== "0") {
	// 		$gr = $gr->where('good_receive_details.warehouse_id', $request->gudang);
	// 		$do = $do->where('delivery_order.warehouse_id', $request->gudang);
	// 		$smin = $smin->where('stock_movement.warehouse_from',$request->gudang);
	// 		$smout = $smout->where('stock_movement.warehouse_to',$request->gudang);
	// 		$scout = $scout->where('stock_correction.warehouse_id',$request->gudang);
	// 		$scin = $scin->where('stock_correction.warehouse_id',$request->gudang);
	// 		$overall = $gr->union($do)->union($smin)->union($smout)->union($scin)->union($scout)->orderby('date','asc')->orderby('stat','asc')->get();
	// 	}else{
	// 		$small = stockmovement::selectraw("stock_movement.stock_movement_id, stock_movement.stock_movement_number, stock_movement.date_stock_movement, stock_movement.quantity, 'inout' as stat")->where('product_id',$request->barang);
	// 		$overall = $gr->union($do)->union($small)->union($scin)->union($scout)->orderby('date')->get();
	// 	}

	// 	$viewoverall = view('stock.table-ks-overall', compact('overall'))->render();
	// 	return compact('viewpo','viewso','viewsm','viewoverall');
	// }

	public function showStockCorrection(Request $request)
	{	
		return view('stock.stock_correction', compact('data'));
	}

	public function showHistoryStockCorrection(){
		$data['history'] = product::where('status',4)->get();
		return view('stock.history_stock_correction', compact('data'));
	}

	public function printProduct(Request $request, $id)
	{
		$product = product::leftjoin('product_category','product_category.product_category_id','=','product.product_category_id')
			->leftjoin('product_brand','product_brand.product_brand_id','=','product.product_brand_id')
			->leftjoin('product_type','product_type.product_type_id','=','product.product_type_id')
			->where('product_id',$id)
			->select('product.*','product_brand.name as b_name','product_type.name as t_name')
			->get();

		$pdf = DOMPDF::loadView('penerimaan_barang.bar_code_pdf', compact('product'))->setPaper('A5','landscape');
		return $pdf->stream('BarCode.pdf');
	}

	// public function getStockInWarehouse(Request $request)
	// {
	// 	$stock = product::leftjoin('product_warehouse','product.product_id','=','product_warehouse.product_id')->where('product.product_id',$request->barang)->leftjoin('warehouse','warehouse.warehouse_id','=','product_warehouse.warehouse_id')->where('product_warehouse.warehouse_id',$request->gudang)->first();
	// 	return $stock;
	// }

	// public function getLastCorrectionNumber(Request $request)
 //    {
 //    	$lastcorrectiontoday = stockcorrection::where('stock_correction_number','like','SC/'.date('dmy').'/%')->orderby('stock_correction_id','desc')->first();
 //    	if(empty($lastcorrectiontoday))
 //    	{
 //    		$newcorrectionnumber = "SC/".date('dmy')."/1";
 //    		return $newcorrectionnumber;
 //    	}
 //    	else{
 //    		$tmpcorrection = explode('/',$lastcorrectiontoday->stock_correction_number); 
 //    		$lastnumber = $tmpcorrection[2];
 //    		$newcorrectionnumber = "SC/".date('dmy')."/".($lastnumber+1);
 //    		return $newcorrectionnumber;
 //    	}
 //    }

 //    public function createStockCorrection(Request $request)
 //    {
 //    	$stockcorrection = new stockcorrection;
 //    	$stockcorrection->product_id = $request->barang;
 //    	$stockcorrection->warehouse_id = $request->gudang;
 //    	$stockcorrection->stock_correction_number = $request->nostockcorrection;
 //    	$stockcorrection->date_stock_correction = date('Y-m-d', strtotime($request->tglsc));
 //    	$stockcorrection->projected_qty = $request->projectedqty;
 //    	$stockcorrection->actual_qty = $request->actualqty;
 //    	$stockcorrection->status = 1;
 //    	$stockcorrection->save();

 //    	if($request->projectedqty > $request->actualqty)
	//     {
	//     	$difference = $request->projectedqty - $request->actualqty; 
	//     	productwarehouse::where('product_id',$request->barang)
	//     		->where('warehouse_id',$request->gudang)
	//     		->update([
	//     			'quantity' => DB::raw('quantity-'.$difference),
	//     		]);
	//     }else{
	//     	$difference = $request->actualqty - $request->projectedqty; 
	//     	productwarehouse::where('product_id',$request->barang)
	//     		->where('warehouse_id',$request->gudang)
	//     		->update([
	//     			'quantity' => DB::raw('quantity+'.$difference),
	//     		]);
	//     }
 //    }
}